#include "CCommon.h"


CCommon::CCommon(void)
{
}


CCommon::~CCommon(void)
{
}

/*
============
va
Tworzy "w locie" stringa w formacie char*
Przykladowe uzycie:

int a = 5;
char str[32];
strcpy( str, CCommon::va( "Liczba wynosi %i", a ) );
============
*/

//////////////////////////////////////////////////////////////////////////
/// Tworzy "w locie" stringa w formacie char*.
/// @see sprintf
/// @param format format wyjściowy stringa
/// @param ... argumenty formatu (%i, %d, %s itd.)
/// @return sformatowany string
//////////////////////////////////////////////////////////////////////////
char    * __cdecl CCommon::va( char *format, ... ) {
    va_list     argptr;
    static char     string[2][1024];
    static int      index = 0;
    char    *buf;

    buf = string[index & 1];
    index++;

    va_start (argptr, format);
    vsprintf (buf, format,argptr);
    va_end (argptr);

    return buf;
}