#pragma once
#include "AllegroGlobals.h" 
#include "CTexture.h"


//////////////////////////////////////////////////////////////////////////
/// Klasa przechowująca informacje na temat tekstur. Inicjuje wczytywanie tektur.
//////////////////////////////////////////////////////////////////////////
class CTextureMgr
{

public:
	CTextureMgr(void);
	~CTextureMgr(void);

	static CTexture* GetTexture(int num);
	static void Init();
private:

	/// 
	/// Tablica tekstur.
	static CTexture* texArray[TEX_MAX];
};

