#pragma once
#include "CLevel.h"
#include"CMoleEngine.h"
#include "AllegroGlobals.h"


//////////////////////////////////////////////////////////////////////////
/// Nagłówek pliku poziomu.
//////////////////////////////////////////////////////////////////////////
typedef struct levelHeader_s 
{
	/// magiczna wartość
	unsigned int magic;

	/// wersja
	unsigned int version;

	/// ilość kolumn
	unsigned int columns; 

	/// ilość wierszy
	unsigned int rows; 
} levelHeader_t;

//////////////////////////////////////////////////////////////////////////
/// Klasa menedżera poziomów.
//////////////////////////////////////////////////////////////////////////
class CLevelMgr
{
public:
	CLevelMgr(void);
	~CLevelMgr(void);

	static void SaveLevel(unsigned int version,CLevel field,string FileName);
	static CLevel LoadLevel(string FileName);
};

