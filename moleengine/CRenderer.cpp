#include "CRenderer.h"

queue <Render_t> CRenderer::drawQueue;

CRenderer::CRenderer(void)
{
}


CRenderer::~CRenderer(void)
{
}


//////////////////////////////////////////////////////////////////////////
/// Metoda dodawania do kolejki wyświetlania. Kolejka jest rysowana pierwszej dodanej tekstury do ostatniej.
/// @param tex numer tekstury, która ma być wyświetlona
/// @param x współrzędna X, w których ma być wyświetlona tekstura
/// @param y współrzędna Y, w których ma być wyświetlona tekstura
//////////////////////////////////////////////////////////////////////////
void CRenderer::AddToDrawQueue(int tex,int x,int y)
{
	Render_t temp;
	temp.tex = tex;
	temp.x = x;
	temp.y = y;
	temp.region = false;
	temp.scaled = false;
	temp.colored = false;
	temp.r = 255;
	temp.g = 255;
	temp.b = 255;
	drawQueue.push(temp);
}

//////////////////////////////////////////////////////////////////////////
/// Metoda dodawania do kolejki wyświetlania - wersja zaawansowana. Kolejka jest rysowana pierwszej dodanej tekstury do ostatniej.
/// @param tex numer tekstury, która ma być wyświetlona
/// @param x współrzędna X, w których ma być wyświetlona tekstura
/// @param y współrzędna Y, w których ma być wyświetlona tekstura
/// @param region true, jesli rysujemy tylko wycinek danej tekstury
/// @param sx wspolrzedna X startu wycinka tekstury
/// @param sy wspolrzedna Y startu wycinka tekstury
/// @param sw szerokosc wycinka tekstury
/// @param sh dlugosc wycinka tekstury
/// @param scaled true, jesli skalujemy teksture
/// @param scale_x wspolczynnik X skalowania tekstury na wyjsciu
/// @param scale_y wspolczynnik Y skalowania tekstury na wyjsciu
/// @param colored true, jesli kolorujemy teksture
/// @param r składowa R koloru (0-255)
/// @param g składowa G koloru (0-255)
/// @param b składowa B koloru (0-255)
//////////////////////////////////////////////////////////////////////////
void CRenderer::AddToDrawQueue( int tex, int x, int y, bool region, float sx, float sy, float sw, float sh, bool scaled, float scale_x, float scale_y, bool colored, unsigned char r, unsigned char g, unsigned char b )
{
	Render_t temp;
	temp.tex = tex;
	temp.x = x;
	temp.y = y;

	// wycinek bitmapy do wyrysowania
	temp.region = region;
	if( region ) {
		temp.sx = sx;
		temp.sy = sy;
		temp.sw = sw;
		temp.sh = sh;
	}
	
	// skalowanie
	temp.scaled = scaled;
	if( scaled ) {
		temp.scale_x = scale_x;
		temp.scale_y = scale_y;
	}

	// kolorowanie
	temp.colored = colored;
	if( colored ) {
		temp.r = r;
		temp.g = g;
		temp.b = b;
	}
	drawQueue.push(temp);
}

//////////////////////////////////////////////////////////////////////////
/// Rysuje na ekranie pojedynczą teksturę z początku kolejki
//////////////////////////////////////////////////////////////////////////
void CRenderer::DrawSingle()
{
	if( !drawQueue.empty() )
	{
		Render_t r = drawQueue.front();
		CTexture *t = CTextureMgr::GetTexture( r.tex );

		if( r.region || r.scaled || r.colored ) {

			al_draw_tinted_scaled_bitmap( t->texture, ( r.colored ? al_map_rgb( r.r, r.g, r.b ) : al_map_rgb( 255, 255, 255 ) ),
				( r.region ? r.sx : 0 ), ( r.region ? r.sy : 0 ), ( r.region ? r.sw : (float)t->width ), ( r.region ? r.sh : (float)t->height ), // wycinek
				r.x, r.y, // x,y
				( r.scaled ? (float)( t->width * r.scale_x ) : (float)t->width ), // skalowanie
				( r.scaled ? (float)( t->height * r.scale_y ) : (float)t->height ), 0 ); // skalowanie, flagi

		}
		else {

			al_draw_bitmap(
				CTextureMgr::GetTexture(r.tex)->texture, //bitmap
				r.x, //pos X
				r.y, // pos Y
				0
				);
		}
	drawQueue.pop();
	}
}


//////////////////////////////////////////////////////////////////////////
/// Rysuje na ekranie całą kolejkę
//////////////////////////////////////////////////////////////////////////
void CRenderer::DrawQueue()
{

	for(drawQueue.size(); drawQueue.size() > 0; )
	{
		DrawSingle();
	}

}


//////////////////////////////////////////////////////////////////////////
/// Dodaje nową kolejkę do kolejki głównej programu
/// @param queue kolejka
//////////////////////////////////////////////////////////////////////////
void CRenderer::AddQueue(CRenderer queue)
{
	for(queue.drawQueue.size(); queue.drawQueue.size() > 0; )
	{
		drawQueue.push(queue.drawQueue.front());
		queue.drawQueue.pop();
	}
}

//////////////////////////////////////////////////////////////////////////
/// Czyści ekran do koloru czarnego.
//////////////////////////////////////////////////////////////////////////
void CRenderer::ClearToBlack()
{
	al_clear_to_color(al_map_rgb(0,0,0));
}

