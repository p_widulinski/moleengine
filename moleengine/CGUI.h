#pragma once
#include "CRenderer.h"
#include "CMoleEngine.h"
#include "CLanguageMgr.h"

//////////////////////////////////////////////////////////////////////////
/// Klasa zarządzająca graficznym interfejsem użytkownika.
//////////////////////////////////////////////////////////////////////////
class CGUI
{
public:
	///
	/// Przesunięcie Y creditsów
	static int credits_y_offset;

	CGUI(void);
	~CGUI(void);
	static void DrawGUI(int window);

	// obsługa czcionek
	static float GetTextWidth( int fontTexture, float scale_x, const char *text );
	static void DrawText( int fontTexture, int x, int y, float scale_x, float scale_y, const char *text );
	static void DrawText( int fontTexture, unsigned char color_r, unsigned char color_g, unsigned char color_b, int x, int y, float scale_x, float scale_y, const char *text );
	static void DrawTextCentered( int fontTexture, int x_center, int y, float scale_x, float scale_y, const char *text );
	static void DrawTextCentered( int fontTexture, unsigned char color_r, unsigned char color_g, unsigned char color_b, int x_center, int y, float scale_x, float scale_y, const char *text );

	static void DrawMainMenu();
	static void DrawGameplayHUD( bool drawText );
	static void DrawOptions();
	static void DrawOptions2();
	static void DrawCredits( bool drawText );
	static char *KeyNameForNumber( int keynum );
};

