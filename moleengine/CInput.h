#pragma once
#include <map>
#include <allegro5/allegro.h>
#include "AllegroGlobals.h" 
using namespace std;



//////////////////////////////////////////////////////////////////////////
/// Klasa odpowiedzialna za odczyt stanu klawiatury.
//////////////////////////////////////////////////////////////////////////
class CInput
{
public:
	CInput(void);
	~CInput(void);
	static void Update(ALLEGRO_EVENT);
	static bool GetKeyState(int);
	static void Delay(int key);
private:

	/// 
	/// Mapa aktualnych stanów klawiszy.
	static std::map<int, bool> keys;
};