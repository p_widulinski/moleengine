#include "CMole.h"
#include "CAudioMgr.h"
#include "CMoleEngine.h"


CMole::CMole(void)
{
	X = 0;
	Y = 0;
	timer_d = MOVE_MOVE_TIME;
}


CMole::~CMole(void)
{
}


//////////////////////////////////////////////////////////////////////////
/// Obsługuje ruch gracza.
/// @param map obiekt obecnego poziomu
/// @param direction kierunek ruchu
//////////////////////////////////////////////////////////////////////////
void CMole::Move(CLevel& map,int direction)
{

	switch(direction)
	{
	case MOV_LEFT:


		if(CMole::TerrainCheck(map.field[X-1+(Y)*map.rows]))
		{
			if(MoveStone(map, direction))
			{
				X--;
				dir = MOV_LEFT;
				timer = timer_d;
				CMole::Entry(map,X+(Y)*map.rows);
			}
		}
		break;

	case MOV_DOWN:
		if(CMole::TerrainCheck(map.field[X+(Y+1)*map.rows]))
		{
			if(MoveStone(map, direction))
			{
				Y++;
				dir = MOV_DOWN;
				timer = timer_d;
				CMole::Entry(map,X+(Y)*map.rows);
			}
		}
		break;

	case MOV_RIGHT:
		if(CMole::TerrainCheck(map.field[X+1+(Y)*map.rows]))
		{
			if(MoveStone(map, direction))
			{
				X++;
				dir = MOV_RIGHT;
				timer = timer_d;
				CMole::Entry(map,X+(Y)*map.rows);
			}
		}
		break;

	case MOV_UP:
		if(CMole::TerrainCheck(map.field[X+(Y-1)*map.rows]))
		{
			if(MoveStone(map, direction))
			{
				Y--;
				dir = MOV_UP;
				timer = timer_d;
				CMole::Entry(map,X+(Y)*map.rows);
			}
		}
		break;

	}
}

//////////////////////////////////////////////////////////////////////////
/// Obsługuje wejście gracza na inne niż obecnie zajmowane pole.
/// @param map obecny poziom
/// @param destination obliczone współrzędne pola, na które wkracza gracz (wzór: X+(Y)*map.rows)
//////////////////////////////////////////////////////////////////////////
void CMole::Entry(CLevel& map, int destination)
{
	if(map.field[destination] == TER_ELIXIR1)
	{
		currentLev.elixir = EX_TIME_STOP;
		currentLev.ex_time = 60*10; //TODO:Ustalić odpowiednią wartość x_time - PG
		CMoleEngine::AddScore( 70 );
		CAudioMgr::GetMusicTrack(MUSIC_TEST2)->SetSpeed( 0.7f );
	}
	if(map.field[destination] == TER_ELIXIR2)
	{
		currentLev.elixir = EX_WALL_BREAKER;
		currentLev.ex_time = 3; //TODO:Ustalić odpowiednią wartość x_time - PG
		CMoleEngine::AddScore( 90 );
		CAudioMgr::GetMusicTrack(MUSIC_TEST2)->SetSpeed( 1.3f );
	}
	if(map.field[destination] == TER_WORM)
	{
		currentLev.worm++;
		CMoleEngine::AddScore( 50 );
		CAudioMgr::GetSfx( SFX_WORM_EAT )->Play();
	}

	if(map.field[destination] == TER_DIAMOND)
	{
		currentLev.diamond++;
		CMoleEngine::AddScore( 300 );
		CAudioMgr::GetSfx( SFX_DIAMOND_COLLECTED )->Play();

	}

	if(map.field[destination] == TER_EXIT && currentLev.worm == currentLev.worm_total)
	{
		next_lev = true;
	}

	if(map.field[destination] != TER_ENTRY && map.field[destination] != TER_EXIT)
	{
		if(map.field[destination] == TER_WALL)
		{
			currentLev.ex_time--;
		}

		if( map.field[destination] == TER_SOIL ) {
			CMoleEngine::AddScore( 1 );
		}

		CAudioMgr::GetSfx( SFX_STEP_ON_GROUND )->Play();

		map.field[destination] = TER_NONE;
	}
}

//////////////////////////////////////////////////////////////////////////
/// Metoda sprawdzania czy gracz może stanąć na danym terenie
/// @param type typ terenu
/// @return true, jeśli gracz może stanąć na danym terenie
//////////////////////////////////////////////////////////////////////////
bool CMole::TerrainCheck(int type)
{
	switch(type)
	{
	case TER_NONE:
		return true;
		break;
	case TER_SOIL:
		return true;
		break;
	case TER_WALL:
		if(currentLev.elixir == EX_WALL_BREAKER)
		{
			return true;
		}
		else
		{
			return false;
		}
		break;
	case TER_STONE:
		return false;
		break;
	case TER_WORM:
		return true;
		break;
	case TER_DIAMOND:
		return true;
		break;
	case TER_ELIXIR1:
		return true;
		break;
	case TER_ELIXIR2:
		return true;
		break;
	case TER_ENTRY:
		return true;
		break;
	case TER_EXIT:
		return true;
		break;
	case TER_WALL_BORDER:
		return false;
		break;
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////
/// Metoda obsługująca przesuwanie kamieni przez gracza.
/// @see moleMove_t
/// @param map obecny poziom
/// @param direction kierunek przesuwania kamieni
/// @return true, jeśli ruch dozwolony
//////////////////////////////////////////////////////////////////////////
bool CMole::MoveStone(CLevel& map, int direction)
{
	switch(direction)
	{
	case MOV_LEFT:

		for(int i = 0; i < map.stoneList.size();i++)
		{
			if(map.stoneList[i].x+1 == X && map.stoneList[i].y == Y)
			{
				if(map.field[X-2+Y*map.rows] == TER_NONE)
				{
					if(CheckStone(map,map.stoneList[i].x-1, map.stoneList[i].y))
					{
						return false;
					}
					else
					{

						map.stoneList[i].x--;
						return true;
					
					}
				}
				else
				{
					return false;
				}

			}
		}
		break;
	case MOV_DOWN:
		for(int i = 0; i < map.stoneList.size();i++)
		{
			if(map.stoneList[i].x  == X && map.stoneList[i].y-1 == Y)
			{
				return false;
			}
		}
		break;
		break;
	case MOV_RIGHT:
		for(int i = 0; i < map.stoneList.size();i++)
		{
			if(map.stoneList[i].x-1 == X && map.stoneList[i].y == Y )
			{
				if(map.field[X+2+Y*map.rows] == TER_NONE )
				{
					if(CheckStone(map, map.stoneList[i].x+1, map.stoneList[i].y))
					{
					return false;
					}
					else
					{

						map.stoneList[i].x++;
						return true;
					}
				
				
				}
				else
				{
					return false;
				}
		}
		}
		break;

	case MOV_UP:
		for(int i = 0; i < map.stoneList.size();i++)
		{
			if(map.stoneList[i].x == X && map.stoneList[i].y+1 == Y)
			{

				return false;
			}
		}

		break;

	}

	return true;
}


//////////////////////////////////////////////////////////////////////////
/// Sprawdza, czy pod danymi xy mapy znajduje się kamień.
/// @param map poziom
/// @param xx współrzędna X
/// @param yy współrzędna Y
/// @return true, jeśli pod xy znajduje się kamień
//////////////////////////////////////////////////////////////////////////
bool CMole::CheckStone(CLevel& map, int xx, int yy)
{

		for(int i = 0; i < map.stoneList.size();i++)
		{
			if(map.stoneList[i].x == xx && map.stoneList[i].y == yy )
			{
					return true;
			}
		}
		
	return false;
}