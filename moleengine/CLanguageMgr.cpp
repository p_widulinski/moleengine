#include "CLanguageMgr.h"
#include <fstream>

using namespace std;

int CLanguageMgr::lang;
char CLanguageMgr::lang_text[LANG_MAX][TXT_MAX][200];

CLanguageMgr::CLanguageMgr(void)
{

}


CLanguageMgr::~CLanguageMgr(void)
{
}

//////////////////////////////////////////////////////////////////////////
/// Pobiera zlokalizowany tekst na podstawie podanej etykiety.
/// @see label_t
/// @param num etykieta
/// @return zlokalizowany tekst
//////////////////////////////////////////////////////////////////////////
char* CLanguageMgr::GetText (int num)
{
	return lang_text[lang][num];
}

//////////////////////////////////////////////////////////////////////////
/// Inicjalizuje klasę menedżera języków. Wczytuje wszystkie etykiety tekstowe.
//////////////////////////////////////////////////////////////////////////
void CLanguageMgr:: Init(void)
{
	const short rozmiar = 200;
	char linia[rozmiar];
	int nr_linii=1;
	char translate[200][200];

   fstream plik;
    plik.open("translate.txt", ios::in);

	if(plik.good()==false) cout<<"Nie mozna otworzyc pliku!";

    while (plik.getline(linia, rozmiar))
    {
        
			strcpy(translate[nr_linii-1], linia );
				nr_linii++;
    }

    plik.close();

	strcpy(lang_text[LANG_POLSKI][TXT_OPTIONS],translate[0]);
	strcpy(lang_text[LANG_POLSKI][TXT_REMAP_KEYS],translate[1]);
	strcpy(lang_text[LANG_POLSKI][TXT_LANGUAGE],translate[2]);
	strcpy(lang_text[LANG_POLSKI][TXT_POLSKI],translate[3]);
	strcpy(lang_text[LANG_POLSKI][TXT_ENGLISH],translate[4]);
	strcpy(lang_text[LANG_POLSKI][TXT_SFX_VOLUME],translate[5]);
	strcpy(lang_text[LANG_POLSKI][TXT_MUSIC_VOLUME],translate[6]);
	strcpy(lang_text[LANG_POLSKI][TXT_DIFFICULTY],translate[7]);
	strcpy(lang_text[LANG_POLSKI][TXT_EASY],translate[8]);
	strcpy(lang_text[LANG_POLSKI][TXT_NORMAL],translate[9]);
	strcpy(lang_text[LANG_POLSKI][TXT_HARD],translate[10]);
	strcpy(lang_text[LANG_POLSKI][TXT_UP],translate[11]);
	strcpy(lang_text[LANG_POLSKI][TXT_DOWN],translate[12]);
	strcpy(lang_text[LANG_POLSKI][TXT_LEFT],translate[13]);
	strcpy(lang_text[LANG_POLSKI][TXT_RIGHT],translate[14]);
	strcpy(lang_text[LANG_POLSKI][TXT_SUICIDE],translate[15]);
	strcpy(lang_text[LANG_POLSKI][TXT_CREDITS],translate[16]);
	strcpy(lang_text[LANG_POLSKI][TXT_Project_Manager],translate[17]);
	strcpy(lang_text[LANG_POLSKI][TXT_Quality_Assurance],translate[18]);
	strcpy(lang_text[LANG_POLSKI][TXT_Game_Logic],translate[19]);
	strcpy(lang_text[LANG_POLSKI][TXT_Core_Programming],translate[20]);
	strcpy(lang_text[LANG_POLSKI][TXT_Testing],translate[21]);
	strcpy(lang_text[LANG_POLSKI][TXT_Level_Development],translate[22]);
	strcpy(lang_text[LANG_POLSKI][TXT_SFX_and_Music],translate[23]);
	strcpy(lang_text[LANG_POLSKI][TXT_Input_Programming],translate[24]);
	strcpy(lang_text[LANG_POLSKI][TXT_Graphics_and_UI],translate[25]);
	strcpy(lang_text[LANG_POLSKI][TXT_Can_you_find_all],translate[26]);
	strcpy(lang_text[LANG_POLSKI][TXT_of_the_Cheat_Codes],translate[27]);


	strcpy(lang_text[LANG_POLSKI][TXT_Soil],translate[28]);
	strcpy(lang_text[LANG_POLSKI][TXT_Wall],translate[29]);
	strcpy(lang_text[LANG_POLSKI][TXT_Rock],translate[30]);
	strcpy(lang_text[LANG_POLSKI][TXT_Worm],translate[31]);
	strcpy(lang_text[LANG_POLSKI][TXT_Diamond],translate[32]);
	strcpy(lang_text[LANG_POLSKI][TXT_TimeStop],translate[33]);
	strcpy(lang_text[LANG_POLSKI][TXT_Entry],translate[34]);
	strcpy(lang_text[LANG_POLSKI][TXT_Exit],translate[35]);
	strcpy(lang_text[LANG_POLSKI][TXT_StoneBreak],translate[36]);
	strcpy(lang_text[LANG_POLSKI][TXT_HardWall],translate[37]);
	strcpy(lang_text[LANG_POLSKI][TXT_select_tile],translate[38]);
	strcpy(lang_text[LANG_POLSKI][TXT_ENTER_place],translate[39]);
	strcpy(lang_text[LANG_POLSKI][TXT_tile],translate[40]);
	strcpy(lang_text[LANG_POLSKI][TXT_W_change],translate[41]);
	strcpy(lang_text[LANG_POLSKI][TXT_map_size],translate[42]);
	strcpy(lang_text[LANG_POLSKI][TXT_S_save_map],translate[43]);

	strcpy(lang_text[LANG_POLSKI][TXT_L_load_map],translate[44]);
	strcpy(lang_text[LANG_POLSKI][TXT_T_playtest],translate[45]);
	strcpy(lang_text[LANG_POLSKI][TXT_after_save],translate[46]);
	strcpy(lang_text[LANG_POLSKI][TXT_Map_name],translate[47]);
	strcpy(lang_text[LANG_POLSKI][TXT_Enter_to_continue],translate[48]);
	strcpy(lang_text[LANG_POLSKI][TXT_Escape_to_exit],translate[49]);
	strcpy(lang_text[LANG_POLSKI][TXT_Backspace_in_order_to_shift],translate[50]);
	strcpy(lang_text[LANG_POLSKI][TXT_Delete_to_delete_the_figure],translate[51]);
	strcpy(lang_text[LANG_POLSKI][TXT_Mission_failed],translate[52]);
	strcpy(lang_text[LANG_POLSKI][TXT_You_died],translate[53]);
	strcpy(lang_text[LANG_POLSKI][TXT_Press_Enter],translate[54]);
	strcpy(lang_text[LANG_POLSKI][TXT_to_try_again],translate[55]);
	strcpy(lang_text[LANG_POLSKI][TXT_Time_out],translate[56]);
	strcpy(lang_text[LANG_POLSKI][TXT_Game_over],translate[57]);
	strcpy(lang_text[LANG_POLSKI][TXT_to_back_to_main_menu],translate[58]);
	strcpy(lang_text[LANG_POLSKI][TXT_to_try_again2],translate[59]);
	strcpy(lang_text[LANG_POLSKI][TXT_You_commited_suicide],translate[60]);
	strcpy(lang_text[LANG_POLSKI][TXT_Congratulation],translate[61]);
	strcpy(lang_text[LANG_POLSKI][TXT_You_beat_the_Game],translate[62]);
	strcpy(lang_text[LANG_POLSKI][TXT_Score],translate[63]);
	strcpy(lang_text[LANG_POLSKI][TXT_Difficulty],translate[64]);
	strcpy(lang_text[LANG_POLSKI][TXT_Easy],translate[65]);
	strcpy(lang_text[LANG_POLSKI][TXT_Normal],translate[66]);
	strcpy(lang_text[LANG_POLSKI][TXT_Hard],translate[67]);
	strcpy(lang_text[LANG_POLSKI][TXT_Level_Complete],translate[68]);
	strcpy(lang_text[LANG_POLSKI][TXT_Diamonds_colected],translate[69]);
	strcpy(lang_text[LANG_POLSKI][TXT_Time_left],translate[70]);
	strcpy(lang_text[LANG_POLSKI][TXT_Bonus_Score],translate[71]);
	strcpy(lang_text[LANG_POLSKI][TXT_Perfect],translate[72]);
	strcpy(lang_text[LANG_POLSKI][TXT_to_continue],translate[73]);
	strcpy(lang_text[LANG_POLSKI][TXT_Map_Size],translate[74]);
	strcpy(lang_text[LANG_POLSKI][TXT_Width],translate[75]);
	strcpy(lang_text[LANG_POLSKI][TXT_Height],translate[76]);



	
	strcpy(lang_text[LANG_ENG][TXT_OPTIONS],translate[77]);
	strcpy(lang_text[LANG_ENG][TXT_REMAP_KEYS],translate[78]);
	strcpy(lang_text[LANG_ENG][TXT_LANGUAGE],translate[79]);
	strcpy(lang_text[LANG_ENG][TXT_POLSKI],translate[80]);
	strcpy(lang_text[LANG_ENG][TXT_ENGLISH],translate[81]);
	strcpy(lang_text[LANG_ENG][TXT_SFX_VOLUME],translate[82]);
	strcpy(lang_text[LANG_ENG][TXT_MUSIC_VOLUME],translate[83]);
	strcpy(lang_text[LANG_ENG][TXT_DIFFICULTY],translate[84]);
	strcpy(lang_text[LANG_ENG][TXT_EASY],translate[85]);
	strcpy(lang_text[LANG_ENG][TXT_NORMAL],translate[86]);
	strcpy(lang_text[LANG_ENG][TXT_HARD],translate[87]);
	strcpy(lang_text[LANG_ENG][TXT_UP],translate[88]);
	strcpy(lang_text[LANG_ENG][TXT_DOWN],translate[89]);
	strcpy(lang_text[LANG_ENG][TXT_LEFT],translate[90]);
	strcpy(lang_text[LANG_ENG][TXT_RIGHT],translate[91]);
	strcpy(lang_text[LANG_ENG][TXT_SUICIDE],translate[92]);
	strcpy(lang_text[LANG_ENG][TXT_CREDITS],translate[93]);
	strcpy(lang_text[LANG_ENG][TXT_Project_Manager],translate[94]);
	strcpy(lang_text[LANG_ENG][TXT_Quality_Assurance],translate[95]);
	strcpy(lang_text[LANG_ENG][TXT_Game_Logic],translate[96]);
	strcpy(lang_text[LANG_ENG][TXT_Core_Programming],translate[97]);
	strcpy(lang_text[LANG_ENG][TXT_Testing],translate[98]);
	strcpy(lang_text[LANG_ENG][TXT_Level_Development],translate[99]);
	strcpy(lang_text[LANG_ENG][TXT_SFX_and_Music],translate[100]);
	strcpy(lang_text[LANG_ENG][TXT_Input_Programming],translate[101]);
	strcpy(lang_text[LANG_ENG][TXT_Graphics_and_UI],translate[102]);
	strcpy(lang_text[LANG_ENG][TXT_Can_you_find_all],translate[103]);
	strcpy(lang_text[LANG_ENG][TXT_of_the_Cheat_Codes],translate[104]);

	strcpy(lang_text[LANG_ENG][TXT_Soil],translate[105]);
	strcpy(lang_text[LANG_ENG][TXT_Wall],translate[106]);
	strcpy(lang_text[LANG_ENG][TXT_Rock],translate[107]);
	strcpy(lang_text[LANG_ENG][TXT_Worm],translate[108]);
	strcpy(lang_text[LANG_ENG][TXT_Diamond],translate[109]);
	strcpy(lang_text[LANG_ENG][TXT_TimeStop],translate[110]);
	strcpy(lang_text[LANG_ENG][TXT_Entry],translate[111]);
	strcpy(lang_text[LANG_ENG][TXT_Exit],translate[112]);
	strcpy(lang_text[LANG_ENG][TXT_StoneBreak],translate[113]);
	strcpy(lang_text[LANG_ENG][TXT_HardWall],translate[114]);
	strcpy(lang_text[LANG_ENG][TXT_select_tile],translate[115]);
	strcpy(lang_text[LANG_ENG][TXT_ENTER_place],translate[116]);
	strcpy(lang_text[LANG_ENG][TXT_tile],translate[117]);
	strcpy(lang_text[LANG_ENG][TXT_W_change],translate[118]);
	strcpy(lang_text[LANG_ENG][TXT_map_size],translate[119]);
	strcpy(lang_text[LANG_ENG][TXT_S_save_map],translate[120]);

	strcpy(lang_text[LANG_ENG][TXT_L_load_map],translate[121]);
	strcpy(lang_text[LANG_ENG][TXT_T_playtest],translate[122]);
	strcpy(lang_text[LANG_ENG][TXT_after_save],translate[123]);
	strcpy(lang_text[LANG_ENG][TXT_Map_name],translate[124]);
	strcpy(lang_text[LANG_ENG][TXT_Enter_to_continue],translate[125]);
	strcpy(lang_text[LANG_ENG][TXT_Escape_to_exit],translate[126]);
	strcpy(lang_text[LANG_ENG][TXT_Backspace_in_order_to_shift],translate[127]);
	strcpy(lang_text[LANG_ENG][TXT_Delete_to_delete_the_figure],translate[128]);
	strcpy(lang_text[LANG_ENG][TXT_Mission_failed],translate[129]);
	strcpy(lang_text[LANG_ENG][TXT_You_died],translate[130]);
	strcpy(lang_text[LANG_ENG][TXT_Press_Enter],translate[131]);
	strcpy(lang_text[LANG_ENG][TXT_to_try_again],translate[132]);
	strcpy(lang_text[LANG_ENG][TXT_Time_out],translate[133]);
	strcpy(lang_text[LANG_ENG][TXT_Game_over],translate[134]);
	strcpy(lang_text[LANG_ENG][TXT_to_back_to_main_menu],translate[135]);
	strcpy(lang_text[LANG_ENG][TXT_to_try_again2],translate[136]);
	strcpy(lang_text[LANG_ENG][TXT_You_commited_suicide],translate[137]);
	strcpy(lang_text[LANG_ENG][TXT_Congratulation],translate[138]);
	strcpy(lang_text[LANG_ENG][TXT_You_beat_the_Game],translate[139]);
	strcpy(lang_text[LANG_ENG][TXT_Score],translate[140]);
	strcpy(lang_text[LANG_ENG][TXT_Difficulty],translate[141]);
	strcpy(lang_text[LANG_ENG][TXT_Easy],translate[142]);
	strcpy(lang_text[LANG_ENG][TXT_Normal],translate[143]);
	strcpy(lang_text[LANG_ENG][TXT_Hard],translate[144]);
	strcpy(lang_text[LANG_ENG][TXT_Level_Complete],translate[145]);
	strcpy(lang_text[LANG_ENG][TXT_Diamonds_colected],translate[146]);
	strcpy(lang_text[LANG_ENG][TXT_Time_left],translate[147]);
	strcpy(lang_text[LANG_ENG][TXT_Bonus_Score],translate[148]);
	strcpy(lang_text[LANG_ENG][TXT_Perfect],translate[149]);
	strcpy(lang_text[LANG_ENG][TXT_to_continue],translate[150]);
	strcpy(lang_text[LANG_ENG][TXT_Map_Size],translate[151]);
	strcpy(lang_text[LANG_ENG][TXT_Width],translate[152]);
	strcpy(lang_text[LANG_ENG][TXT_Height],translate[153]);

	/*Wyjątkowo poniższe polskie i angielskie słowa znajduja sie na koncu pliku translate.txt*/


	strcpy(lang_text[LANG_POLSKI][TXT_PLAY],translate[154]);
	strcpy(lang_text[LANG_POLSKI][TXT_EXIT],translate[155]);
	strcpy(lang_text[LANG_POLSKI][TXT_UP_ARROW],translate[156]);
	strcpy(lang_text[LANG_POLSKI][TXT_DOWN_ARROW],translate[157]);
	strcpy(lang_text[LANG_POLSKI][TXT_LEFT_ARROW],translate[158]);
	strcpy(lang_text[LANG_POLSKI][TXT_RIGHT_ARROW],translate[159]);
	strcpy(lang_text[LANG_POLSKI][TXT_SPACE],translate[160]);
	strcpy(lang_text[LANG_POLSKI][TXT_UNKNOWN],translate[161]);
	strcpy(lang_text[LANG_POLSKI][TXT_BACK],translate[162]);

	strcpy(lang_text[LANG_ENG][TXT_PLAY],translate[163]);
	strcpy(lang_text[LANG_ENG][TXT_EXIT],translate[164]);
	strcpy(lang_text[LANG_ENG][TXT_UP_ARROW],translate[165]);
	strcpy(lang_text[LANG_ENG][TXT_DOWN_ARROW],translate[166]);
	strcpy(lang_text[LANG_ENG][TXT_LEFT_ARROW],translate[167]);
	strcpy(lang_text[LANG_ENG][TXT_RIGHT_ARROW],translate[168]);
	strcpy(lang_text[LANG_ENG][TXT_SPACE],translate[169]);
	strcpy(lang_text[LANG_ENG][TXT_UNKNOWN],translate[170]);
	strcpy(lang_text[LANG_ENG][TXT_BACK],translate[171]);




}
