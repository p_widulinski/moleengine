#pragma once
#include <vector>
#include "CStone.h"

//////////////////////////////////////////////////////////////////////////
/// Klasa opisująca pojedynczy poziom.
//////////////////////////////////////////////////////////////////////////
class CLevel
{
public:
	/// 
	/// Pole gry
	int *field;

	/// 
	/// Ilość rzędów pola gry
	int rows;

	/// 
	/// Ilość kolumn pola gry
	int columns;

	/// 
	/// Ilość dżdżownic na poziomie
	int worms;

	/// 
	/// Ilość diamentów na poziomie
	int diamonds;

	/// 
	/// Wektor z kamieniami na poziomie
	vector <CStone> stoneList;

	CLevel(int size[2]);
	CLevel(int r, int c);
	CLevel(int r);
	CLevel();
	
	~CLevel(void);
	bool StoneExist(int x,int y);
private:

};

