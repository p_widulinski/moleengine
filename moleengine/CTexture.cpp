#include "CTexture.h"


CTexture::CTexture(void)
{
	width = 0;
	height = 0;
	texture = NULL;
}


CTexture::~CTexture(void)
{
}


//////////////////////////////////////////////////////////////////////////
/// Wczytuje teksturę z pliku, scieżka wzgledną z katalogu graphics/. Wczytuje pliki z rozszerzeniem .png.
/// @param file nazwa pliku wczytywanego
//////////////////////////////////////////////////////////////////////////
void CTexture::LoadTexture(char* file)
{
	texture = al_load_bitmap(CCommon::va("graphics/%s.png", file));
	width = al_get_bitmap_width(texture);
	height = al_get_bitmap_height(texture);
}
