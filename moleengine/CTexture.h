#pragma once
#include "AllegroGlobals.h" 
#include "CCommon.h"

//////////////////////////////////////////////////////////////////////////
/// Klasa pojedyńczej tektury. Przechowuje informacje o jej rozmiarach i wskaźnik do pliku z bitmapą
//////////////////////////////////////////////////////////////////////////
class CTexture
{
public:
	/// 
	/// Szerokość tekstury
	int width;

	/// 
	/// Długość tekstury
	int height;

	/// 
	/// Wskaźnik do tekstury w systemie Allegro
	ALLEGRO_BITMAP* texture; 

	CTexture(void);
	~CTexture(void);
	void LoadTexture(char* file);
};

