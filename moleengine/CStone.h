#pragma once

#include "AllegroGlobals.h"

//////////////////////////////////////////////////////////////////////////
/// Klasa opisująca pojedynczy kamień.
//////////////////////////////////////////////////////////////////////////
class CStone
{
public:
	/// 
	/// Pozycja X
	int x;

	/// 
	/// Pozycja Y
	int y;

	/// licznik spadania
	int timer;
	
	/// ustalony czas spadania
	int timer_d;
	
	/// kierunek spadania
	int direction;
	
	///czy kamień jest trzymany i z którego kierunku
	int hold; 

	
	CStone(void);
	~CStone(void);
};

