#pragma once
#include "AllegroGlobals.h"
#include "CLevel.h"

//////////////////////////////////////////////////////////////////////////
/// Klasa obiektu gracza. Przechowuje zmienne gracza i operuje na nich
//////////////////////////////////////////////////////////////////////////
class CMole
{
public:

	CMole(void);
	~CMole(void);

	/// 
	/// Pozycja X
	int X;

	/// 
	/// Pozycja Y
	int Y;

	/// 
	/// Licznik ruchu
	int timer;

	/// 
	/// Ustalony czas ruchu
	int timer_d;

	/// 
	/// Kierunek ruchu
	int dir;

	void Move(CLevel& map,int direction);
	void Entry(CLevel& map, int destination);
	bool TerrainCheck(int type);
	bool MoveStone(CLevel& map, int direction);
	static bool CheckStone(CLevel& map, int xx, int yy);
};

