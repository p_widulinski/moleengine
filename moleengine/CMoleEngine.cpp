#include "CLevelEditor.h"
#include "AllegroGlobals.h"
#include "CLanguageMgr.h"
#include "CAudioMgr.h"

const float FPS = 60;
const int SCREEN_W = 1024;
const int SCREEN_H = 768;
bool doexit = false;
bool redraw  = false;

bool key[16] = { false, false, false, false, false, false, false, false,false,false, false, false, false, false,false,false  };

int x,y;
string cheats;

string cheat_codes[CH_MAX];
bool cheat_flags[CH_MAX];

ALLEGRO_EVENT_QUEUE *event_queue;
ALLEGRO_EVENT ev;

status_t CMoleEngine::status;
int CMoleEngine::time;
bool next_lev = false; //flaga infurmujaca o przejściu do kolejnego poziomu 

bool fadeToBlack;//flaga informująca o włączeniu animacji przejścia w czerń
float intensity; //szybkość przejścia w czerń
bool fadeDir; //Kierunek przechodzenia - false - do czerni, true - od czerni

bool gameplayInterrupted; // czy gra zostala przerwana przez jeden z ekranow flow?
bool FlowFlag[10]; //Flagi używane do wyświetlania informacji dla użytkownika w trakcie trwania metody FadeToBlack
flow_t FlowFadeFlag; //Flagi uzywana zeby sprawdzic jaki typ Fade To Black jest aktualnie uzywany
CMole CMoleEngine::mole;
CLevel CMoleEngine::lev;
int keys[4];
//TODO:usunąć po stworzneiu obsługi tekstu - PG

int bgScrollX = 0;
int bgScrollY = 0;

///
/// odczytany timer w momencie przejścia na kolejny poziom.
int time_left[2];

CMoleEngine::CMoleEngine(void)
{

}


CMoleEngine::~CMoleEngine(void)
{
}


//////////////////////////////////////////////////////////////////////////
/// Inicjuje działanie aplikacji.
//////////////////////////////////////////////////////////////////////////
void CMoleEngine::Init()
{
	if(!al_init()){
		fprintf(stderr, "Failed to initialize Allegro.\n");
		return ;
	}

	al_init_font_addon();
	al_init_ttf_addon();

	al_install_audio();
	al_init_acodec_addon();
	al_reserve_samples( 20 );

	display = al_create_display(SCREEN_W,SCREEN_H);

	al_set_window_title( display, "MoleEngine (c) " __DATE__ " " __TIME__ " by Fire Team 2015" );


	timer = al_create_timer(1.0 / FPS);

	if (!display){
		fprintf(stderr, "Failed to create display.\n");
		return ;
	}


	if(!al_install_keyboard()) 
	{
		fprintf(stderr, "failed to initialize the keyboard!\n");
		return ;
	}
	if(!timer) 
	{
		fprintf(stderr, "failed to create timer!\n");
		return ;
	}

	al_init_image_addon(); 




	event_queue = al_create_event_queue();

	al_register_event_source(event_queue, al_get_display_event_source(display));
	al_register_event_source(event_queue, al_get_timer_event_source(timer));
	al_register_event_source(event_queue, al_get_keyboard_event_source());
	al_start_timer(timer);

	//TODO: Zmienić inicjacje struktury options, aby byla wczytywana z pliku config - PG
	CMoleEngine::status.current_menu_option = 0;
	CMoleEngine::status.current_window = WIN_MAIN_MENU;
	CMoleEngine::status.diamonds = 0;
	CMoleEngine::status.difficulty = DIF_NORMAL;
	CMoleEngine::status.gamma = 5;
	CMoleEngine::status.music = 0.4f;
	CMoleEngine::status.sfx = 0.4f;
	CMoleEngine::status.sound = 1.0f;
	status.bindingKey = false;
	status.bindingKeyUp = false;
	status.bindingKeyDown = false;
	status.bindingKeyLeft = false;
	status.bindingKeyRight = false;
	status.keyUp = ALLEGRO_KEY_UP;
	status.keyDown = ALLEGRO_KEY_DOWN;
	status.keyLeft = ALLEGRO_KEY_LEFT;
	status.keyRight = ALLEGRO_KEY_RIGHT;
	status.keySuicide = ALLEGRO_KEY_SPACE;

	CAudioMgr::Init();
	CAudioMgr::LoadAudio();
	CAudioMgr::AdjustSfxVolume( status.sfx );
	CAudioMgr::AdjustMusicVolume( status.music );
	CAudioMgr::GetMusicTrack( MUSIC_TEST1 )->PlayLoop();

	CTextureMgr::Init();
	CLanguageMgr::Init();
	CLanguageMgr::lang = LANG_ENG;

	keys[KEY_LEFT] = ALLEGRO_KEY_LEFT;
	keys[KEY_RIGHT] = ALLEGRO_KEY_RIGHT;
	keys[KEY_UP] = ALLEGRO_KEY_UP;
	keys[KEY_DOWN] = ALLEGRO_KEY_DOWN;

	cheat_codes[CH_GOD] = "GODMODE";
	cheat_codes[CH_DRUNK] = "DRUNK";
	cheat_codes[CH_BAD] = "IBADBOY";
}

//////////////////////////////////////////////////////////////////////////
/// Zwraca optymalny limit czasu dla obecnie rozgrywanego poziomu.
/// @return optymalny limit czasu w sekundach
//////////////////////////////////////////////////////////////////////////
int CMoleEngine::GetLevelTimelimit() {
	switch( currentLev.id ) {
	case 400:
	case 401:
	case 402:
		return 3 * 60 + 31;

	default:
		return LEVEL_TIMELIMIT;
	}
}

/*
void CMoleEngine::GetRemainingTime()

*/
//////////////////////////////////////////////////////////////////////////
/// Zwraca ilosc sekund pozostala graczowi na ukonczenie poziomu.
/// @return ilosc sekund pozostala graczowi na ukonczenie poziomu
//////////////////////////////////////////////////////////////////////////
int CMoleEngine::GetRemainingTime() {
	int t = GetLevelTimelimit() - ( GetSeconds() - currentLev.lv_time );
	if( t < 0 ) {
		return 0;
	}

	return t;
}


//////////////////////////////////////////////////////////////////////////
/// Zwraca sekundy od startu aplikacji
/// @return sekundy od startu aplikacji
//////////////////////////////////////////////////////////////////////////
int CMoleEngine::GetSeconds() {
	return time / 60;
}


//////////////////////////////////////////////////////////////////////////
/// Pętla główna aplikacji. 
//////////////////////////////////////////////////////////////////////////
void CMoleEngine::MoleMain()
{
	CRenderer::ClearToBlack();
	time = 0;

	//Petla główna

	while(!doexit)
	{
		al_wait_for_event(event_queue, &ev);

		if(ev.type == ALLEGRO_EVENT_TIMER)
		{
			redraw=true;

			time++; // inkrementowany 60 razy na sekunde

			switch(CMoleEngine::status.current_window)
			{
			case WIN_EDITOR:
				CLevelEditor::EditorKeys(lev);
				break;


			case WIN_GAME_MAIN:

				CMoleEngine::GameKeys();

				break;

			case WIN_SIZE_MENU:

				CLevelEditor::EditSizeKeys(lev);

				break;

			case WIN_LOAD_MENU:

				CLevelEditor::LoadMenuKeys(lev);

				break;				

			case WIN_MAIN_MENU:
				CMoleEngine::MainMenuKeys();
				break;			

			case WIN_CREDITS:
				CMoleEngine::CreditsKeys();
				break;

			case WIN_OPTIONS1:
				CMoleEngine::Options1Keys();
				break;

			case WIN_OPTIONS2:
				CMoleEngine::Options2Keys();
				break;
			}
		}
		else 
			if(ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) 
			{
				break;
			}
			else 
			{
				CInput::Update(ev);
			}


			CMoleEngine::Cheats();

			CMoleEngine::NextLevel();


			if( status.current_window == WIN_GAME_MAIN ) {
				CMoleEngine::CheckLevelTimeout();
			}

			if(!fadeToBlack)
			{
				CMoleEngine::StoneMovement();
				CMoleEngine::ElixirTimer();
			}
			CMoleEngine::MoleTimer();
			CMoleEngine::MoleDeath();

			CMoleEngine::GameEnd();
			CMoleEngine::MoleSuicide();
			
			CMoleEngine::FadeToBlackCounter();
			//Odswiezenie obrazu po obsłużeniu wszystkich zdarzeń o ile jest taka potrzeba
			if(redraw && al_is_event_queue_empty(event_queue)) {

				switch(CMoleEngine::status.current_window)
				{
				case WIN_EDITOR:
					CLevelEditor::EditorTextures(lev);
					break;


				case WIN_GAME_MAIN:
					CMoleEngine::GameTextures();
					CGUI::DrawGUI( WIN_GAME_MAIN );

					break;

				case WIN_MAIN_MENU:
					CGUI::DrawGUI( WIN_MAIN_MENU );
					break;

				case WIN_CREDITS:
					CGUI::DrawGUI( WIN_CREDITS );
					break;

				case WIN_OPTIONS1:
					CGUI::DrawGUI( WIN_OPTIONS1 );
					break;

				case WIN_OPTIONS2:
					CGUI::DrawGUI( WIN_OPTIONS2 );
					break;
				}




				//TODO:Po stworzeniu obsługi napisów usunąć switch poniżej - PG
				switch(CMoleEngine::status.current_window)
				{
				case WIN_EDITOR:
					CLevelEditor::EditorFont();
					break;


				case WIN_GAME_MAIN:
					CMoleEngine::FlowText();
					CGUI::DrawGameplayHUD( true );

					break;

				case WIN_SIZE_MENU:

					CLevelEditor::EditFonts();
					break;

				case WIN_LOAD_MENU:

					CLevelEditor::LoadMenu();

					break;	

				case WIN_CREDITS:
					CGUI::DrawCredits( true );
					break;
				}
				CRenderer::DrawQueue();

				redraw = false;
				al_flip_display();
				CRenderer::ClearToBlack();

			}

	}


	al_destroy_timer(timer);
	al_destroy_display(display);
	al_destroy_event_queue(event_queue);
}


//////////////////////////////////////////////////////////////////////////
/// Ładuje nową grę od pierwszego poziomu.
/// @see difficulty_t
/// @param difficulty poziom trudności
//////////////////////////////////////////////////////////////////////////
void CMoleEngine::StartNewGame(int difficulty)
{
	CMoleEngine::status.current_window = WIN_GAME_MAIN;

	FlowFlag[FL_DEATH] = false;
	FlowFlag[FL_TIME_OUT] = false;
	FlowFlag[FL_GAME_OVER] = false;
	FlowFlag[FL_NEXT_LEVEL] = false;
	FlowFlag[FL_SUICIDE] = false;
	FlowFlag[FL_GAME_END] = false;

	currentLev.lv_time = GetSeconds();
	currentLev.life = 3;
	currentLev.score = 0;
	currentLev.scoreRequiredFor1Up = 2000;
	currentLev.score1UpMsgDisappearTime = 0;

	status.difficulty = difficulty;
	switch(difficulty)
	{
	case DIF_EASY:
		{
			lev = CLevelMgr::LoadLevel("100");
		}
		break;
	case DIF_NORMAL:
		{
			lev = CLevelMgr::LoadLevel("200");
		}
		break;
	case DIF_HARD:
		{
			lev = CLevelMgr::LoadLevel("400");
		}
		break;


	}
	bgScrollX = 0;
	bgScrollY = 0;
	fadeToBlack = true;
	intensity = MAX_INTENSITY;
	fadeDir = true;
	CMoleEngine::ExtendMap();

	currentLev.elixir = EX_NONE;
	currentLev.ex_time = 0;

	CAudioMgr::GetMusicTrack( MUSIC_TEST1 )->Stop();
	CAudioMgr::GetMusicTrack( MUSIC_TEST2 )->PlayLoop();
}


//////////////////////////////////////////////////////////////////////////
/// Uruchamia edytor poziomów.
//////////////////////////////////////////////////////////////////////////
void CMoleEngine::StartLevelEditor()
{
	CMoleEngine::status.current_window = WIN_EDITOR;

	lev = CLevelMgr::LoadLevel("100");
}


//////////////////////////////////////////////////////////////////////////
/// Pokazuje ekran "O autorach".
//////////////////////////////////////////////////////////////////////////
void CMoleEngine::ShowCredits()
{
	CGUI::credits_y_offset = 622;
	CMoleEngine::status.current_window = WIN_CREDITS;
}


//////////////////////////////////////////////////////////////////////////
/// Wyświetla okno opcji.
//////////////////////////////////////////////////////////////////////////
void CMoleEngine::ShowOptions()
{
	CMoleEngine::status.current_window = WIN_OPTIONS1;
	status.current_menu_option = 1;
}


//////////////////////////////////////////////////////////////////////////
/// Metoda obługująca przyciski okna opcji 1.
//////////////////////////////////////////////////////////////////////////
void CMoleEngine::Options1Keys()
{
	if(CInput::GetKeyState(ALLEGRO_KEY_UP))
	{
		CInput::Delay(ALLEGRO_KEY_UP);

		CAudioMgr::GetSfx( SFX_MENU_ON_CLICK )->Play();
		status.current_menu_option--;
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_DOWN))
	{
		CInput::Delay(ALLEGRO_KEY_DOWN);

		CAudioMgr::GetSfx( SFX_MENU_ON_CLICK )->Play();
		status.current_menu_option++;
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_LEFT))
	{
		CInput::Delay(ALLEGRO_KEY_LEFT);

		CAudioMgr::GetSfx( SFX_MENU_ON_CLICK )->Play();

		switch( status.current_menu_option ) {
		case 0:
			// TODO: switch to options2
			break;

		case 1: // language
			if( CLanguageMgr::lang == LANG_POLSKI ) {
				CLanguageMgr::lang = LANG_ENG;
			}
			else {
				CLanguageMgr::lang = LANG_POLSKI;
			}
			break;

		case 2: // sfx volume
			status.sfx -= 0.2f;
			if( status.sfx < 0.0f ) {
				status.sfx = 0.0f;
			}
			CAudioMgr::AdjustSfxVolume( status.sfx );
			break;

		case 3: // music volume
			status.music -= 0.2f;
			if( status.music < 0.0f ) {
				status.music = 0.0f;
			}
			CAudioMgr::AdjustMusicVolume( status.music );
			break;

		case 4: // difficulty
			switch( status.difficulty ) {
			case DIF_EASY:
				status.difficulty = DIF_HARD;
				break;
			case DIF_NORMAL:
				status.difficulty = DIF_EASY;
				break;
			case DIF_HARD:
				status.difficulty = DIF_NORMAL;
				break;
			}
		}
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_RIGHT))
	{
		CInput::Delay(ALLEGRO_KEY_RIGHT);

		CAudioMgr::GetSfx( SFX_MENU_ON_CLICK )->Play();

		switch( status.current_menu_option ) {

		case 1: // language
			if( CLanguageMgr::lang == LANG_POLSKI ) {
				CLanguageMgr::lang = LANG_ENG;
			}
			else {
				CLanguageMgr::lang = LANG_POLSKI;
			}
			break;

		case 2: // sfx volume
			status.sfx += 0.2f;
			if( status.sfx > 1.0f ) {
				status.sfx = 1.0f;
			}
			CAudioMgr::AdjustSfxVolume( status.sfx );

			break;

		case 3: // music volume
			status.music += 0.2f;
			if( status.music > 1.0f ) {
				status.music = 1.0f;
			}
			CAudioMgr::AdjustMusicVolume( status.music );

			break;

		case 4: // difficulty
			switch( status.difficulty ) {
			case DIF_EASY:
				status.difficulty = DIF_NORMAL;
				break;
			case DIF_NORMAL:
				status.difficulty = DIF_HARD;
				break;
			case DIF_HARD:
				status.difficulty = DIF_EASY;
				break;
			}
		}
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_ENTER))
	{
		CInput::Delay(ALLEGRO_KEY_ENTER);

		CAudioMgr::GetSfx( SFX_MENU_ON_CLICK )->Play();

		if( status.current_menu_option == 0 ) {
			CMoleEngine::status.current_window = WIN_OPTIONS2;
			status.current_menu_option = 0;
			return;
		}

	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_ESCAPE))
	{
		CInput::Delay(ALLEGRO_KEY_ESCAPE);
		CMoleEngine::status.current_window = WIN_MAIN_MENU;
	}

	// zabezpiecz przed wyjsciem pozycji menu poza zakres dostepnych opcji - pw
	if( status.current_menu_option < 0 ) {
		status.current_menu_option = 4;
	}
	else if( status.current_menu_option > 4 ) {
		status.current_menu_option = 0;
	}
}


//////////////////////////////////////////////////////////////////////////
/// Mapuje klawisz do czynności okreslonej zmiennymi binding*.
/// @param keynum id klawisza
//////////////////////////////////////////////////////////////////////////
void CMoleEngine::BindKey( int keynum ) {
	if( status.bindingKeyUp ) {
		status.keyUp = keynum;
		status.bindingKeyUp = false;
	}
	else if( status.bindingKeyDown ) {
		status.keyDown = keynum;
		status.bindingKeyDown = false;
	}
	else if( status.bindingKeyLeft ) {
		status.keyLeft = keynum;
		status.bindingKeyLeft = false;
	}
	else if( status.bindingKeyRight ) {
		status.keyRight = keynum;
		status.bindingKeyRight = false;
	}
	else if( status.bindingKeySuicide ) {
		status.keySuicide = keynum;
		status.bindingKeySuicide = false;
	}

	status.bindingKey = false;
}


//////////////////////////////////////////////////////////////////////////
/// Metoda obługująca przyciski okna opcji 2.
//////////////////////////////////////////////////////////////////////////
void CMoleEngine::Options2Keys()
{
	if(CInput::GetKeyState(ALLEGRO_KEY_UP))
	{
		CInput::Delay(ALLEGRO_KEY_UP);

		CAudioMgr::GetSfx( SFX_MENU_ON_CLICK )->Play();

		if( status.bindingKey ) {
			BindKey( ALLEGRO_KEY_UP );
		}
		else {
			status.current_menu_option--;
		}
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_DOWN))
	{
		CInput::Delay(ALLEGRO_KEY_DOWN);

		CAudioMgr::GetSfx( SFX_MENU_ON_CLICK )->Play();

		if( status.bindingKey ) {
			BindKey( ALLEGRO_KEY_DOWN );
		}
		else {
			status.current_menu_option++;
		}
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_LEFT))
	{
		CInput::Delay(ALLEGRO_KEY_LEFT);

		CAudioMgr::GetSfx( SFX_MENU_ON_CLICK )->Play();

		if( status.bindingKey ) {
			BindKey( ALLEGRO_KEY_LEFT );
		}
		else
		{
			switch( status.current_menu_option ) {
			case 0:
				// TODO: switch to options2
				break;

			case 1: // language
				if( CLanguageMgr::lang == LANG_POLSKI ) {
					CLanguageMgr::lang = LANG_ENG;
				}
				else {
					CLanguageMgr::lang = LANG_POLSKI;
				}
				break;

			case 2: // sfx volume
				status.sfx -= 0.2f;
				if( status.sfx < 0.0f ) {
					status.sfx = 0.0f;
				}
				CAudioMgr::AdjustSfxVolume( status.sfx );
				break;

			case 3: // music volume
				status.music -= 0.2f;
				if( status.music < 0.0f ) {
					status.music = 0.0f;
				}
				CAudioMgr::AdjustMusicVolume( status.music );
				break;

			case 4: // difficulty
				switch( status.difficulty ) {
				case DIF_EASY:
					status.difficulty = DIF_HARD;
					break;
				case DIF_NORMAL:
					status.difficulty = DIF_EASY;
					break;
				case DIF_HARD:
					status.difficulty = DIF_NORMAL;
					break;
				}
			}
		}
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_RIGHT))
	{
		CInput::Delay(ALLEGRO_KEY_RIGHT);

		CAudioMgr::GetSfx( SFX_MENU_ON_CLICK )->Play();

		if( status.bindingKey ) {
			BindKey( ALLEGRO_KEY_RIGHT );
		}
		else
		{
			switch( status.current_menu_option ) {
			case 0:
				// TODO: switch to options2
				break;

			case 1: // language
				if( CLanguageMgr::lang == LANG_POLSKI ) {
					CLanguageMgr::lang = LANG_ENG;
				}
				else {
					CLanguageMgr::lang = LANG_POLSKI;
				}
				break;

			case 2: // sfx volume
				status.sfx += 0.2f;
				if( status.sfx > 1.0f ) {
					status.sfx = 1.0f;
				}
				CAudioMgr::AdjustSfxVolume( status.sfx );

				break;

			case 3: // music volume
				status.music += 0.2f;
				if( status.music > 1.0f ) {
					status.music = 1.0f;
				}
				CAudioMgr::AdjustMusicVolume( status.music );

				break;

			case 4: // difficulty
				switch( status.difficulty ) {
				case DIF_EASY:
					status.difficulty = DIF_NORMAL;
					break;
				case DIF_NORMAL:
					status.difficulty = DIF_HARD;
					break;
				case DIF_HARD:
					status.difficulty = DIF_EASY;
					break;
				}
			}
		}
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_ENTER))
	{
		CInput::Delay(ALLEGRO_KEY_ENTER);

		CAudioMgr::GetSfx( SFX_MENU_ON_CLICK )->Play();

		if( status.bindingKey ) {
			BindKey( ALLEGRO_KEY_ENTER );
		}
		else {
			CMoleEngine::status.bindingKey = true;

			if( status.current_menu_option == 0 ) {// up
				CMoleEngine::status.bindingKeyUp = true;
			}
			else if( status.current_menu_option == 1 ) {// down
				CMoleEngine::status.bindingKeyDown = true;
			}
			else if( status.current_menu_option == 2 ) {// left
				CMoleEngine::status.bindingKeyLeft = true;
			}
			else if( status.current_menu_option == 3 ) {// right
				CMoleEngine::status.bindingKeyRight = true;
			}
			else if( status.current_menu_option == 4 ) {// suicide
				CMoleEngine::status.bindingKeySuicide = true;
			}
		}
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_Q))
	{
		CInput::Delay(ALLEGRO_KEY_Q);
		if( status.bindingKey ) {
			BindKey( ALLEGRO_KEY_Q );
		}
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_W))
	{
		CInput::Delay(ALLEGRO_KEY_W);
		if( status.bindingKey ) {
			BindKey( ALLEGRO_KEY_W );
		}
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_E))
	{
		CInput::Delay(ALLEGRO_KEY_E);
		if( status.bindingKey ) {
			BindKey( ALLEGRO_KEY_E );
		}
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_R))
	{
		CInput::Delay(ALLEGRO_KEY_R);
		if( status.bindingKey ) {
			BindKey( ALLEGRO_KEY_R );
		}
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_T))
	{
		CInput::Delay(ALLEGRO_KEY_T);
		if( status.bindingKey ) {
			BindKey( ALLEGRO_KEY_T );
		}
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_Y))
	{
		CInput::Delay(ALLEGRO_KEY_Y);
		if( status.bindingKey ) {
			BindKey( ALLEGRO_KEY_Y );
		}
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_U))
	{
		CInput::Delay(ALLEGRO_KEY_U);
		if( status.bindingKey ) {
			BindKey( ALLEGRO_KEY_U );
		}
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_I))
	{
		CInput::Delay(ALLEGRO_KEY_I);
		if( status.bindingKey ) {
			BindKey( ALLEGRO_KEY_I );
		}
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_O))
	{
		CInput::Delay(ALLEGRO_KEY_O);
		if( status.bindingKey ) {
			BindKey( ALLEGRO_KEY_O );
		}
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_P))
	{
		CInput::Delay(ALLEGRO_KEY_P);
		if( status.bindingKey ) {
			BindKey( ALLEGRO_KEY_P );
		}
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_A))
	{
		CInput::Delay(ALLEGRO_KEY_A);
		if( status.bindingKey ) {
			BindKey( ALLEGRO_KEY_A );
		}
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_S))
	{
		CInput::Delay(ALLEGRO_KEY_S);
		if( status.bindingKey ) {
			BindKey( ALLEGRO_KEY_S );
		}
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_D))
	{
		CInput::Delay(ALLEGRO_KEY_D);
		if( status.bindingKey ) {
			BindKey( ALLEGRO_KEY_D );
		}
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_F))
	{
		CInput::Delay(ALLEGRO_KEY_F);
		if( status.bindingKey ) {
			BindKey( ALLEGRO_KEY_F );
		}
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_G))
	{
		CInput::Delay(ALLEGRO_KEY_G);
		if( status.bindingKey ) {
			BindKey( ALLEGRO_KEY_G );
		}
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_H))
	{
		CInput::Delay(ALLEGRO_KEY_H);
		if( status.bindingKey ) {
			BindKey( ALLEGRO_KEY_H );
		}
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_J))
	{
		CInput::Delay(ALLEGRO_KEY_J);
		if( status.bindingKey ) {
			BindKey( ALLEGRO_KEY_J );
		}
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_K))
	{
		CInput::Delay(ALLEGRO_KEY_K);
		if( status.bindingKey ) {
			BindKey( ALLEGRO_KEY_K );
		}
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_L))
	{
		CInput::Delay(ALLEGRO_KEY_L);
		if( status.bindingKey ) {
			BindKey( ALLEGRO_KEY_L );
		}
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_Z))
	{
		CInput::Delay(ALLEGRO_KEY_Z);
		if( status.bindingKey ) {
			BindKey( ALLEGRO_KEY_Z );
		}
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_X))
	{
		CInput::Delay(ALLEGRO_KEY_X);
		if( status.bindingKey ) {
			BindKey( ALLEGRO_KEY_X );
		}
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_C))
	{
		CInput::Delay(ALLEGRO_KEY_C);
		if( status.bindingKey ) {
			BindKey( ALLEGRO_KEY_C );
		}
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_V))
	{
		CInput::Delay(ALLEGRO_KEY_V);
		if( status.bindingKey ) {
			BindKey( ALLEGRO_KEY_V );
		}
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_B))
	{
		CInput::Delay(ALLEGRO_KEY_B);
		if( status.bindingKey ) {
			BindKey( ALLEGRO_KEY_B );
		}
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_N))
	{
		CInput::Delay(ALLEGRO_KEY_N);
		if( status.bindingKey ) {
			BindKey( ALLEGRO_KEY_N );
		}
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_M))
	{
		CInput::Delay(ALLEGRO_KEY_M);
		if( status.bindingKey ) {
			BindKey( ALLEGRO_KEY_M );
		}
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_SPACE))
	{
		CInput::Delay(ALLEGRO_KEY_SPACE);
		if( status.bindingKey ) {
			BindKey( ALLEGRO_KEY_SPACE );
		}
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_ESCAPE))
	{
		CInput::Delay(ALLEGRO_KEY_ESCAPE);

		status.bindingKey = false;
		status.bindingKeyUp = false;
		status.bindingKeyDown = false;
		status.bindingKeyLeft = false;
		status.bindingKeyRight = false;
		status.bindingKeySuicide = false;

		status.current_window = WIN_OPTIONS1;
		status.current_menu_option = 0;
	}

	// zabezpiecz przed wyjsciem pozycji menu poza zakres dostepnych opcji - pw
	if( status.current_menu_option < 0 ) {
		status.current_menu_option = 4;
	}
	else if( status.current_menu_option > 4 ) {
		status.current_menu_option = 4;
	}
}


//////////////////////////////////////////////////////////////////////////
/// Metoda obługująca przyciski okna menu głównego.
//////////////////////////////////////////////////////////////////////////
void CMoleEngine::MainMenuKeys()
{
	if(CInput::GetKeyState(ALLEGRO_KEY_UP))
	{
		CInput::Delay(ALLEGRO_KEY_UP);

		CAudioMgr::GetSfx( SFX_MENU_ON_CLICK )->Play();
		status.current_menu_option--;
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_DOWN))
	{
		CInput::Delay(ALLEGRO_KEY_DOWN);

		CAudioMgr::GetSfx( SFX_MENU_ON_CLICK )->Play();
		status.current_menu_option++;
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_E))
	{
		CAudioMgr::GetSfx( SFX_MENU_ON_CLICK )->Play();

		CInput::Delay(ALLEGRO_KEY_E);
		StartLevelEditor();
	}

	else if(CInput::GetKeyState(ALLEGRO_KEY_ENTER))
	{
		CInput::Delay(ALLEGRO_KEY_ENTER);

		CAudioMgr::GetSfx( SFX_MENU_ON_CLICK )->Play();

		switch( status.current_menu_option ) {
		case 0: // play
			StartNewGame(status.difficulty);
			break;
		case 1: // options
			ShowOptions();
			break;
		case 2: // credits
			ShowCredits();
			break;
		case 3: // quit
			doexit = true;
			break;
		}

	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_ESCAPE))
	{
		CInput::Delay(ALLEGRO_KEY_ESCAPE);
		//TODO:usunac doexit kiedy beda skonczone pozostale okna - PG
		doexit = true;
	}

	// zabezpiecz przed wyjsciem pozycji menu poza zakres dostepnych opcji - pw
	if( status.current_menu_option < 0 ) {
		status.current_menu_option = 3;
	}
	else if( status.current_menu_option > 3 ) {
		status.current_menu_option = 0;
	}
}


//////////////////////////////////////////////////////////////////////////
/// Metoda obługująca przyciski okna "O autorach".
//////////////////////////////////////////////////////////////////////////
void CMoleEngine::CreditsKeys()
{
	if(CInput::GetKeyState(ALLEGRO_KEY_ENTER))
	{
		CInput::Delay(ALLEGRO_KEY_ENTER);
		CMoleEngine::status.current_window = WIN_MAIN_MENU;
	}
	else if( CInput::GetKeyState(ALLEGRO_KEY_ESCAPE) ) {
		CInput::Delay(ALLEGRO_KEY_ESCAPE);
		CMoleEngine::status.current_window = WIN_MAIN_MENU;
	}
}

void CMoleEngine::Cmd_MoveUp() {
	if(mole.Y > 0 && mole.dir == MOV_NONE)
	{
		mole.Move(lev,MOV_UP);
	}
}

void CMoleEngine::Cmd_MoveDown() {
	if(mole.Y < lev.rows && mole.dir == MOV_NONE)
	{
		mole.Move(lev,MOV_DOWN);
	}
}

void CMoleEngine::Cmd_MoveLeft() {
	if(mole.X > 0 && mole.dir == MOV_NONE)
	{
		mole.Move(lev,MOV_LEFT);
	}
}

void CMoleEngine::Cmd_MoveRight() {
	if(mole.X < lev.columns && mole.dir == MOV_NONE)
	{
		mole.Move(lev,MOV_RIGHT);
	}
}

void CMoleEngine::Cmd_CommitSuicide() {
	FlowFadeFlag = FL_SUICIDE;
}

//////////////////////////////////////////////////////////////////////////
/// Wykonuje akcję dla podanego klawisza.
/// @param keynum numer klawisza
/// @return 1 jeśli ma nie resetować klawisza, 2 jeśli ma resetować, 0 jeśli brak akcji
//////////////////////////////////////////////////////////////////////////
int CMoleEngine::ActionForKeyBinding( int keynum ) {
	if( status.keyUp == keynum ) {
		Cmd_MoveUp();
		return 1;
	}
	else if( status.keyDown == keynum ) {
		Cmd_MoveDown();
		return 1;
	}
	else if( status.keyLeft == keynum ) {
		Cmd_MoveLeft();
		return 1;
	}
	else if( status.keyRight == keynum ) {
		Cmd_MoveRight();
		return 1;
	}
	else if( status.keySuicide == keynum ) {
		Cmd_CommitSuicide();
		return 2;
	}

	return 0;
}

//////////////////////////////////////////////////////////////////////////
/// Metoda obługująca przyciski okna rozgrywki własciwej
//////////////////////////////////////////////////////////////////////////
void CMoleEngine::GameKeys()
{
	if(FlowFlag[FL_DEATH] || FlowFlag[FL_TIME_OUT] || FlowFlag[FL_GAME_OVER] || FlowFlag[FL_NEXT_LEVEL] || FlowFlag[FL_SUICIDE] )
	{

		if(CInput::GetKeyState(ALLEGRO_KEY_ENTER))
		{
			CInput::Delay(ALLEGRO_KEY_ENTER);
			FlowFlag[FL_DEATH] = false;
			FlowFlag[FL_TIME_OUT] = false;
			FlowFlag[FL_GAME_OVER] = false;
			FlowFlag[FL_NEXT_LEVEL] = false;
			FlowFlag[FL_SUICIDE] = false;
			FlowFlag[FL_GAME_END] = false;
			intensity++;
		}
	}

	if(FlowFlag[FL_GAME_END] )
	{
		if(CInput::GetKeyState(ALLEGRO_KEY_ENTER))
		{
			CInput::Delay(ALLEGRO_KEY_ENTER);
			FlowFlag[FL_DEATH] = false;
			FlowFlag[FL_TIME_OUT] = false;
			FlowFlag[FL_GAME_OVER] = false;
			FlowFlag[FL_NEXT_LEVEL] = false;
			FlowFlag[FL_SUICIDE] = false;
			FlowFlag[FL_GAME_END] = false;

			fadeDir = false;
			fadeToBlack = false;
			FlowFadeFlag = FL_NONE;
			intensity = 0;
			next_lev = false;
			status.current_window = WIN_MAIN_MENU;
			CAudioMgr::GetMusicTrack( MUSIC_TEST2 )->Stop();
			CAudioMgr::GetMusicTrack( MUSIC_TEST1 )->PlayLoop();
		}

	}

	if(!fadeToBlack)
	{
		if(CInput::GetKeyState(keys[KEY_UP]))
		{
			ActionForKeyBinding( ALLEGRO_KEY_UP );
		}
		else if(CInput::GetKeyState(keys[KEY_DOWN]))
		{
			ActionForKeyBinding( ALLEGRO_KEY_DOWN );
		}
		else if(CInput::GetKeyState(keys[KEY_LEFT]))
		{
			ActionForKeyBinding( ALLEGRO_KEY_LEFT );
		}
		else if(CInput::GetKeyState(keys[KEY_RIGHT]))
		{
			ActionForKeyBinding( ALLEGRO_KEY_RIGHT );
		}
		else if(CInput::GetKeyState(ALLEGRO_KEY_SPACE))
		{
			if( ActionForKeyBinding( ALLEGRO_KEY_SPACE ) != 1 ) {
				CInput::Delay(ALLEGRO_KEY_SPACE);
			}
		}
		else if(CInput::GetKeyState(ALLEGRO_KEY_ENTER))
		{
			if( ActionForKeyBinding( ALLEGRO_KEY_ENTER ) != 1 ) {
				CInput::Delay(ALLEGRO_KEY_ENTER);
			}
		}
		else if(CInput::GetKeyState(ALLEGRO_KEY_Q))
		{
			if( ActionForKeyBinding( ALLEGRO_KEY_Q ) != 1 ) {
				CInput::Delay(ALLEGRO_KEY_Q);
			}
			cheatInput("Q");
		}
		else if(CInput::GetKeyState(ALLEGRO_KEY_W))
		{
			if( ActionForKeyBinding( ALLEGRO_KEY_W ) != 1 ) {
				CInput::Delay(ALLEGRO_KEY_W);
			}
			cheatInput("W");
		}
		else if(CInput::GetKeyState(ALLEGRO_KEY_E))
		{
			if( ActionForKeyBinding( ALLEGRO_KEY_E ) != 1 ) {
				CInput::Delay(ALLEGRO_KEY_E);
			}
			cheatInput("E");
		}
		else if(CInput::GetKeyState(ALLEGRO_KEY_R))
		{
			if( ActionForKeyBinding( ALLEGRO_KEY_R ) != 1 ) {
				CInput::Delay(ALLEGRO_KEY_R);
			}
			cheatInput("R");
		}
		else if(CInput::GetKeyState(ALLEGRO_KEY_T))
		{
			if( ActionForKeyBinding( ALLEGRO_KEY_T ) != 1 ) {
				CInput::Delay(ALLEGRO_KEY_T);
			}
			cheatInput("T");
		}
		else if(CInput::GetKeyState(ALLEGRO_KEY_Y))
		{
			if( ActionForKeyBinding( ALLEGRO_KEY_Y ) != 1 ) {
				CInput::Delay(ALLEGRO_KEY_Y);
			}
			cheatInput("Y");
		}
		else if(CInput::GetKeyState(ALLEGRO_KEY_U))
		{
			if( ActionForKeyBinding( ALLEGRO_KEY_U ) != 1 ) {
				CInput::Delay(ALLEGRO_KEY_U);
			}
			cheatInput("U");
		}
		else if(CInput::GetKeyState(ALLEGRO_KEY_I))
		{
			if( ActionForKeyBinding( ALLEGRO_KEY_I ) != 1 ) {
				CInput::Delay(ALLEGRO_KEY_I);
			}
			cheatInput("I");
		}
		else if(CInput::GetKeyState(ALLEGRO_KEY_O))
		{
			if( ActionForKeyBinding( ALLEGRO_KEY_O ) != 1 ) {
				CInput::Delay(ALLEGRO_KEY_O);
			}
			cheatInput("O");
		}
		else if(CInput::GetKeyState(ALLEGRO_KEY_P))
		{
			if( ActionForKeyBinding( ALLEGRO_KEY_P ) != 1 ) {
				CInput::Delay(ALLEGRO_KEY_P);
			}
			cheatInput("P");
		}
		else if(CInput::GetKeyState(ALLEGRO_KEY_A))
		{
			if( ActionForKeyBinding( ALLEGRO_KEY_A ) != 1 ) {
				CInput::Delay(ALLEGRO_KEY_A);
			}
			cheatInput("A");
		}
		else if(CInput::GetKeyState(ALLEGRO_KEY_S))
		{
			if( ActionForKeyBinding( ALLEGRO_KEY_S ) != 1 ) {
				CInput::Delay(ALLEGRO_KEY_S);
			}
			cheatInput("S");
		}
		else if(CInput::GetKeyState(ALLEGRO_KEY_D))
		{
			if( ActionForKeyBinding( ALLEGRO_KEY_D ) != 1 ) {
				CInput::Delay(ALLEGRO_KEY_D);
			}
			cheatInput("D");
		}
		else if(CInput::GetKeyState(ALLEGRO_KEY_F))
		{
			if( ActionForKeyBinding( ALLEGRO_KEY_F ) != 1 ) {
				CInput::Delay(ALLEGRO_KEY_F);
			}
			cheatInput("F");
		}
		else if(CInput::GetKeyState(ALLEGRO_KEY_G))
		{
			if( ActionForKeyBinding( ALLEGRO_KEY_G ) != 1 ) {
				CInput::Delay(ALLEGRO_KEY_G);
			}
			cheatInput("G");
		}
		else if(CInput::GetKeyState(ALLEGRO_KEY_H))
		{
			if( ActionForKeyBinding( ALLEGRO_KEY_H ) != 1 ) {
				CInput::Delay(ALLEGRO_KEY_H);
			}
			cheatInput("H");
		}
		else if(CInput::GetKeyState(ALLEGRO_KEY_J))
		{
			if( ActionForKeyBinding( ALLEGRO_KEY_J ) != 1 ) {
				CInput::Delay(ALLEGRO_KEY_J);
			}
			cheatInput("J");
		}
		else if(CInput::GetKeyState(ALLEGRO_KEY_K))
		{
			if( ActionForKeyBinding( ALLEGRO_KEY_K ) != 1 ) {
				CInput::Delay(ALLEGRO_KEY_K);
			}
			cheatInput("K");
		}
		else if(CInput::GetKeyState(ALLEGRO_KEY_L))
		{
			if( ActionForKeyBinding( ALLEGRO_KEY_L ) != 1 ) {
				CInput::Delay(ALLEGRO_KEY_L);
			}
			cheatInput("L");
		}
		else if(CInput::GetKeyState(ALLEGRO_KEY_Z))
		{
			if( ActionForKeyBinding( ALLEGRO_KEY_Z ) != 1 ) {
				CInput::Delay(ALLEGRO_KEY_Z);
			}
			cheatInput("Z");
		}
		else if(CInput::GetKeyState(ALLEGRO_KEY_X))
		{
			if( ActionForKeyBinding( ALLEGRO_KEY_X ) != 1 ) {
				CInput::Delay(ALLEGRO_KEY_X);
			}
			cheatInput("X");
		}
		else if(CInput::GetKeyState(ALLEGRO_KEY_C))
		{
			if( ActionForKeyBinding( ALLEGRO_KEY_C ) != 1 ) {
				CInput::Delay(ALLEGRO_KEY_C);
			}
			cheatInput("C");
		}
		else if(CInput::GetKeyState(ALLEGRO_KEY_V))
		{
			if( ActionForKeyBinding( ALLEGRO_KEY_V ) != 1 ) {
				CInput::Delay(ALLEGRO_KEY_V);
			}
			cheatInput("V");
		}
		else if(CInput::GetKeyState(ALLEGRO_KEY_B))
		{
			if( ActionForKeyBinding( ALLEGRO_KEY_B ) != 1 ) {
				CInput::Delay(ALLEGRO_KEY_B);
			}
			cheatInput("B");
		}
		else if(CInput::GetKeyState(ALLEGRO_KEY_N))
		{
			if( ActionForKeyBinding( ALLEGRO_KEY_N ) != 1 ) {
				CInput::Delay(ALLEGRO_KEY_N);
			}
			cheatInput("N");
		}
		else if(CInput::GetKeyState(ALLEGRO_KEY_M))
		{
			if( ActionForKeyBinding( ALLEGRO_KEY_M ) != 1 ) {
				CInput::Delay(ALLEGRO_KEY_M);
			}
			cheatInput("M");
		}
		else if(CInput::GetKeyState(ALLEGRO_KEY_ESCAPE))
		{
			CInput::Delay(ALLEGRO_KEY_ESCAPE);

			// powrót do głównego menu - pw
			status.current_window = WIN_MAIN_MENU;

			CAudioMgr::GetMusicTrack( MUSIC_TEST2 )->Stop();
			CAudioMgr::GetMusicTrack( MUSIC_TEST1 )->PlayLoop();
		}
	}
}


//////////////////////////////////////////////////////////////////////////
/// Metoda dodająca tekstury okna rozgrywki własciwej do kolejki rysowania
//////////////////////////////////////////////////////////////////////////
void CMoleEngine::GameTextures()
{
	int temp=0;
	int y_offset = 130; // offset y ze wzgledu na hud - pw

	int mole_offset_y = 0; //offset dla rysowania elementw mapy, ze względu na przemieszczanie się kreta
	int mole_offset_x = 0;





	switch(mole.dir)
	{
	case MOV_UP:
		{
			mole_offset_y = -SCREEN_CENTER_HEIGHT + 32*mole.Y + mole.timer*(32/mole.timer_d);
			mole_offset_x = -SCREEN_CENTER_WIDTH + 32*mole.X;
			break;
		}
	case MOV_DOWN:
		{
			mole_offset_y = -SCREEN_CENTER_HEIGHT + 32*mole.Y- mole.timer*(32/mole.timer_d);
			mole_offset_x = -SCREEN_CENTER_WIDTH + 32*mole.X;
			break;
		}
	case MOV_LEFT:
		{
			mole_offset_x = -SCREEN_CENTER_WIDTH + 32*mole.X + mole.timer*(32/mole.timer_d);
			mole_offset_y = -SCREEN_CENTER_HEIGHT + 32*mole.Y;
			break;
		}
	case MOV_RIGHT:
		{
			mole_offset_x = -SCREEN_CENTER_WIDTH + 32*mole.X - mole.timer*(32/mole.timer_d);
			mole_offset_y = -SCREEN_CENTER_HEIGHT + 32*mole.Y;
			break;
		}
	case MOV_NONE:
		{
			mole_offset_x = -SCREEN_CENTER_WIDTH + 32*mole.X;
			mole_offset_y = -SCREEN_CENTER_HEIGHT + 32*mole.Y;
			break;
		}
	default:
		{
			mole_offset_x = -SCREEN_CENTER_WIDTH + 32*mole.X;
			mole_offset_y = -SCREEN_CENTER_HEIGHT + 32*mole.Y;
			break;
		}
	}

	if(DARKNESS)
	{
		for(int i = -SCREEN_CENTER_HEIGHT/32;i < lev.columns+20-(SCREEN_CENTER_HEIGHT/32); i++)
		{
			for(int j = -SCREEN_CENTER_WIDTH/32;j < lev.rows+32-(SCREEN_CENTER_WIDTH/32); j++)
			{

				if(j < 0 || j > lev.rows-1 || i < 0 || i > lev.columns-1)
				{
					CRenderer::AddToDrawQueue(TEX_BLACK,j*32-mole_offset_x,y_offset + i*32-mole_offset_y);
				}



			}

		}
	}
	for(int i = 0;i < lev.columns; i++)
	{
		for(int j = 0;j < lev.rows; j++)
		{

			temp = lev.rows*i+j;
			if(lev.field[temp] > 0)
			{

				CRenderer::AddToDrawQueue(lev.field[temp],j*32-mole_offset_x,y_offset + i*32-mole_offset_y);
			}
			else
			{

				CRenderer::AddToDrawQueue(TEX_EARTH_BG,j*32-mole_offset_x,y_offset + i*32-mole_offset_y);
			}
			if(DARKNESS)
			{
				if(i == 0)
				{
					CRenderer::AddToDrawQueue(TEX_BLACK_UP,j*32-mole_offset_x,y_offset + i*32-mole_offset_y);
				}

				if(j == 0)
				{
					CRenderer::AddToDrawQueue(TEX_BLACK_LEFT,j*32-mole_offset_x,y_offset + i*32-mole_offset_y);
				}

				if(i == lev.columns-1)
				{
					CRenderer::AddToDrawQueue(TEX_BLACK_DOWN,j*32-mole_offset_x,y_offset + i*32-mole_offset_y);
				}

				if(j == lev.rows-1)
				{
					CRenderer::AddToDrawQueue(TEX_BLACK_RIGHT,j*32-mole_offset_x,y_offset + i*32-mole_offset_y);
				}
			}
		}
	}
	for( int i = 0; i < lev.stoneList.size(); i++)
	{
		switch(lev.stoneList[i].direction)
		{
		case MOV_DOWN:
			CRenderer::AddToDrawQueue(TER_STONE,lev.stoneList[i].x*32-mole_offset_x,y_offset + lev.stoneList[i].y*32-mole_offset_y-(lev.stoneList[i].timer*(32/lev.stoneList[i].timer_d)));
			break;
		case MOV_LEFT:
			CRenderer::AddToDrawQueue(TER_STONE,lev.stoneList[i].x*32-mole_offset_x+(lev.stoneList[i].timer*(32/lev.stoneList[i].timer_d)),y_offset -mole_offset_y + lev.stoneList[i].y*32);
			break;
		case MOV_RIGHT:
			CRenderer::AddToDrawQueue(TER_STONE,lev.stoneList[i].x*32-mole_offset_x-(lev.stoneList[i].timer*(32/lev.stoneList[i].timer_d)),y_offset -mole_offset_y + lev.stoneList[i].y*32);
			break;
		default:
			CRenderer::AddToDrawQueue(TER_STONE,lev.stoneList[i].x*32-mole_offset_x,y_offset + lev.stoneList[i].y*32-mole_offset_y);
			break;

		}
	}

	CRenderer::AddToDrawQueue(TEX_MOLE,SCREEN_CENTER_WIDTH,y_offset + SCREEN_CENTER_HEIGHT);

	if(fadeToBlack)
	{

		for(int i = 0; i < intensity; i++)
		{
			CRenderer::AddToDrawQueue(TEX_FADE_TO_BLACK,0,y_offset);
		}
	}
	if(FlowFlag[FL_NEXT_LEVEL])
	{
		if(currentLev.diamond_total == currentLev.diamond)
		{
			CRenderer::AddToDrawQueue(TEX_TROPHY,512-75,250);
		}
	}
}


//////////////////////////////////////////////////////////////////////////
/// Metoda obługująca przejście do kolejnego poziomu.
//////////////////////////////////////////////////////////////////////////
void CMoleEngine::NextLevel()
{
	if(FlowFadeFlag != FL_GAME_END)
	{
		if(FlowFadeFlag == FL_NEXT_LEVEL || FlowFadeFlag == FL_NONE)
		{
			if(next_lev)
			{

				if( !fadeToBlack ) {
					if(currentLev.diamond_total == currentLev.diamond) {
						CAudioMgr::GetSfx( SFX_PERFECT_LEVEL_COMPLETION )->Play();
					}
					else {
						CAudioMgr::GetSfx( SFX_LEVEL_COMPLETE )->Play();
					}
				}

				if(currentLev.id+1 == 100+LAST_LEVEL || currentLev.id+1 == 200+LAST_LEVEL || currentLev.id+1 == 300+LAST_LEVEL)
				{
					FlowFadeFlag = FL_GAME_END;
				}
				else
				{
					fadeToBlack = true;
					FlowFadeFlag = FL_NEXT_LEVEL;

					if(intensity == 1)
					{
						time_left[0] = CMoleEngine::GetRemainingTime() % 60;
						time_left[1] = CMoleEngine::GetRemainingTime() / 60;

						if(cheat_flags[CH_BAD])
						{							
							AddScore( -5000 );
						}
						else
						{
							AddScore( (time_left[1]*60*2 + time_left[0])*25 );
						}
					}
					if(intensity == MAX_INTENSITY-1)
					{
						FlowFlag[FL_NEXT_LEVEL] = true;

					}

					if(intensity == MAX_INTENSITY)
					{
						CLevel temp(0,0);
						char tempC[4];
						itoa(currentLev.id+1,tempC,10);
						temp = CLevelMgr::LoadLevel(tempC);
						if(temp.columns > 1 && temp.rows > 1)
						{
							lev = temp;
						}

						currentLev.lv_time = GetSeconds();
						next_lev = false;
						bgScrollX = 0;
						bgScrollY = 0;
						CMoleEngine::ExtendMap();

						currentLev.ex_time = 0;
						currentLev.elixir = EX_NONE;
						cheat_flags[CH_BAD] = false;
						CAudioMgr::GetMusicTrack(MUSIC_TEST2)->SetSpeed( 1.0f );
					}


				}
			}
		}	
	}
}

//////////////////////////////////////////////////////////////////////////
/// Metoda obługująca timer poziomu.
//////////////////////////////////////////////////////////////////////////
void CMoleEngine::CheckLevelTimeout()
{
	if(FlowFadeFlag == FL_TIME_OUT || FlowFadeFlag == FL_NONE)
	{

		if( GetRemainingTime() <= 0 )
		{

			if( currentLev.life > 1 )
			{

				fadeToBlack = true;
				FlowFadeFlag = FL_TIME_OUT;

				if(intensity == MAX_INTENSITY/2)
				{
					FlowFlag[FL_TIME_OUT] = true;

				}

				if(intensity == MAX_INTENSITY)
				{

					currentLev.life--;
					RestartLevel();
				}
			}
			else
			{

				fadeToBlack = true;

				if(intensity == MAX_INTENSITY/3)
				{
					FlowFlag[FL_GAME_OVER] = true;
				}

				if(intensity == MAX_INTENSITY)
				{
					fadeDir = false;
					fadeToBlack = false;
					intensity = 0;
					status.current_window = WIN_MAIN_MENU;
					mole.Y=-1;
					mole.X =-1;
					FlowFlag[FL_GAME_OVER] = false;
					CAudioMgr::GetMusicTrack( MUSIC_TEST2 )->Stop();
					CAudioMgr::GetMusicTrack( MUSIC_TEST1 )->PlayLoop();
				}

			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////
/// Powoduje restart obecnie rozgrywanego poziomu.
//////////////////////////////////////////////////////////////////////////
void CMoleEngine::RestartLevel()
{

	CLevel temp(0,0);
	char tempC[4];
	itoa(currentLev.id,tempC,10);
	temp = CLevelMgr::LoadLevel(tempC);
	if(temp.columns > 1 && temp.rows > 1)
	{
		lev = temp;
	}

	currentLev.lv_time = GetSeconds();
	next_lev = false;

	bgScrollX = 0;
	bgScrollY = 0;
	CMoleEngine::ExtendMap();


	currentLev.ex_time = 0;
	currentLev.elixir = EX_NONE;
	CAudioMgr::GetMusicTrack(MUSIC_TEST2)->SetSpeed( 1.0f );

}


//////////////////////////////////////////////////////////////////////////
/// Metoda odpowiedzialna za ruch wszystkich kamieni
//////////////////////////////////////////////////////////////////////////
void CMoleEngine::StoneMovement()
{
	if(currentLev.elixir != EX_TIME_STOP)
	{
		for (int i = 0; i < lev.stoneList.size();i++)
		{
			if(lev.stoneList[i].timer == 0)
			{

				CheckHold(i);

				if(lev.field[(lev.stoneList[i].x+0)+(lev.stoneList[i].y+1) *lev.rows] == TER_NONE 
					&& !CMole::CheckStone(lev,lev.stoneList[i].x+0,lev.stoneList[i].y+1)
					&& lev.stoneList[i].hold != MOV_DOWN
					)
				{

					lev.stoneList[i].y++;
					lev.stoneList[i].timer = lev.stoneList[i].timer_d;
					lev.stoneList[i].direction = MOV_DOWN;

					CAudioMgr::GetSfx( SFX_ON_STONE_TUMBLE )->PlayNoInterrupt();
				}


				if(lev.field[(lev.stoneList[i].x-1)+(lev.stoneList[i].y+0) *lev.rows] == TER_NONE 
					&& !CMole::CheckStone(lev,lev.stoneList[i].x-1,lev.stoneList[i].y+0) 
					&& lev.stoneList[i].direction == MOV_NONE

					)
				{

					if(lev.stoneList[i].hold == MOV_NONE)
					{
						if(lev.field[(lev.stoneList[i].x-1)+(lev.stoneList[i].y+1) *lev.rows] == TER_NONE)
						{
							if( !CMole::CheckStone(lev,lev.stoneList[i].x-1,lev.stoneList[i].y+1) )

							{

								lev.stoneList[i].x--;
								lev.stoneList[i].timer = lev.stoneList[i].timer_d;
								lev.stoneList[i].direction = MOV_LEFT;

							}
						}
					}

				}

				if(lev.field[(lev.stoneList[i].x+1)+(lev.stoneList[i].y+0) *lev.rows] == TER_NONE 
					&& !CMole::CheckStone(lev,lev.stoneList[i].x+1,lev.stoneList[i].y+0)
					&& lev.stoneList[i].direction == MOV_NONE
					)
				{

					if(lev.stoneList[i].hold == MOV_NONE)
					{
						if(lev.field[(lev.stoneList[i].x+1)+(lev.stoneList[i].y+1) *lev.rows] == TER_NONE)
						{
							if( !CMole::CheckStone(lev,lev.stoneList[i].x+1,lev.stoneList[i].y+1) )

							{

								lev.stoneList[i].x++;
								lev.stoneList[i].timer = lev.stoneList[i].timer_d;
								lev.stoneList[i].direction = MOV_RIGHT;
							}
						}
					}
				}


			}
			else
			{
				lev.stoneList[i].timer--;
				if(lev.stoneList[i].timer==0)
				{
					lev.stoneList[i].direction = MOV_NONE;
				}
			}
		}
	}
}


//////////////////////////////////////////////////////////////////////////
/// Metoda obługująca licznik eliksirów.
//////////////////////////////////////////////////////////////////////////
void CMoleEngine::ElixirTimer()
{

	if(currentLev.elixir == EX_TIME_STOP)
	{
		currentLev.ex_time--;
		printf("%i\n",currentLev.ex_time);
	}
	if(currentLev.ex_time == 0)
	{
		currentLev.elixir = EX_NONE;
		CAudioMgr::GetMusicTrack(MUSIC_TEST2)->SetSpeed( 1.0f );
	}
}


//////////////////////////////////////////////////////////////////////////
/// Metoda dodająca niezniszczalne ściany na krawędziach mapy. Dodaje obiekty kamieni do listy, zlicza parametry mapy i przekazuje je do struktury currentLevel
/// @see currentLevel_t
//////////////////////////////////////////////////////////////////////////
void CMoleEngine::ExtendMap()
{
	printf("\n");
	printf("\n");
	CLevel temp(lev.rows+2,lev.columns+2);

	CStone tempStone;
	tempStone.x = 0;
	tempStone.y = 0;

	for(int i=0; i < temp.columns; i++)
	{
		for(int j=0;j < temp.rows;j++)
		{
			temp.field[(i)*(temp.rows)+(j)] = TER_SOIL;


			if(i == 0 || j == 0 || i == temp.columns-1 || j == temp.rows-1)
			{
				temp.field[(i)*(temp.rows)+(j)] = TER_WALL_BORDER;
			}
			else
			{
				temp.field[(i)*(temp.rows)+(j)] = lev.field[(i-1)*lev.rows+(j-1)];
			}


			printf("%i ",temp.field[i*lev.rows+j]);

			if(temp.field[(i)*(temp.rows)+(j)] == TER_STONE)
			{
				tempStone.x = j;
				tempStone.y = i;
				temp.stoneList.push_back(tempStone);
				temp.field[(i)*(temp.rows)+(j)] = TER_NONE;

			}
			else if(temp.field[(i)*(temp.rows)+(j)] == TER_WORM)
			{

				temp.worms++;
			}
			else if(temp.field[(i)*(temp.rows)+(j)] == TER_DIAMOND)
			{

				temp.diamonds++;
			}
			else if(temp.field[(i)*(temp.rows)+(j)] == TER_ENTRY)
			{
				mole.X = j;
				mole.Y = i;
			}

		}
		printf("\n");
	}
	currentLev.worm_total = temp.worms;
	currentLev.diamond_total = temp.diamonds;
	currentLev.worm = 0;
	currentLev.diamond = 0;
	lev = temp;
}


//////////////////////////////////////////////////////////////////////////
/// Metoda obługująca licznik gracza potrzebny do płynnego poruszania się (animacji)
//////////////////////////////////////////////////////////////////////////
void CMoleEngine::MoleTimer()
{
	if(mole.dir != MOV_NONE)
	{
		mole.timer--;
		if(mole.timer == 0)
		{
			mole.dir = MOV_NONE;
		}
	}

}


//////////////////////////////////////////////////////////////////////////
/// Metoda sprawdzająca, czy kamień nie jest trzymany przez gracza
/// @param stone numer kamienia z listy
//////////////////////////////////////////////////////////////////////////
void CMoleEngine::CheckHold(int stone)
{
	if(lev.stoneList[stone].timer == 0)
	{

		//MOV_DOWN

		if( lev.stoneList[stone].x == mole.X 
			&& lev.stoneList[stone].y+1 == mole.Y)
		{
			if(mole.dir == MOV_UP || mole.dir == MOV_LEFT || mole.dir == MOV_RIGHT)
			{

				lev.stoneList[stone].hold = MOV_DOWN;

			}
		}


		//MOV_LEFT
		else if( lev.stoneList[stone].x-1 == mole.X 
			&& lev.stoneList[stone].y == mole.Y
			||
			lev.stoneList[stone].x-1 == mole.X 
			&& lev.stoneList[stone].y+1 == mole.Y 
			)
		{
			if(mole.dir == MOV_UP || mole.dir == MOV_DOWN || mole.dir == MOV_RIGHT)
			{

				lev.stoneList[stone].hold = MOV_LEFT;

			}
		}

		//MOV_RIGHT
		else if( lev.stoneList[stone].x+1 == mole.X 
			&& lev.stoneList[stone].y == mole.Y
			||
			lev.stoneList[stone].x+1 == mole.X 
			&& lev.stoneList[stone].y+1 == mole.Y 
			)
		{
			if(mole.dir == MOV_UP || mole.dir == MOV_DOWN ||  mole.dir == MOV_LEFT)
			{

				lev.stoneList[stone].hold = MOV_RIGHT;

			}
		}

		else
		{
			lev.stoneList[stone].hold = MOV_NONE;
		}



	}
}

//TODO: Metoda na potrzeby developmentu, usunać w finalnej wersji - PG

//////////////////////////////////////////////////////////////////////////
/// Uruchamia poziom do testów.
/// @param name nazwa poziomu
//////////////////////////////////////////////////////////////////////////
void CMoleEngine::TestLevel(string name)
{
	StartNewGame(DIF_EASY);
	lev = CLevelMgr::LoadLevel(name); 
	CMoleEngine::ExtendMap();
}

//////////////////////////////////////////////////////////////////////////
/// Dodaje znak do stringa tajnego kodu.
/// @param cha znak do dopisania
//////////////////////////////////////////////////////////////////////////
void CMoleEngine::cheatInput(string cha)
{
	if( cheats.empty() || ( !cheats.empty() && *cheats.rbegin() != *cha.rbegin() ) ) {
		cheats = cheats + cha;
		checkCheat();
	}
}

//////////////////////////////////////////////////////////////////////////
/// Sprawdza, czy wpisano tajny kod.
/// @return true, jeśli zastosowano tajny kod
//////////////////////////////////////////////////////////////////////////
bool CMoleEngine::checkCheat()
{
	int temp_flag = 0;


	for(int j = 0 ; j < CH_MAX;j++)
	{
		if(cheats == cheat_codes[j])
		{
			cheat_flags[j] = true;
			cheats = "";
			return true;
		}

		else 
			if(cheat_codes[j].length() >= cheats.length())
			{
				if(cheats[cheats.length()-1] != cheat_codes[j][cheats.length()-1])
				{

					temp_flag += 1;
				}
			}

	}

	if(temp_flag == CH_MAX)
	{

		temp_flag = 0;
		for(int j = 0 ; j < CH_MAX;j++)
		{


			if(cheats[cheats.length()-1] == cheat_codes[j][0])
			{
				cheats = cheats[cheats.length()-1];
				temp_flag += 1;
			}


		}
		if(temp_flag == 0)
		{
			cheats = "";
		}
	}


	return false;
}

//////////////////////////////////////////////////////////////////////////
/// Metoda obsługująca zdarzenie utraty życia.
//////////////////////////////////////////////////////////////////////////
void CMoleEngine::MoleDeath()
{

	if(FlowFadeFlag == FL_DEATH || FlowFadeFlag == FL_NONE)
	{
		if(!cheat_flags[0])
		{

			for(int i = 0 ; i < lev.stoneList.size();i++)
			{
				if( lev.stoneList[i].x == mole.X 
					&& lev.stoneList[i].y == mole.Y)
				{
					if( !fadeToBlack ) {
						CAudioMgr::GetSfx( SFX_ON_LIFE_LOSS )->Play();
					}

					if(currentLev.life > 1)
					{
						fadeToBlack = true;
						FlowFadeFlag = FL_DEATH;

						if(intensity == MAX_INTENSITY/2)
						{
							FlowFlag[FL_DEATH] = true;
						}

						if(intensity == MAX_INTENSITY)
						{

							currentLev.life--;
							RestartLevel();
						}
					}
					else
					{

						fadeToBlack = true;

						if(intensity == MAX_INTENSITY/3)
						{
							FlowFlag[FL_GAME_OVER] = true;
						}

						if(intensity == MAX_INTENSITY)
						{
							fadeDir = false;
							fadeToBlack = false;

							intensity = 0;
							status.current_window = WIN_MAIN_MENU;
							mole.Y=-1;
							mole.X =-1;
							CAudioMgr::GetMusicTrack( MUSIC_TEST2 )->Stop();
							CAudioMgr::GetMusicTrack( MUSIC_TEST1 )->PlayLoop();
						}

					}
				}
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////
/// Metoda aktualizująca licznik przyciemnienia/rozjaśnienia ekranu rozgrywki.
//////////////////////////////////////////////////////////////////////////
void CMoleEngine::FadeToBlackCounter()
{
	if(!FlowFlag[FL_DEATH] && !FlowFlag[FL_TIME_OUT] && !FlowFlag[FL_GAME_OVER] && !FlowFlag[FL_NEXT_LEVEL] && !FlowFlag[FL_SUICIDE] && !FlowFlag[FL_GAME_END])
	{
		if(fadeToBlack)
		{
			if(!fadeDir)
			{
				intensity += 1;
				if(intensity >= MAX_INTENSITY)
				{
					fadeDir = true;
				}
			}
			else
			{
				intensity -= 1;
				if(intensity <= 0)
				{
					fadeDir = false;
					fadeToBlack = false;
					FlowFadeFlag = FL_NONE;
					intensity = 0;
				}
			}

		}

	}
}

//////////////////////////////////////////////////////////////////////////
/// Metoda wypisująca na ekranie tekst okolicznościowy.
/// @see flow_t
//////////////////////////////////////////////////////////////////////////
void CMoleEngine::FlowText()
{
	if(FlowFlag[FL_DEATH])
	{
		CGUI::DrawTextCentered( TEX_FONT1,255,0,0, 512, 250, 5.0f, 5.0f, CLanguageMgr::GetText( TXT_Mission_failed ));


		CGUI::DrawTextCentered( TEX_FONT1,255,0,0, 512, 300, 4.0f, 4.0f, CLanguageMgr::GetText( TXT_You_died ));

		CGUI::DrawTextCentered( TEX_FONT1,255,0,0, 512, 550, 3.0f, 3.0f, CLanguageMgr::GetText( TXT_Press_Enter ));
		CGUI::DrawTextCentered( TEX_FONT1,255,0,0, 512, 575, 3.0f, 3.0f, CLanguageMgr::GetText( TXT_to_try_again ));


	}
	if(FlowFlag[FL_TIME_OUT])
	{
		CGUI::DrawTextCentered( TEX_FONT1,255,0,0, 512, 250, 5.0f, 5.0f, CLanguageMgr::GetText( TXT_Mission_failed ));
		CGUI::DrawTextCentered( TEX_FONT1,255,0,0, 512, 300, 4.0f, 4.0f, CLanguageMgr::GetText( TXT_Time_out ));



		CGUI::DrawTextCentered( TEX_FONT1,255,0,0, 512, 550, 3.0f, 3.0f, CLanguageMgr::GetText( TXT_Press_Enter ));
		CGUI::DrawTextCentered( TEX_FONT1,255,0,0, 512, 575, 3.0f, 3.0f, CLanguageMgr::GetText( TXT_to_try_again ));
	}

	if(FlowFlag[FL_GAME_OVER])
	{
		CGUI::DrawTextCentered( TEX_FONT1,255,0,0, 512, 250, 5.0f, 5.0f, CLanguageMgr::GetText( TXT_Game_over ));
		CGUI::DrawTextCentered( TEX_FONT1,255,0,0, 512, 550, 3.0f, 3.0f, CLanguageMgr::GetText( TXT_Press_Enter ));
		CGUI::DrawTextCentered( TEX_FONT1,255,0,0, 512, 575, 3.0f, 3.0f, CLanguageMgr::GetText( TXT_to_back_to_main_menu ));

	}


	if(FlowFlag[FL_SUICIDE])
	{
		if(currentLev.life > 1)
		{
			CGUI::DrawTextCentered( TEX_FONT1,255,0,0, 512, 250, 5.0f, 5.0f, CLanguageMgr::GetText( TXT_Mission_failed ));
			CGUI::DrawTextCentered( TEX_FONT1,255,0,0, 512, 550, 3.0f, 3.0f, CLanguageMgr::GetText( TXT_Press_Enter ));
			CGUI::DrawTextCentered( TEX_FONT1,255,0,0, 512, 575, 3.0f, 3.0f, CLanguageMgr::GetText( TXT_to_try_again ));
		}
		else
		{
			CGUI::DrawTextCentered( TEX_FONT1,255,0,0, 512, 250, 4.0f, 5.0f, CLanguageMgr::GetText( TXT_Game_over ));
			CGUI::DrawTextCentered( TEX_FONT1,255,0,0, 512, 550, 3.0f, 3.0f, CLanguageMgr::GetText( TXT_Press_Enter ));
			CGUI::DrawTextCentered( TEX_FONT1,255,0,0, 512, 575, 3.0f, 3.0f, CLanguageMgr::GetText( TXT_to_back_to_main_menu ));

		}
		CGUI::DrawTextCentered( TEX_FONT1,255,0,0, 512, 300, 4.0f, 4.0f, CLanguageMgr::GetText( TXT_You_commited_suicide ));

	}

	if(FlowFadeFlag == FL_GAME_END)
	{
		if(intensity > (MAX_INTENSITY*2)/3-40)
		{	
			CGUI::DrawTextCentered( TEX_FONT1,255,255,255, 512, 250, 5.0f, 5.0f, CLanguageMgr::GetText( TXT_Congratulation ));
		}
		if(intensity > (MAX_INTENSITY*2)/3-30)
		{	
			CGUI::DrawTextCentered( TEX_FONT1,255,255,255, 512, 300, 4.0f, 4.0f, CLanguageMgr::GetText( TXT_You_beat_the_Game ));
		}
		if(intensity > (MAX_INTENSITY*2)/3-20)
		{	
			CGUI::DrawTextCentered( TEX_FONT1,255,255,255, 512, 350, 4.0f, 4.0f, CLanguageMgr::GetText( TXT_Score ));
			CGUI::DrawTextCentered( TEX_FONT1,255,255,255, 512, 400, 4.0f, 4.0f, CCommon::va( "%05i", currentLev.score ));
		}
		if(intensity > (MAX_INTENSITY*2)/3-10)
		{	
			CGUI::DrawTextCentered( TEX_FONT1,255,255,255, 512, 450, 4.0f, 4.0f, CLanguageMgr::GetText( TXT_Difficulty ));
			switch(status.difficulty)
			{
			case DIF_EASY:
				{
					CGUI::DrawTextCentered( TEX_FONT1,0,255,0, 512, 500, 4.0f, 4.0f, CLanguageMgr::GetText( TXT_Easy ));
				}
				break;
			case DIF_NORMAL:
				{
					CGUI::DrawTextCentered( TEX_FONT1,255,255,0, 512, 500, 4.0f, 4.0f, CLanguageMgr::GetText( TXT_Normal ));
				}
				break;
			case DIF_HARD:
				{
					CGUI::DrawTextCentered( TEX_FONT1,255,0,0, 512, 500, 4.0f, 4.0f, CLanguageMgr::GetText( TXT_Hard ));

				}
				break;
			}

		}
		if(intensity >= (MAX_INTENSITY*2)/3)
		{	
			CGUI::DrawTextCentered( TEX_FONT1,255,255,255, 512, 550, 3.0f, 3.0f, CLanguageMgr::GetText( TXT_Press_Enter ));
			CGUI::DrawTextCentered( TEX_FONT1,255,255,255, 512, 575, 3.0f, 3.0f, CLanguageMgr::GetText( TXT_to_back_to_main_menu ));
		}
	}

	if(FlowFlag[FL_NEXT_LEVEL])
	{

		char str1[40];
		strcpy(str1, CLanguageMgr::GetText( TXT_Diamonds_colected ));
		strcat(str1, CCommon::va( " %i", currentLev.diamond ));

		char str2[40];
		strcpy(str2, CLanguageMgr::GetText( TXT_Time_left ));
		strcat(str2, CCommon::va( " %02i:%02i", time_left[1], time_left[0] ));

		char str3[40];
		strcpy(str3, CLanguageMgr::GetText( TXT_Bonus_Score ));
		if(cheat_flags[CH_BAD])
		{
			strcat(str3, CCommon::va( " %02i", -5000));
		}
		else
		{
			strcat(str3, CCommon::va( " %02i", (time_left[1]*60*2 + time_left[0])*25 ));
		}

		if(currentLev.diamond_total == currentLev.diamond)
		{
			CGUI::DrawTextCentered( TEX_FONT1,255,255,255, 512, 200, 5.0f, 5.0f, CLanguageMgr::GetText( TXT_Level_Complete ));
			CGUI::DrawTextCentered( TEX_FONT1,255,255,255, 512, 400, 5.0f, 5.0f, CLanguageMgr::GetText( TXT_Perfect ));



			CGUI::DrawTextCentered( TEX_FONT1,255,255,255, 512, 490, 4.0f, 4.0f, str1);

			CGUI::DrawTextCentered( TEX_FONT1,255,255,255, 512, 540, 4.0f, 4.0f, str2);
			CGUI::DrawTextCentered( TEX_FONT1,255,255,255, 512, 590, 4.0f, 4.0f, str3);

		}
		else
		{
			CGUI::DrawTextCentered( TEX_FONT1,255,255,255, 512, 250, 5.0f, 5.0f, CLanguageMgr::GetText( TXT_Level_Complete ));
			CGUI::DrawTextCentered( TEX_FONT1,255,255,255, 512, 390, 4.0f, 4.0f, str1);



			CGUI::DrawTextCentered( TEX_FONT1,255,255,255, 512, 440, 4.0f, 4.0f, str2);
			CGUI::DrawTextCentered( TEX_FONT1,255,255,255, 512, 490, 4.0f, 4.0f, str3);
		}

		CGUI::DrawTextCentered( TEX_FONT1,255,255,255, 512, 650, 3.0f, 3.0f, CLanguageMgr::GetText( TXT_Press_Enter ));
		CGUI::DrawTextCentered( TEX_FONT1,255,255,255, 512, 675, 3.0f, 3.0f, CLanguageMgr::GetText( TXT_to_continue ));
	}
}


//////////////////////////////////////////////////////////////////////////
/// Obsługuje tajny kod "drunk" (odwrócenie sterowania)
//////////////////////////////////////////////////////////////////////////
void CMoleEngine::Cheats()
{
	if(cheat_flags[CH_DRUNK])
	{
		int temp;
		temp = keys[KEY_LEFT];
		keys[KEY_LEFT] = keys[KEY_RIGHT];
		keys[KEY_RIGHT] = temp;

		temp = keys[KEY_UP];
		keys[KEY_UP] = keys[KEY_DOWN];
		keys[KEY_DOWN] = temp;
		cheat_flags[CH_DRUNK] = false;
	}

	if(cheat_flags[CH_BAD])
	{
		next_lev = true;
		
	}
}


//////////////////////////////////////////////////////////////////////////
/// Obsługuje samobójstwo gracza.
//////////////////////////////////////////////////////////////////////////
void CMoleEngine::MoleSuicide()
{
	if(FlowFadeFlag == FL_SUICIDE)
	{
		if( !fadeToBlack ) {
			CAudioMgr::GetSfx( SFX_ON_LIFE_LOSS )->Play();
		}

		if(currentLev.life > 1)
		{
			fadeToBlack = true;

			if(intensity == MAX_INTENSITY/2)
			{
				if(!fadeDir)
				{
					FlowFlag[FL_SUICIDE] = true;
				}
			}

			if(intensity == MAX_INTENSITY)
			{

				currentLev.life--;
				RestartLevel();
			}
		}
		else
		{

			fadeToBlack = true;

			if(intensity == MAX_INTENSITY/2)
			{				
				if(!fadeDir)
				{
					FlowFlag[FL_SUICIDE] = true;}
			}

			if(intensity == MAX_INTENSITY)
			{
				fadeDir = false;
				fadeToBlack = false;

				intensity = 0;
				status.current_window = WIN_MAIN_MENU;
				mole.Y=-1;
				mole.X =-1;
				CAudioMgr::GetMusicTrack( MUSIC_TEST2 )->Stop();
				CAudioMgr::GetMusicTrack( MUSIC_TEST1 )->PlayLoop();
			}

		}
	}
}

//////////////////////////////////////////////////////////////////////////
/// Obsługuje zdarzenie końca gry.
//////////////////////////////////////////////////////////////////////////
void CMoleEngine::GameEnd()
{
	if(FlowFadeFlag == FL_GAME_END)
	{
		fadeToBlack = true;

		if(intensity == (MAX_INTENSITY*2)/3)
		{				
			if(!fadeDir)
			{
				FlowFlag[FL_GAME_END] = true;
			}

		}

	}
}

//////////////////////////////////////////////////////////////////////////
/// Dodaje punkty.
/// @param amount ilość punktów do dodania
//////////////////////////////////////////////////////////////////////////
void CMoleEngine::AddScore( int amount )
{
	currentLev.score += amount;

	if( currentLev.score >= currentLev.scoreRequiredFor1Up ) {
		CAudioMgr::GetSfx( SFX_PERFECT_LEVEL_COMPLETION )->Play();
		currentLev.life++;
		currentLev.scoreRequiredFor1Up += 2000;
		currentLev.score1UpMsgDisappearTime = GetSeconds() + 3;
	}	
}

