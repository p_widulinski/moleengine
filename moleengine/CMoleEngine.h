#pragma once

#include "AllegroGlobals.h" 
#include "CTexture.h"
#include "CTextureMgr.h"
#include "CRenderer.h"
#include "CInput.h"
#include "CLevel.h"
#include "CMole.h"
#include "CGUI.h"

//////////////////////////////////////////////////////////////////////////
/// Struktura zawierająca zmienne niezbędne do działania aplikacji oraz zmienne ustawień aplikacji
//////////////////////////////////////////////////////////////////////////
typedef struct status_s 
{
	///aktualnie renderowane okno
	int current_window; 

	///aktualnie wybrana opcja na ekranie
	int current_menu_option; 

	///ilość diamentów zebrana przez gracza w ciągu wszystkich gier
	int diamonds; 

	///głosność ogolna w zakresie 0-100, gdzie 100 = 100%
	int sound; 

	///głośnośc muzyki w zakresie 0.0f - 1.0f, gdzie 1.0f = 100%
	float music; 

	///głośność efektów w zakresie 0.0f - 1.0f, gdzie 1.0f = 100%
	float sfx;

	///poziom trudności gry
	int difficulty; 

	///jasność ogólna wszystkich grafik, zakres 0-10, gdzie 5 to bazowa, bez modyfkacji
	int gamma;

	/// przypisanie klawisza Up
	int keyUp;

	/// przypisanie klawisza Down
	int keyDown;

	/// przypisanie klawisza Left
	int keyLeft;

	/// przypisanie klawisza Right
	int keyRight;

	/// przypisanie klawisza Suicide
	int keySuicide;

	/// czy binduje w tej chwili klawisz?
	bool bindingKey;
	
	/// czy binduje w tej chwili klawisz Up?
	bool bindingKeyUp;

	/// czy binduje w tej chwili klawisz Down?
	bool bindingKeyDown;

	/// czy binduje w tej chwili klawisz Left?
	bool bindingKeyLeft;

	/// czy binduje w tej chwili klawisz Right?
	bool bindingKeyRight;

	/// czy binduje w tej chwili klawisz Suicide?
	bool bindingKeySuicide;
} status_t;



//////////////////////////////////////////////////////////////////////////
/// Główna klasa programu. Inicjuje resztę zmiennych i obiektów. Zawiera pętle główną programu.
//////////////////////////////////////////////////////////////////////////
class CMoleEngine
{
public:
	CMoleEngine(void);
	~CMoleEngine(void);
	void MoleMain(); 
	void Init();

	/// 
	/// Obiekt menedżera tekstur.
	CTextureMgr TexMgr;

	/// 
	/// Wskaźnik na ekran Allegro.
	ALLEGRO_DISPLAY *display;

	/// 
	/// Główny timer aplikacji. (60 FPS)
	ALLEGRO_TIMER *timer;

	/// 
	/// Dodatkowy timer aplikacji. Inkrementowany 60 razy na sekundę.
	static int time;
	/// 
	/// Obecny stan aplikacji.
	static status_t status; 

	

	static void GameTextures();
	static void GameKeys();
	static void TestLevel(string name); //TODO: Metoda na potrzeby developmentu, usunać w finalnej wersji - PG
	static int GetSeconds();
	static int GetRemainingTime();
	static int GetLevelTimelimit();
	static void AddScore( int amount );

private:
	/// 
	/// Obecnie rozgrywany poziom
	static CLevel lev;

	/// 
	/// Obiekt gracza
	static CMole mole;

	static void NextLevel();
	static void RestartLevel();
	static void StoneMovement();
	static void ExtendMap();
	static void ElixirTimer();
	static void MainMenuKeys();
	static void StartNewGame(int difficulty);
	static void StartLevelEditor();
	static void MoleTimer();
	static void CheckHold(int stone);
	static void cheatInput(string cha);
	static bool checkCheat();
	static void MoleDeath();
	static void FadeToBlackCounter();
	static void CreditsKeys();
	static void FlowText();
	static void Cheats();
	static void ShowCredits();
	static void CheckLevelTimeout();
	static void MoleSuicide();
	static void GameEnd();
	static void ShowOptions();
	static void Options1Keys();
	static void Options2Keys();
	static void BindKey( int keynum );
	static void Cmd_MoveUp();
	static void Cmd_MoveDown();
	static void Cmd_MoveLeft();
	static void Cmd_MoveRight();
	static void Cmd_CommitSuicide();
	static int ActionForKeyBinding( int keynum );
};

