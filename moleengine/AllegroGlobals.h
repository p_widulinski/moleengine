#pragma once

#include <iostream>
#include <conio.h>
#include <allegro5/allegro.h>
#include "allegro5/allegro_image.h"
#include "allegro5/allegro_native_dialog.h"
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include <stdarg.h>


using namespace std;

extern const float FPS;
extern const int SCREEN_W;
extern const int SCREEN_H;
extern bool doexit;
extern bool redraw ;

extern bool key[16];

extern int x,y;

extern ALLEGRO_EVENT_QUEUE *event_queue;
extern ALLEGRO_EVENT ev;

//extern struct options_s options;
extern struct currentLevel_s currentLev;

extern bool next_lev;
extern bool fadeToBlack;


//Ustawienia fizyki gry
#define STONE_FALLING_TIME 8
#define MOVE_MOVE_TIME 8
#define SCREEN_CENTER_WIDTH 32*14
#define SCREEN_CENTER_HEIGHT 32*8
#define MAX_INTENSITY 75
#define LAST_LEVEL 11 //ostatni numer poziomu, po którym nastepuje koniec gry
#define LEVEL_TIMELIMIT 21 // limit czasu w sekundach
#define DARKNESS true //ciemność poza mapą
//#define MUTE_SOUND   // jesli niezakomentowane, dzwiek nie bedzie odtwarzany

//////////////////////////////////////////////////////////////////////////
/// Struktura zawierająca informacje o obecnie rozgrywanym poziomie.
//////////////////////////////////////////////////////////////////////////
typedef struct currentLevel_s 
{
	///aktualnie rozgrywany poziom
	int id;
	
	///typ poziomu, 0 dla poziomow gry normalnej, 1 dla poziomów stworzonych przez gracza
	bool type;
	
	///ilość zebranych dżdzownic
	int worm;
	
	///ilość dżdzownic na mapie
	int worm_total;
	
	///ilość zebranych diamnetów
	int diamond;
	
	///ilość diamnetów  na mapie
	int diamond_total;
	
	///ilość żyć
	int life;
	
	///obecnie zebrany elixir
	int elixir;
	
	///timer elixiru
	int ex_time;
	
	///timer poziomu
	int lv_time;
	
	///flaga otwarcia przejścia do kolejnego poziomu
	bool doorOpen;
	
	/// punkty
	int score;

	/// punkty potrzebne do zdobycia 1 Up'a
	int scoreRequiredFor1Up;

	/// sekundy od startu aplikacji kiedy zniknie powiadomienie o 1 Up'ie
	int score1UpMsgDisappearTime;
} currentLevel_t;


//////////////////////////////////////////////////////////////////////////
/// Enumeracja rodzajów terenu gry
//////////////////////////////////////////////////////////////////////////
enum terrain_t { //enumeracja terenu gry
	TER_NONE, TER_SOIL, TER_WALL, TER_STONE, TER_WORM, TER_DIAMOND, TER_ELIXIR1, TER_ENTRY, TER_EXIT ,TER_ELIXIR2 ,TER_WALL_BORDER
};


//////////////////////////////////////////////////////////////////////////
/// Enumeracja klawiszy
//////////////////////////////////////////////////////////////////////////
enum  key_t { //enumeracja klawiszy
	KEY_UP, KEY_DOWN, KEY_LEFT, KEY_RIGHT, KEY_1, KEY_2, KEY_3, KEY_4, KEY_5, KEY_6, KEY_7, KEY_8, KEY_ENTER, KEY_DELETE, KEY_PGUP, KEY_PGDN
};

//////////////////////////////////////////////////////////////////////////
/// Enumeracja tekstur
//////////////////////////////////////////////////////////////////////////
enum texture_t { //enumeracja tekstur
	TEX_CURSOR, TEX_SOIL, TEX_WALL, TEX_ROCK, TEX_WORM, TEX_DIAMOND, TEX_ELIXIR1, TEX_ENTRY, TEX_EXIT,TEX_ELIXIR2 ,TEX_WALL_BORDER, TEX_BORDER, TEX_EARTH_BG,TEX_MOLE,
	//Textury LevelEditora

	TEX_DIAMOND_HUD,TEX_ELIXIR_HUD, TEX_EXITGAME_HUD, TEX_HEART_HUD, TEX_TIMER_HUD,
	TEX_WORM_HUD, TEX_HUD, TEX_FRAME_HUD , TEX_AVATAR_HUD,  TEX_TROPHY,//Textury HUD

	TEX_MMBG, TEX_MMPLAY, TEX_MMOPTIONS, TEX_MMSHOP, TEX_MMCREDITS, TEX_MMRANKING, TEX_MMEXIT,
	TEX_MMCM, TEX_MMMOLE, //Textury MainMenu

	TEX_OPTIONSBG,
	TEX_OPTIONSLINE,

	TEX_OPTIONSBARBG, TEX_OPTIONSBARFG, //Textury Options


	TEX_OPTIONSKEYBOX, //Textury Options 2

	 TEX_CREDITSBG, //Textury Credits

	TEX_MAIN_MENU_HIGHLIGHT, TEX_FADE_TO_BLACK,

	TEX_FONT1,

	TEX_BLACK,TEX_BLACK_UP,TEX_BLACK_LEFT,TEX_BLACK_DOWN,TEX_BLACK_RIGHT, //textury przyciemniajace mape

	TEX_MAX
};

//////////////////////////////////////////////////////////////////////////
/// Enumeracja okien
//////////////////////////////////////////////////////////////////////////
enum window_t { //enumeracja wszystkich okien
	WIN_EDITOR, WIN_GAME_MAIN, WIN_SIZE_MENU, WIN_LOAD_MENU, WIN_MAIN_MENU, WIN_OPTIONS1, WIN_OPTIONS2, WIN_CREDITS
};

//////////////////////////////////////////////////////////////////////////
/// Enumeracja opcji w menu
//////////////////////////////////////////////////////////////////////////
enum menu_t { //enumeracja opcji w menu
	MEN_TEST1
};

//////////////////////////////////////////////////////////////////////////
/// Enumeracja rodzajów ruchu gracza
//////////////////////////////////////////////////////////////////////////
enum moleMove_t { 
	MOV_NONE, MOV_LEFT,MOV_DOWN,MOV_RIGHT,MOV_UP,MOV_LEFT_DOWN,MOV_LEFT_UP,MOV_RIGHT_DOWN,MOV_RIGHT_UP
};

//////////////////////////////////////////////////////////////////////////
/// Enumeracja typów eliksirów
//////////////////////////////////////////////////////////////////////////
enum elixirType_t { 
	EX_NONE, EX_TIME_STOP, EX_WALL_BREAKER
};

//////////////////////////////////////////////////////////////////////////
/// Enumeracja tajnych kodów
//////////////////////////////////////////////////////////////////////////
enum cheats_t { 
	CH_GOD, CH_DRUNK,CH_BAD, CH_MAX
};

//////////////////////////////////////////////////////////////////////////
/// Enumeracja zdarzeń
//////////////////////////////////////////////////////////////////////////
enum  flow_t { //enumeracja zdarzeń
	FL_NONE, FL_DEATH, FL_TIME_OUT, FL_GAME_OVER, FL_NEXT_LEVEL, FL_SUICIDE, FL_GAME_END
};

//////////////////////////////////////////////////////////////////////////
/// Enumeracja poziomów trudności
//////////////////////////////////////////////////////////////////////////
enum  difficulty_t {
	DIF_EASY, DIF_NORMAL,DIF_HARD
};

//////////////////////////////////////////////////////////////////////////
/// Enumeracja SFX
//////////////////////////////////////////////////////////////////////////
enum sfxs{
	SFX_MENU_ON_CLICK, SFX_STEP_ON_GROUND, SFX_ON_LIFE_LOSS, SFX_ON_STONE_TUMBLE, SFX_ON_NEXT_LEVEL_UNLOCK, SFX_WORM_EAT, SFX_DIAMOND_COLLECTED, SFX_LEVEL_COMPLETE, SFX_PERFECT_LEVEL_COMPLETION, SFX_MAX
};

//////////////////////////////////////////////////////////////////////////
/// Enumeracja muzyki
//////////////////////////////////////////////////////////////////////////
enum musics{
	MUSIC_TEST1, MUSIC_TEST2, MUSIC_MAX
};

