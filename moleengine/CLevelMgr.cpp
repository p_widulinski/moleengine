#include "CLevelMgr.h"

currentLevel_s currentLev;

CLevelMgr::CLevelMgr(void)
{
}


CLevelMgr::~CLevelMgr(void)
{
}

//////////////////////////////////////////////////////////////////////////
/// Zapisuje poziom na dysk twardy.
/// @param version wersja poziomu
/// @param field poziom
/// @param FileName nazwa pliku
//////////////////////////////////////////////////////////////////////////
void CLevelMgr::SaveLevel(unsigned int version,CLevel field,string FileName)
{
	FILE *ptr_myfile;
	struct levelHeader_s my_record;

	my_record.magic = 1337;
	my_record.version = version;
	my_record.columns = field.columns;
	my_record.rows = field.rows;

	ptr_myfile=fopen(CCommon::va("levels/%s.bin", FileName.c_str()),"wb");
	if (!ptr_myfile)
	{
		printf("Unable to open file!");
		return ;
	}

	fwrite(&my_record, sizeof(struct levelHeader_s), 1, ptr_myfile);

	for(int i=0; i < my_record.columns; i++)
	{
		for(int j=0;j < my_record.rows;j++)
		{

			fwrite(&field.field[i*my_record.rows+j], sizeof(unsigned int), 1, ptr_myfile);
		}
	}

	fclose(ptr_myfile);
}


//////////////////////////////////////////////////////////////////////////
/// Wczytuje poziom z dysku twardego.
/// @param FileName nazwa pliku
/// @return wczytany poziom
//////////////////////////////////////////////////////////////////////////
CLevel CLevelMgr::LoadLevel(string FileName)
{

	FILE *ptr_myfile;
	struct levelHeader_s my_record;


	ptr_myfile = fopen(CCommon::va("levels/%s.bin", FileName.c_str()),"rb");
	if (!ptr_myfile)
	{
		printf("Unable to open file!");
		CLevel temp(0,0);
		return temp;
	}
	else
	{
		fread(&my_record, sizeof(struct levelHeader_s), 1, ptr_myfile);
		//TODO: usunac printf
		printf("%d\n",my_record.magic);
		printf("%d\n",my_record.version);
		printf("%d\n",my_record.columns);
		printf("%d\n",my_record.rows);
		CLevel temp(my_record.rows,my_record.columns);
		temp.rows = my_record.rows;
		temp.columns = my_record.columns;


		for(int i=0; i < my_record.columns; i++)
		{
			for(int j=0;j < my_record.rows;j++)
			{

				fread(&temp.field[i*my_record.rows+j], sizeof(unsigned int), 1, ptr_myfile);
				printf("%i ",temp.field[i*my_record.rows+j]);

				
			}
			printf("\n");
		}
		currentLev.worm_total = temp.worms;
		currentLev.worm = 0;
		currentLev.id = atoi(FileName.c_str());
		fclose(ptr_myfile);
		return temp;
	}

}
