#include "CInput.h"

map<int, bool> CInput::keys;

CInput::CInput(void)
{
}

CInput::~CInput(void)
{
}

//////////////////////////////////////////////////////////////////////////
/// Aktualizuje listę aktualnie wciśniętych klawiszy.
/// @param ev zdarzenie Allegro
//////////////////////////////////////////////////////////////////////////
void CInput::Update(ALLEGRO_EVENT ev)
{
	if (ev.type == ALLEGRO_EVENT_KEY_DOWN)
	{
		keys[ev.keyboard.keycode] = true;
	}
	else if(ev.type == ALLEGRO_EVENT_KEY_UP)
	{
		keys[ev.keyboard.keycode] = false;
	}
}

//////////////////////////////////////////////////////////////////////////
/// Sprawdza czy klawisz o danym kodzie jest aktualnie wciśnięty.
/// @param key id klawisza
//////////////////////////////////////////////////////////////////////////
bool CInput::GetKeyState(int key)
{
	if (keys.find(key) == keys.end())
	{
		return false;
	}
	else
	{
		return keys.at(key);
	}
}

//////////////////////////////////////////////////////////////////////////
/// Wyłącza klawisz.
/// @param key id klawisza
//////////////////////////////////////////////////////////////////////////
void CInput::Delay(int key)
{
	keys.at(key) = false;
}

