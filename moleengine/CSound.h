#pragma once
#include "AllegroGlobals.h" 
#include "CCommon.h"

//////////////////////////////////////////////////////////////////////////
/// Klasa opisująca pojedynczy dźwięk.
//////////////////////////////////////////////////////////////////////////
class CSound
{
public:
	CSound();
	~CSound();

	/// 
	/// Próbka dźwięku w systemie Allegro.
	ALLEGRO_SAMPLE *sample;
	ALLEGRO_SAMPLE_INSTANCE *sampleInstance;

	/// 
	/// Ścieżka do pliku dźwiękowego na dysku.
	char* path;
	
	char* SetSoundPath(char*);
	void Play();
	void Release();
	void Load(char*);
	void PlayLoop();
	void Stop();
	void SetSpeed( float speed );
	void PlayNoInterrupt();
	void SetVolume( float vol );
};

