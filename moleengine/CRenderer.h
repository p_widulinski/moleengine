#pragma once
#include <queue>
#include "CTexture.h"
#include "CTextureMgr.h"

//////////////////////////////////////////////////////////////////////////
/// Struktura pojedynczej ramki (instrukcji) renderingu.
//////////////////////////////////////////////////////////////////////////
typedef struct Render_s 
{
	/// ID tekstury
	int tex;

	/// Pozycja X
	int x;

	/// Pozycja Y
	int y;
	
	/// czy recznie okreslamy jaki obszar bitmapy rysujemy - pw
	bool region;
	
	/// x obszaru do rysowania
	float sx; 
	
	/// y obszaru do rysowania
	float sy; 
	
	/// szerokosc obszaru do rysowania
	float sw; 
	
	/// wysokosc obszaru do rysowania
	float sh; 

	/// czy skalujemy rysowana bitmape
	bool scaled; 
	
	/// skala x
	float scale_x; 
	
	/// skala y
	float scale_y; 

	/// czy kolorujemy rysowana bitmape
	bool colored;  
	
	/// skladowa R
	unsigned char r;
	
	/// skladowa G
	unsigned char g;
	
	/// skladowa B
	unsigned char b;
} Render_t;

/*
class CRenderer
Klasa wyświetlająca wszystkie elementy na ekranie
*/

//////////////////////////////////////////////////////////////////////////
/// Klasa rysująca na ekranie. 
//////////////////////////////////////////////////////////////////////////
class CRenderer
{
	
public:

	CRenderer(void);
	~CRenderer(void);
	
	static void AddToDrawQueue(int tex,int x,int y);
	static void AddToDrawQueue( int tex, int x, int y, bool region, float sx, float sy, float sw, float sh, bool scaled, float scale_x, float scale_y, bool colored, unsigned char r, unsigned char g, unsigned char b );
	static void DrawSingle();
	static void DrawQueue();
	static void AddQueue(CRenderer queue);
	static void ClearToBlack();
private:

	/// 
	/// Kolejka rysowania
	static queue <Render_t> drawQueue;

};

