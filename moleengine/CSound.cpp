#include "CSound.h"


CSound::CSound()
{
	sample = NULL;
}


CSound::~CSound()
{
	if( sample ) {
		al_destroy_sample( sample );
	}
}


//////////////////////////////////////////////////////////////////////////
/// Ustawia ścieżkę do pliku dźwiękowego (zmienna path) znajdującego się na dysku
/// @param _path ścieżka do pliku dźwiękowego
/// @return ścieżka do pliku dźwiękowego
//////////////////////////////////////////////////////////////////////////
char* CSound::SetSoundPath(char* _path)
{
	path = _path;
	return path;
}


//////////////////////////////////////////////////////////////////////////
/// Wczytuje dźwięk WAV z dysku na podstawie podanej ścieżki do pliku
/// @param soundName ściezka do pliku
//////////////////////////////////////////////////////////////////////////
void CSound::Load(char *soundName)
{
	sample = al_load_sample( CCommon::va("sounds/%s.wav", soundName));
	sampleInstance = al_create_sample_instance( sample );
	al_set_sample_instance_playmode( sampleInstance, ALLEGRO_PLAYMODE_ONCE );
    al_attach_sample_instance_to_mixer( sampleInstance, al_get_default_mixer() );
}

//////////////////////////////////////////////////////////////////////////
/// Odtwarza dźwięk.
//////////////////////////////////////////////////////////////////////////
void CSound::Play()
{
	if( sample ) {
		if( al_get_sample_instance_playing( sampleInstance ) ) {
			al_stop_sample_instance( sampleInstance );
		}
		al_set_sample_instance_playmode( sampleInstance, ALLEGRO_PLAYMODE_ONCE );

#ifndef MUTE_SOUND
		al_play_sample_instance( sampleInstance );
#endif
	}
}

//////////////////////////////////////////////////////////////////////////
/// Odtwarza dźwięk bez przerywania istniejącego.
//////////////////////////////////////////////////////////////////////////
void CSound::PlayNoInterrupt()
{
	if( sample ) {
#ifndef MUTE_SOUND
		al_play_sample_instance( sampleInstance );
#endif
	}
}

//////////////////////////////////////////////////////////////////////////
/// Odtwarza dźwięk w pętli.
//////////////////////////////////////////////////////////////////////////
void CSound::PlayLoop()
{
	if( sample ) {
		if( al_get_sample_instance_playing( sampleInstance ) ) {
			al_stop_sample_instance( sampleInstance );
		}
		al_set_sample_instance_playmode( sampleInstance, ALLEGRO_PLAYMODE_LOOP );
#ifndef MUTE_SOUND
		al_play_sample_instance( sampleInstance );
#endif
	}
}

//////////////////////////////////////////////////////////////////////////
/// Odtwarza dźwięk w pętli.
//////////////////////////////////////////////////////////////////////////
void CSound::SetSpeed( float speed )
{
	if( sample ) {
		//if( al_get_sample_instance_playing( sampleInstance ) ) {
		//	al_stop_sample_instance( sampleInstance );
		//}
		//al_set_sample_instance_playmode( sampleInstance, ALLEGRO_PLAYMODE_LOOP );
		//al_play_sample_instance( sampleInstance );
		al_set_sample_instance_speed( sampleInstance, speed );
	}
}

//////////////////////////////////////////////////////////////////////////
/// Zatrzymuje odtwarzanie dźwięku.
//////////////////////////////////////////////////////////////////////////
void CSound::Stop()
{
	if( sample ) {
		if( al_get_sample_instance_playing( sampleInstance ) ) {
			al_stop_sample_instance( sampleInstance );
		}
	}
}

//////////////////////////////////////////////////////////////////////////
/// Usuwa dźwięk z pamięci.
//////////////////////////////////////////////////////////////////////////
void CSound::Release()
{
	al_destroy_sample( sample );
	sample = NULL;
}

void CSound::SetVolume( float vol )
{
	if( sample ) {
		al_set_sample_instance_gain( sampleInstance, vol );
	}
}
