#include "CTextureMgr.h"

CTexture* CTextureMgr::texArray[TEX_MAX];


CTextureMgr::CTextureMgr(void)
{
	for(int i=0; i < TEX_MAX;i++)
	{
		texArray[i] = new CTexture();
	}
}

CTextureMgr::~CTextureMgr(void)
{

}


//////////////////////////////////////////////////////////////////////////
/// Wczytuje wszystkie bitmapy z dysku do aplikacji
//////////////////////////////////////////////////////////////////////////
void CTextureMgr::Init()
{

	texArray[TEX_CURSOR]->LoadTexture("cursor"); 
	texArray[TEX_SOIL]->LoadTexture("TEarth");
	texArray[TEX_WALL]->LoadTexture("TWall");
	texArray[TEX_ROCK]->LoadTexture("TRock"); 
	texArray[TEX_WORM]->LoadTexture("TWorm");
	texArray[TEX_DIAMOND]->LoadTexture("TDiamond");
	texArray[TEX_ELIXIR1]->LoadTexture("TTimeelixir");
	texArray[TEX_ENTRY]->LoadTexture("TIN");
	texArray[TEX_EXIT]->LoadTexture("TOUT");
	texArray[TEX_ELIXIR2]->LoadTexture("TCrushelixir");
	texArray[TEX_WALL_BORDER]->LoadTexture("ThWall");
	texArray[TEX_BORDER]->LoadTexture("border");
	texArray[TEX_MOLE]->LoadTexture("TMole");
	texArray[TEX_DIAMOND_HUD]->LoadTexture("Diamond");
	texArray[TEX_ELIXIR_HUD]->LoadTexture("Elixir");
	texArray[TEX_EXITGAME_HUD]->LoadTexture("ExitGame");
	texArray[TEX_HEART_HUD]->LoadTexture("Heart");
	texArray[TEX_TIMER_HUD]->LoadTexture("Timer");
	texArray[TEX_WORM_HUD]->LoadTexture("Worm");
	texArray[TEX_HUD]->LoadTexture("HUD");
	texArray[TEX_FRAME_HUD]->LoadTexture("FrameHUD");
	texArray[TEX_AVATAR_HUD]->LoadTexture("Avatar");
	texArray[TEX_EARTH_BG]->LoadTexture("TEarthBG");
	texArray[TEX_MMBG]->LoadTexture("MMBG");
	texArray[TEX_MMPLAY]->LoadTexture("Play");
	texArray[TEX_MMOPTIONS]->LoadTexture("Options");
	texArray[TEX_MMSHOP]->LoadTexture("Shop");
	texArray[TEX_MMCREDITS]->LoadTexture("Credits");
	texArray[TEX_MMRANKING]->LoadTexture("Ranking");
	texArray[TEX_MMEXIT]->LoadTexture("Exit");
	texArray[TEX_MMCM]->LoadTexture("MMCM");
	texArray[TEX_MMMOLE]->LoadTexture("MMMOLE");
	texArray[TEX_TROPHY]->LoadTexture("trophy");

    texArray[TEX_OPTIONSBG]->LoadTexture("OptionsBG");
	//texArray[TEX_OPTIONSBAR]->LoadTexture("OptionsBar");
    //texArray[TEX_OPTIONSDIFFICULTY]->LoadTexture("OptionsDifficulty");
    //texArray[TEX_OPTIONSEASY]->LoadTexture("OptionsEasy");
    //texArray[TEX_OPTIONSENGLISH]->LoadTexture("OptionsEnglish");
    //texArray[TEX_OPTIONSGENERAL]->LoadTexture("OptionsGeneral");
    //texArray[TEX_OPTIONSHARD]->LoadTexture("OptionsHard");
    //texArray[TEX_OPTIONSKEYS]->LoadTexture("OptionsKeys");
    //texArray[TEX_OPTIONSLANGUAGE]->LoadTexture("OptionsLanguage");
    texArray[TEX_OPTIONSLINE]->LoadTexture("OptionsLine");
    //texArray[TEX_OPTIONSMUSIC]->LoadTexture("OptionsMusic");
    //texArray[TEX_OPTIONSNORMAL]->LoadTexture("OptionsNormal");
    //texArray[TEX_OPTIONSPOLISH]->LoadTexture("OptionsPolish");
    //texArray[TEX_OPTIONSSFX]->LoadTexture("OptionsSFX");
    //texArray[TEX_OPTIONSTEXT]->LoadTexture("OptionsText");
	//texArray[TEX_OPTIONSGENERAL2]->LoadTexture("OptionsGeneral2");
	//texArray[TEX_OPTIONSKEYS2]->LoadTexture("OptionsKeys2");
	//texArray[TEX_OPTIONSUP]->LoadTexture("OptionsUP");
	//texArray[TEX_OPTIONSDOWN]->LoadTexture("OptionsDOWN");
	//texArray[TEX_OPTIONSLEFT]->LoadTexture("OptionsLEFT");
	//texArray[TEX_OPTIONSRIGHT]->LoadTexture("OptionsRIGHT");
	texArray[TEX_OPTIONSKEYBOX]->LoadTexture("OptionsKeyBox");
	texArray[TEX_OPTIONSBARBG]->LoadTexture("OptionsBarBG");
	texArray[TEX_OPTIONSBARFG]->LoadTexture("OptionsBarFG");

	texArray[TEX_CREDITSBG]->LoadTexture("CreditsBG");
	//texArray[TEX_CREDITSTEXT]->LoadTexture("CreditsText");
	
	texArray[TEX_MAIN_MENU_HIGHLIGHT]->LoadTexture("Selected");
	texArray[TEX_FADE_TO_BLACK]->LoadTexture("Black");

	texArray[TEX_BLACK]->LoadTexture("blackSq");
	texArray[TEX_BLACK_UP]->LoadTexture("FadeUp");
	texArray[TEX_BLACK_LEFT]->LoadTexture("FadeLeft");
	texArray[TEX_BLACK_DOWN]->LoadTexture("FadeDown");
	texArray[TEX_BLACK_RIGHT]->LoadTexture("FadeRight");

	texArray[TEX_FONT1]->LoadTexture("Font1");
}

//////////////////////////////////////////////////////////////////////////
/// Zwraca pojedynczą teksturę
/// @param num numer tekstury
/// @return wskaźnik na teksturę
//////////////////////////////////////////////////////////////////////////
CTexture* CTextureMgr::GetTexture(int num)
{
	return texArray[num];
}
