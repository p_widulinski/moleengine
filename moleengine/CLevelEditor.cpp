#include "CLevelEditor.h"
#include "CMoleEngine.h"

//Test class
int CLevelEditor::terrain = 1;

string CLevelEditor::name;
string CLevelEditor::height;
string CLevelEditor::width;
bool CLevelEditor::param;
bool sub_menu = false; //false jeśli save menu, true dla load menu

CLevelEditor::CLevelEditor(void)
{

}

CLevelEditor::~CLevelEditor(void)
{

}


//TODO:Do przeniesienia po utworzeniu stosownej klasy - PG

//////////////////////////////////////////////////////////////////////////
/// Rysuje tekstury edytora map.
/// @param lev poziom do wyrysowania w obszarze głównym
//////////////////////////////////////////////////////////////////////////

void CLevelEditor::EditorTextures(CLevel lev)
{
	int	offset_x = -SCREEN_CENTER_WIDTH + 32*x;
	int offset_y = -SCREEN_CENTER_HEIGHT + 32*y;
	int MapBorder[4] = {0,0,lev.rows,lev.columns};



	int temp=0;
	for(int i = 0;i < lev.columns; i++)
	{
		for(int j = 0;j < lev.rows; j++)
		{

			temp = lev.rows*i+j;
			if(lev.field[temp] > 0)
			{
				CRenderer::AddToDrawQueue(lev.field[temp],j*32 - offset_x,i*32 - offset_y);
			}
		}
	}

		for(int i = -1;i < lev.columns+1; i++)
	{
		for(int j = -1;j < lev.rows+1; j++)
		{
			if(i == -1)
			{
			CRenderer::AddToDrawQueue(TEX_BORDER,j*32 - offset_x,i*32 - offset_y);
			}
			if(i == lev.columns)
			{
			CRenderer::AddToDrawQueue(TEX_BORDER,j*32 - offset_x,i*32 - offset_y);
			}
			if(j == -1)
			{
			CRenderer::AddToDrawQueue(TEX_BORDER,j*32 - offset_x,i*32 - offset_y);
			}
			if(j == lev.rows)
			{
			CRenderer::AddToDrawQueue(TEX_BORDER,j*32 - offset_x,i*32 - offset_y);
			}
		}
	}

	for(int i = 1; i < 11 /*TEX_MAX*/;i++) // TEX_MAX -> 11 - pw
	{
		CRenderer::AddToDrawQueue(i,810,35*i);
	}

	CRenderer::AddToDrawQueue(0,810,35*terrain);
	CRenderer::AddToDrawQueue(0,SCREEN_CENTER_WIDTH,SCREEN_CENTER_HEIGHT);


}

//TODO:Do przeniesienia po utworzeniu stosownej klasy - PG

//////////////////////////////////////////////////////////////////////////
/// Rysuje tekst edytora map.
/// @param font czcionka
//////////////////////////////////////////////////////////////////////////
void CLevelEditor::EditorFont()
{
	//TODO: Przenieść do CGui po jej utworzeniu

	CGUI::DrawText( TEX_FONT1,255,255,255, 850, 35, 2.5f, 2.5f, CLanguageMgr::GetText( TXT_Soil ));
	CGUI::DrawText( TEX_FONT1,255,255,255, 850, 35*2, 2.5f, 2.5f, CLanguageMgr::GetText( TXT_Wall ));
	CGUI::DrawText( TEX_FONT1,255,255,255, 850, 35*3, 2.5f, 2.5f, CLanguageMgr::GetText( TXT_Rock ));
	CGUI::DrawText( TEX_FONT1,255,255,255, 850, 35*4, 2.5f, 2.5f, CLanguageMgr::GetText( TXT_Worm ));
	CGUI::DrawText( TEX_FONT1,255,255,255, 850, 35*5, 2.5f, 2.5f, CLanguageMgr::GetText( TXT_Diamond ));
	CGUI::DrawText( TEX_FONT1,255,255,255, 850, 35*6, 2.5f, 2.5f, CLanguageMgr::GetText( TXT_TimeStop ));
	CGUI::DrawText( TEX_FONT1,255,255,255, 850, 35*7, 2.5f, 2.5f, CLanguageMgr::GetText( TXT_Entry ));
	CGUI::DrawText( TEX_FONT1,255,255,255, 850, 35*8, 2.5f, 2.5f, CLanguageMgr::GetText( TXT_Exit ));
	CGUI::DrawText( TEX_FONT1,255,255,255, 850, 35*9, 2.0f, 2.0f, CLanguageMgr::GetText( TXT_StoneBreak ));
	CGUI::DrawText( TEX_FONT1,255,255,255, 850, 35*10, 2.5f, 2.5f, CLanguageMgr::GetText( TXT_HardWall ));

	CGUI::DrawText( TEX_FONT1,255,255,255, 850, 35*12, 1.5f, 1.5f, "[PGUP/PGDN]");
	CGUI::DrawText( TEX_FONT1,255,255,255, 850, 35*13-15, 1.5f, 1.5f, CLanguageMgr::GetText( TXT_select_tile ));
	CGUI::DrawText( TEX_FONT1,255,255,255, 850, 35*14, 1.5f, 1.5f, CLanguageMgr::GetText( TXT_ENTER_place ) );
	CGUI::DrawText( TEX_FONT1,255,255,255, 850, 35*15-15, 1.5f, 1.5f, CLanguageMgr::GetText( TXT_tile ));
	CGUI::DrawText( TEX_FONT1,255,255,255, 850, 35*16, 1.5f, 1.5f, CLanguageMgr::GetText( TXT_W_change ));
	CGUI::DrawText( TEX_FONT1,255,255,255, 850, 35*17-15, 1.5f, 1.5f, CLanguageMgr::GetText( TXT_map_size ));
	CGUI::DrawText( TEX_FONT1,255,255,255, 850, 35*18, 1.5f, 1.5f, CLanguageMgr::GetText( TXT_S_save_map ));
	CGUI::DrawText( TEX_FONT1,255,255,255, 850, 35*19-15, 1.5f, 1.5f, CLanguageMgr::GetText( TXT_L_load_map ));
	CGUI::DrawText( TEX_FONT1,255,255,255, 850, 35*20, 1.5f, 1.5f, CLanguageMgr::GetText( TXT_T_playtest ));
	CGUI::DrawText( TEX_FONT1,255,255,255, 850, 35*21-15, 1.5f, 1.5f, CLanguageMgr::GetText( TXT_after_save ));


}

/*
CLevelEditor::EditorKeys(ALLEGRO_FONT* font,CLevel& lev)
Metoda obsługująca zdarzenia klawiszy dla Edytora Map
TODO:Usunąć argument font, po utworzeniu klasy odpowiedzialnej za napisy
*/

//////////////////////////////////////////////////////////////////////////
/// Obsługa klawiszy edytora map.
/// @param font (zawsze NULL)
/// @param lev obecny poziom
//////////////////////////////////////////////////////////////////////////
void CLevelEditor::EditorKeys(CLevel& lev)
{

	if(CInput::GetKeyState(ALLEGRO_KEY_UP))
	{
		CInput::Delay(ALLEGRO_KEY_UP);
		if(y > 0)
		{
			y--;
		}
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_DOWN))
	{
		CInput::Delay(ALLEGRO_KEY_DOWN);
		if(y < lev.columns-1)
		{
			y++;
		}
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_LEFT))
	{
		CInput::Delay(ALLEGRO_KEY_LEFT);
		if(x > 0)
		{
			x--;
		}
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_RIGHT))
	{
		CInput::Delay(ALLEGRO_KEY_RIGHT);
		if(x < lev.rows-1)
		{
			x++;
		}
	} 
	else if(CInput::GetKeyState(ALLEGRO_KEY_PGUP))
	{
		CInput::Delay(ALLEGRO_KEY_PGUP);
		if(terrain > 1)
		{
			terrain--;
		}
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_PGDN))
	{
		CInput::Delay(ALLEGRO_KEY_PGDN);
		if(terrain < TEX_BORDER-1)
		{
			terrain++;
		}
	}	
	else if(CInput::GetKeyState(ALLEGRO_KEY_S))
	{
		CInput::Delay(ALLEGRO_KEY_S);
		sub_menu = false;
		CMoleEngine::status.current_window = WIN_LOAD_MENU;

	}	
	else if(CInput::GetKeyState(ALLEGRO_KEY_L))
	{
		CInput::Delay(ALLEGRO_KEY_L);

		sub_menu = true;
		CMoleEngine::status.current_window = WIN_LOAD_MENU;
	}	
	else if(CInput::GetKeyState(ALLEGRO_KEY_W))
	{

		CMoleEngine::status.current_window = WIN_SIZE_MENU;

	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_T)) //TODO: usunąć w finalnej wersji, Tester poziomów - PG
	{

		CMoleEngine::TestLevel(name);

	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_F))
	{

		for(int i=0; i < lev.columns; i++)
		{
			for(int j=0;j < lev.rows;j++)
			{

				if(lev.field[i*lev.rows+j] == TER_NONE)
				{
					lev.field[i*lev.rows+j] = terrain;
				}

			}
		}

	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_G))
	{
		int temp = lev.field[y*lev.rows+x]; //TODO:WARNING
		for(int i=0; i < lev.columns; i++)
		{
			for(int j=0;j < lev.rows;j++)
			{

				if(lev.field[i*lev.rows+j] == temp)
				{
					lev.field[i*lev.rows+j] = terrain;
				}

			}
		}

	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_ENTER))
	{
		CInput::Delay(ALLEGRO_KEY_ENTER);
		lev.field[lev.rows*y+x] = terrain;
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_DELETE))
	{
		CInput::Delay(ALLEGRO_KEY_DELETE);
		lev.field[lev.rows*y+x] = TER_NONE;
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_ESCAPE))
	{
		CInput::Delay(ALLEGRO_KEY_ESCAPE);


		//TODO:usunac doexit kiedy beda skonczone pozostale okna - PG
		//TODO:Stworzć metodę, która cofa siędo odpowiedniego okna, na bazie obecneggo - PG
		doexit = true;
	}
	else if(CInput::GetKeyState(ALLEGRO_KEY_Q))
	{
		CInput::Delay(ALLEGRO_KEY_Q);
		CMoleEngine::status.current_window = WIN_MAIN_MENU;
	}	


}


//////////////////////////////////////////////////////////////////////////
/// Obsługuje zdarzenia klawiszy dla Zmiany rozmiaru mapy dla Edytora Map
/// @param lev obecny poziom
//////////////////////////////////////////////////////////////////////////
void CLevelEditor::EditSizeKeys(CLevel& lev)
{

	if(CInput::GetKeyState(ALLEGRO_KEY_1))
	{
		CInput::Delay(ALLEGRO_KEY_1);
		if(!param)
		{
			if(width.length() < 2)
			{
				width += '1';
			}
		}
		else
		{
			if(height.length() < 2)
			{
				height += '1';
			}
		}
	}

	else if(CInput::GetKeyState(ALLEGRO_KEY_2))
	{
		CInput::Delay(ALLEGRO_KEY_2);
		if(!param)
		{
			if(width.length() < 2)
			{
				width += '2';
			}
		}
		else
		{
			if(height.length() < 2)
			{
				height += '2';
			}
		}
	}

	else if(CInput::GetKeyState(ALLEGRO_KEY_3))
	{
		CInput::Delay(ALLEGRO_KEY_3);
		if(!param)
		{
			if(width.length() < 2)
			{
				width += '3';
			}
		}
		else
		{
			if(height.length() < 2)
			{
				height += '3';
			}
		}
	}

	else if(CInput::GetKeyState(ALLEGRO_KEY_4))
	{
		CInput::Delay(ALLEGRO_KEY_4);
		if(!param)
		{
			if(width.length() < 2)
			{
				width += '4';
			}
		}
		else
		{
			if(height.length() < 2)
			{
				height += '4';
			}
		}
	}

	else if(CInput::GetKeyState(ALLEGRO_KEY_5))
	{
		CInput::Delay(ALLEGRO_KEY_5);
		if(!param)
		{
			if(width.length() < 2)
			{
				width += '5';
			}
		}
		else
		{
			if(height.length() < 2)
			{
				height += '5';
			}
		}
	}


	else if(CInput::GetKeyState(ALLEGRO_KEY_6))
	{
		CInput::Delay(ALLEGRO_KEY_6);
		if(!param)
		{
			if(width.length() < 2)
			{
				width += '6';
			}
		}
		else
		{
			if(height.length() < 2)
			{
				height += '6';
			}
		}
	}

	else if(CInput::GetKeyState(ALLEGRO_KEY_7))
	{
		CInput::Delay(ALLEGRO_KEY_7);
		if(!param)
		{
			if(width.length() < 2)
			{
				width += '7';
			}
		}
		else
		{
			if(height.length() < 2)
			{
				height += '7';
			}
		}
	}

	else if(CInput::GetKeyState(ALLEGRO_KEY_8))
	{
		CInput::Delay(ALLEGRO_KEY_8);
		if(!param)
		{
			if(width.length() < 2)
			{
				width += '8';
			}
		}
		else
		{
			if(height.length() < 2)
			{
				height += '8';
			}
		}
	}

	else if(CInput::GetKeyState(ALLEGRO_KEY_9))
	{
		CInput::Delay(ALLEGRO_KEY_9);
		if(!param)
		{
			if(width.length() < 2)
			{
				width += '9';
			}
		}
		else
		{
			if(height.length() < 2)
			{
				height += '9';
			}
		}
	}

	else if(CInput::GetKeyState(ALLEGRO_KEY_0))
	{
		CInput::Delay(ALLEGRO_KEY_0);
		if(!param)
		{
			if(width.length() < 2)
			{
				width += '0';
			}
		}
		else
		{
			if(height.length() < 2)
			{
				height += '0';
			}
		}
	}

	else if(CInput::GetKeyState(ALLEGRO_KEY_DELETE))
	{
		CInput::Delay(ALLEGRO_KEY_DELETE);
		if(!param)
		{
			if(width.length() > 0)
			{
				width.pop_back();
			}
		}
		else
		{
			if(height.length() > 0)
			{
				height.pop_back();
			}
		}
	}

	else if(CInput::GetKeyState(ALLEGRO_KEY_BACKSPACE))
	{
		CInput::Delay(ALLEGRO_KEY_BACKSPACE);
		if(param)
		{

			param = false;
		}


	}

	else if(CInput::GetKeyState(ALLEGRO_KEY_ENTER))
	{
		CInput::Delay(ALLEGRO_KEY_ENTER);
		if(!param)
		{

			param = true;
		}
		else
		{
			CMoleEngine::status.current_window = WIN_EDITOR;
			CLevelEditor::ChangeMapSize(lev);
		}

	}

	else if(CInput::GetKeyState(ALLEGRO_KEY_ESCAPE))
	{
		CInput::Delay(ALLEGRO_KEY_ESCAPE);
		CMoleEngine::status.current_window = WIN_EDITOR;
	}
}


//////////////////////////////////////////////////////////////////////////
/// Metoda zmieniająca rozmiar mapy.
/// @param lev obecny poziom
//////////////////////////////////////////////////////////////////////////
void CLevelEditor::ChangeMapSize(CLevel& lev)
{


	CLevel temp(atoi(width.c_str()),atoi(height.c_str()));

	int tempNum = 0;
	if(temp.columns > 1 && temp.rows > 1)
	{

		for(int i=0; i < temp.columns; i++)
		{
			for(int j=0;j < temp.rows;j++)
			{

				if(i == 8)
				{
					printf("");
				}

				if(lev.columns > i && lev.rows > j)
				{
					temp.field[i*temp.rows+j] = lev.field[i*lev.rows+j];

				}
				else
				{
					temp.field[i*temp.rows+j] = 0;
				}
			}
		}
	}

	lev = temp;
	x = 0;
	y = 0;

}

//TODO:Do przeniesienia po utworzeniu stosownej klasy - PG

//////////////////////////////////////////////////////////////////////////
/// Rysuje ekran ładowania mapy.
/// @param font czcionka
//////////////////////////////////////////////////////////////////////////
void CLevelEditor::LoadMenu()
{
	CGUI::DrawTextCentered( TEX_FONT1,255,255,255, SCREEN_W/2, 50, 5.0f, 5.0f, CLanguageMgr::GetText( TXT_Map_name ));
	CGUI::DrawTextCentered( TEX_FONT1,255,255,255, SCREEN_W/2, 100, 5.0f, 5.0f, name.c_str());

}


//////////////////////////////////////////////////////////////////////////
/// Metoda obsługująca zdarzenia klawiszy dla Okna wczytujacego mapy dla Edytora Map
/// @param lev obecny poziom
//////////////////////////////////////////////////////////////////////////
void CLevelEditor::LoadMenuKeys(CLevel& lev)
{
	if(CInput::GetKeyState(ALLEGRO_KEY_1))
	{
		CInput::Delay(ALLEGRO_KEY_1);
		name += '1';
	}

	else if(CInput::GetKeyState(ALLEGRO_KEY_2))
	{
		CInput::Delay(ALLEGRO_KEY_2);
		name += '2';
	}

	else if(CInput::GetKeyState(ALLEGRO_KEY_3))
	{
		CInput::Delay(ALLEGRO_KEY_3);
		name += '3';
	}

	else if(CInput::GetKeyState(ALLEGRO_KEY_4))
	{
		CInput::Delay(ALLEGRO_KEY_4);
		name += '4';
	}

	else if(CInput::GetKeyState(ALLEGRO_KEY_5))
	{
		CInput::Delay(ALLEGRO_KEY_5);
		name += '5';
	}


	else if(CInput::GetKeyState(ALLEGRO_KEY_6))
	{
		CInput::Delay(ALLEGRO_KEY_6);
		name += '6';
	}

	else if(CInput::GetKeyState(ALLEGRO_KEY_7))
	{
		CInput::Delay(ALLEGRO_KEY_7);
		name += '7';
	}

	else if(CInput::GetKeyState(ALLEGRO_KEY_8))
	{
		CInput::Delay(ALLEGRO_KEY_8);
		name += '8';
	}

	else if(CInput::GetKeyState(ALLEGRO_KEY_9))
	{
		CInput::Delay(ALLEGRO_KEY_9);
		name += '9';
	}

	else if(CInput::GetKeyState(ALLEGRO_KEY_0))
	{
		CInput::Delay(ALLEGRO_KEY_0);
		name += '0';
	}

	else if(CInput::GetKeyState(ALLEGRO_KEY_BACKSPACE))
	{

		CInput::Delay(ALLEGRO_KEY_BACKSPACE);

		if(name.length() > 0)
		{
			name.pop_back();
		}

	}


	else if(CInput::GetKeyState(ALLEGRO_KEY_ENTER))
	{
		CInput::Delay(ALLEGRO_KEY_ENTER);


		if(!sub_menu)
		{
			CLevelMgr::SaveLevel(1,lev,name);
			//Save
		}
		else
		{
			CLevel temp(0,0);
			temp = CLevelMgr::LoadLevel(name);
			if(temp.columns > 1 && temp.rows > 1)
			{
				lev = temp;
			}

			x = 0;
			y = 0;
			//Load
		}

		CMoleEngine::status.current_window = WIN_EDITOR;
	}

	else if(CInput::GetKeyState(ALLEGRO_KEY_ESCAPE))
	{
		CInput::Delay(ALLEGRO_KEY_ESCAPE);
		CMoleEngine::status.current_window = WIN_EDITOR;
	}
}


//TODO:Do przeniesienia po utworzeniu stosownej klasy - PG

//////////////////////////////////////////////////////////////////////////
/// Rysuje ekran edycji rozmiaru mapy.
/// @param font czcionka
//////////////////////////////////////////////////////////////////////////
void CLevelEditor::EditFonts()
{
	CGUI::DrawTextCentered( TEX_FONT1,255,255,255, SCREEN_W/2, 50, 3.0f, 3.0f, CLanguageMgr::GetText( TXT_Map_Size ));

	CGUI::DrawTextCentered( TEX_FONT1,255,255,255, SCREEN_W/2-100, 100, 3.0f, 3.0f, width.c_str());
	CGUI::DrawTextCentered( TEX_FONT1,255,255,255, SCREEN_W/2+100, 100, 3.0f, 3.0f, height.c_str());


	if(!param)
	{
		CGUI::DrawTextCentered( TEX_FONT1,255,255,255, SCREEN_W/2-100, 110, 3.0f, 3.0f, "___");
	}
	else
	{
		CGUI::DrawTextCentered( TEX_FONT1,255,255,255, SCREEN_W/2+100, 110, 3.0f, 3.0f, "___");
	}

	CGUI::DrawTextCentered( TEX_FONT1,255,255,255, SCREEN_W/2-100, 150, 3.0f, 3.0f, CLanguageMgr::GetText( TXT_Width ));
	CGUI::DrawTextCentered( TEX_FONT1,255,255,255, SCREEN_W/2+100, 150, 3.0f, 3.0f, CLanguageMgr::GetText( TXT_Height ));

	CGUI::DrawTextCentered( TEX_FONT1,255,255,255, SCREEN_W/2, 400, 3.0f, 3.0f, CLanguageMgr::GetText( TXT_Enter_to_continue ));
	CGUI::DrawTextCentered( TEX_FONT1,255,255,255, SCREEN_W/2, 430, 3.0f, 3.0f, CLanguageMgr::GetText( TXT_Escape_to_exit ));
	CGUI::DrawTextCentered( TEX_FONT1,255,255,255, SCREEN_W/2, 460, 3.0f, 3.0f, CLanguageMgr::GetText( TXT_Backspace_in_order_to_shift ));
	CGUI::DrawTextCentered( TEX_FONT1,255,255,255, SCREEN_W/2, 490, 3.0f, 3.0f, CLanguageMgr::GetText( TXT_Delete_to_delete_the_figure ));

}