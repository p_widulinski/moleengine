#pragma once

#include <iostream>
#include"CMoleEngine.h"
#include"CLevelMgr.h"

//////////////////////////////////////////////////////////////////////////
/// Enumeracja okien edytora map.
//////////////////////////////////////////////////////////////////////////
enum win_editor_t {
	 WEDI_MAIN, WEDI_LOAD, WEDI_SIZE
};

//////////////////////////////////////////////////////////////////////////
/// Klasa edytora poziomów.
//////////////////////////////////////////////////////////////////////////
class CLevelEditor
{
public:
	CLevelEditor(void);
	~CLevelEditor(void);

	static void EditorTextures(CLevel lev);
	static void EditorFont();
	static void EditorKeys(CLevel& lev);

	
	static void EditSizeKeys(CLevel& lev);
	static void EditFonts();

	static void LoadMenu();
	static void LoadMenuKeys(CLevel& lev);

private:
	static void ChangeMapSize(CLevel& lev);

	/// 
	/// Typ aktualnie wybranego podłoża
	static int terrain;
	
	/// 
	/// Nazwa mapy przy wczytywaniu lub zapisywaniu
	static string name;
	
	/// 
	/// Szerokość mapy przy wczytywaniu lub zapisywaniu
	static string width;
	
	/// 
	/// Wysokośc mapy przy wczytywaniu lub zapisywaniu
	static string height;
	
	/// 
	/// Parametr wskazujący w metodzie LoadMenu, która zmienan jest aktualnie edytowana
	static bool param;
};

