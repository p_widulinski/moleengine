#include "CLevel.h"

//////////////////////////////////////////////////////////////////////////
/// Konstruktor jednoparametrowy.
/// @param size rozmiar poziomu
//////////////////////////////////////////////////////////////////////////
CLevel::CLevel(int size[2])
{
	field = new int [size[0] * size[1]];
	rows = size[0];
	columns = size[1];
	worms = 0;
	diamonds = 0;
	for(int i= 0; i < columns;i++)
	{
		for(int j=0; j < rows; j++)
		{
			field[i*rows+j] = 0;
		}
	}

}


//////////////////////////////////////////////////////////////////////////
/// Konstruktor dwuparametrowy.
/// @param r ilość rzędów poziomu
/// @param c ilość kolumn poziomu
//////////////////////////////////////////////////////////////////////////
CLevel::CLevel(int r, int c)
{
	field = new int [r * c];
	rows = r;
	columns = c;
	worms = 0;
	diamonds = 0;
	for(int i= 0; i < columns;i++)
	{
		for(int j=0; j < rows; j++)
		{
			field[i*rows+j] = 0;
		}
	}
}


//////////////////////////////////////////////////////////////////////////
/// Konstruktor jednoparametrowy.
/// @param r ilość rzędów/kolumn poziomu
//////////////////////////////////////////////////////////////////////////
CLevel::CLevel(int r)
{
	field = new int [r * r];
	rows = r;
	columns = r;
	worms = 0;
	diamonds = 0;
	for(int i=0; i < columns; i++)
	{
		for(int j=0; j < rows; j++)
		{
			field[i*rows+j] = 0;
		}
	}
}

CLevel::CLevel()
{
	field = new int [0];
	rows = 0;
	columns = 0;
	worms = 0;
	diamonds = 0;
}

CLevel::~CLevel(void)
{
}

//////////////////////////////////////////////////////////////////////////
/// Sprawdza, czy pod podanym xy istnieje kamień.
/// @param x współrzędna X testu
/// @param y współrzędna Y testu
/// @return true, jeśli kamień istnieje
//////////////////////////////////////////////////////////////////////////
bool CLevel::StoneExist(int x,int y)
{
	for( int i = 0; i < stoneList.size(); i++)
	{
		if(stoneList[i].x == x && stoneList[i].y == y)
		{
			return true;
		}
	}
	return false;
}
