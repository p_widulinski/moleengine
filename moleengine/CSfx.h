#pragma once
#include "CSound.h"

//////////////////////////////////////////////////////////////////////////
/// Klasa opisująca pojedynczy efekt specjalny (SFX).
//////////////////////////////////////////////////////////////////////////
class CSfx :
	public CSound
{
public:
	CSfx();
	~CSfx();

	/// Nazwa efektu specjalnego
	char* sfxName;

	/// Flaga włączająca efekt echa dźwięku
	bool echoEffect;
};

