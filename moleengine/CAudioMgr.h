#pragma once
#include "CSfx.h"
#include "CMusicTrack.h"

//////////////////////////////////////////////////////////////////////////
/// Klasa menedżera audio.
//////////////////////////////////////////////////////////////////////////
class CAudioMgr
{
public:
	CAudioMgr();
	~CAudioMgr();

	static CSfx* GetSfx(int num);
	static CMusicTrack* GetMusicTrack(int num);

	static void LoadAudio();
	static void ReleaseAudio();
	static void Init();
	static void AdjustSfxVolume( float gain );
	static void AdjustMusicVolume( float gain );
private:
	/// 
	/// Tablica efektów dźwiękowych.
	static CSfx* soundEffects[SFX_MAX];

	/// 
	/// Tablica ścieżek muzycznych.
	static CMusicTrack* musicTracks[MUSIC_MAX];
};

