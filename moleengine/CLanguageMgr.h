#pragma once
#include<string>
#include "AllegroGlobals.h"
enum language_t 
{ 
	LANG_POLSKI, LANG_ENG, LANG_MAX 
};
enum label_t
{
	TXT_OPTIONS, TXT_REMAP_KEYS, TXT_LANGUAGE, TXT_POLSKI, TXT_ENGLISH, TXT_SFX_VOLUME, TXT_MUSIC_VOLUME,
	TXT_DIFFICULTY, TXT_EASY, TXT_NORMAL, TXT_HARD, TXT_UP, TXT_DOWN, TXT_LEFT, TXT_RIGHT, TXT_SUICIDE,
	TXT_CREDITS, TXT_Project_Manager, TXT_Quality_Assurance, TXT_Game_Logic, TXT_Core_Programming,
	TXT_Testing, TXT_Level_Development, TXT_SFX_and_Music, TXT_Input_Programming, TXT_Graphics_and_UI,
	TXT_Can_you_find_all, TXT_of_the_Cheat_Codes, TXT_Soil, TXT_Wall, TXT_Rock, TXT_Worm, TXT_Diamond,
	TXT_TimeStop, TXT_Entry, TXT_Exit, TXT_StoneBreak, TXT_HardWall, TXT_select_tile, TXT_ENTER_place,
	TXT_tile, TXT_W_change, TXT_map_size, TXT_S_save_map, TXT_L_load_map, TXT_T_playtest, TXT_after_save,
	TXT_Map_name, TXT_Enter_to_continue, TXT_Escape_to_exit, TXT_Backspace_in_order_to_shift, TXT_Delete_to_delete_the_figure,
	TXT_Mission_failed, TXT_You_died, TXT_Press_Enter, TXT_to_try_again, TXT_Time_out, TXT_Game_over, 
	TXT_to_back_to_main_menu, TXT_to_try_again2, TXT_You_commited_suicide, TXT_Congratulation, TXT_You_beat_the_Game, 
	TXT_Score, TXT_Difficulty,TXT_Easy, TXT_Normal, TXT_Hard, TXT_Level_Complete, TXT_Diamonds_colected, TXT_Time_left,
	TXT_Bonus_Score, TXT_Perfect,TXT_to_continue, TXT_Map_Size, TXT_Width, TXT_Height, TXT_PLAY, TXT_EXIT,
	TXT_ENTER,TXT_UP_ARROW, TXT_DOWN_ARROW, TXT_LEFT_ARROW, TXT_RIGHT_ARROW, TXT_SPACE, TXT_UNKNOWN, TXT_BACK,
	TXT_MAX
};


//////////////////////////////////////////////////////////////////////////
/// Klasa odpowiedzialna za obsługę wielu języków interfejsu użytkownika.
//////////////////////////////////////////////////////////////////////////
class CLanguageMgr

{
public:
	CLanguageMgr ( void );
	~CLanguageMgr ( void );

	///
	/// Obecnie wybrany język
	static int lang;

	///
	/// Tablica ze wszystkimi etykietami
	static char lang_text[LANG_MAX][TXT_MAX][200];

	static char* GetText (int num);
	static void Init(void);
};



