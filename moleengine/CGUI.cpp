#include "CGUI.h"

int CGUI::credits_y_offset;

CGUI::CGUI(void)
{
	credits_y_offset = 622;
}


CGUI::~CGUI(void)
{
}

//////////////////////////////////////////////////////////////////////////
/// Rysuje GUI wybranego okna.
/// @see struktura window_t
/// @param window wybrane okno
//////////////////////////////////////////////////////////////////////////
 void CGUI::DrawGUI(int window)
 {
	 switch(window)
	 {
	 case WIN_MAIN_MENU:
CGUI::DrawMainMenu();
	 break;
	 case WIN_GAME_MAIN:
CGUI::DrawGameplayHUD( false );
	 break;
	 case WIN_OPTIONS1:
CGUI::DrawOptions();
	 break;
	 case WIN_OPTIONS2:
CGUI::DrawOptions2();
	 break;
	 case WIN_CREDITS:
CGUI::DrawCredits(false);
	 break;
	 }
 }

 //////////////////////////////////////////////////////////////////////////
 /// Rysuje główne menu.
 //////////////////////////////////////////////////////////////////////////
   void CGUI::DrawMainMenu()

 {
	 int y = 163;

	 CRenderer::AddToDrawQueue(TEX_MMBG, 0, 0);


	 CRenderer::AddToDrawQueue(TEX_MMPLAY, 40, y);
	 
	 DrawTextCentered( TEX_FONT1, 255, 255, 255, 120, y + 64, 2.0f, 2.0f, CLanguageMgr::GetText( TXT_PLAY) );


	 y += 120;
	 CRenderer::AddToDrawQueue(TEX_MMOPTIONS, 40, y);
	 
	 DrawTextCentered( TEX_FONT1, 255, 255, 255, 120, y + 64, 2.0f, 2.0f, CLanguageMgr::GetText( TXT_OPTIONS ) );

	 y += 120;
	 //CRenderer::AddToDrawQueue(TEX_MMSHOP, 40, 283); // cut - pw
	 CRenderer::AddToDrawQueue(TEX_MMCREDITS, 40, y);
	 
	 DrawTextCentered( TEX_FONT1, 255, 255, 255, 120, y + 64, 2.0f, 2.0f, CLanguageMgr::GetText( TXT_CREDITS ) );

	 y += 120;
	 //CRenderer::AddToDrawQueue(TEX_MMRANKING, 40, 523); // cut - pw
	 CRenderer::AddToDrawQueue(TEX_MMEXIT, 40, y);
	 
	 DrawTextCentered( TEX_FONT1, 255, 255, 255, 120, y + 64, 2.0f, 2.0f, CLanguageMgr::GetText( TXT_EXIT) );

	 CRenderer::AddToDrawQueue(TEX_MMCM, 309, 102);

	 CRenderer::AddToDrawQueue(TEX_MMMOLE, 238, 303);

	 CRenderer::AddToDrawQueue(TEX_MAIN_MENU_HIGHLIGHT, 28, 154 + CMoleEngine::status.current_menu_option * 120 );

 }

//////////////////////////////////////////////////////////////////////////
/// Rysuje HUD (Heads Up Display, stan) rozgrywki.
/// @param drawText określa czy rysować tekst
//////////////////////////////////////////////////////////////////////////
 void CGUI::DrawGameplayHUD( bool drawText )
 {
	 CRenderer::AddToDrawQueue(TEX_FRAME_HUD, 0, 0);

	 CRenderer::AddToDrawQueue(TEX_HEART_HUD, 20, 20);
	 CRenderer::AddToDrawQueue(TEX_WORM_HUD, 120, 20);
	 CRenderer::AddToDrawQueue(TEX_DIAMOND_HUD, 220, 20);
	 CRenderer::AddToDrawQueue(TEX_ELIXIR_HUD, 320, 20);
	 CRenderer::AddToDrawQueue(TEX_TIMER_HUD, 420, 20);
	 CRenderer::AddToDrawQueue(TEX_AVATAR_HUD, 893, 20);
	
	 DrawTextCentered( TEX_FONT1, 92, 95, 3.0f, 3.0f, CCommon::va( "%i", currentLev.life ) );

	 if( currentLev.worm != currentLev.worm_total ) {
		DrawTextCentered( TEX_FONT1, 192, 95, 3.0f, 3.0f, CCommon::va( "%i", currentLev.worm ) );
	 }
	 else {
		if( CMoleEngine::GetSeconds() & 1 ) {
			DrawTextCentered( TEX_FONT1, 192, 95, 3.0f, 3.0f, CCommon::va( "%i", currentLev.worm ) );
		}
		else {
			DrawTextCentered( TEX_FONT1, 0, 255, 0, 192, 95, 3.0f, 3.0f, CCommon::va( "%i", currentLev.worm ) );
		}
	 }

	 DrawTextCentered( TEX_FONT1, 292, 95, 3.0f, 3.0f, CCommon::va( "%i", currentLev.diamond ) );
	 DrawTextCentered( TEX_FONT1, 392, 95, 3.0f, 3.0f, CCommon::va( "%i", currentLev.elixir ) );
	 
	 DrawTextCentered( TEX_FONT1, 577, 50, 3.0f, 3.0f, CCommon::va( "LVL", currentLev.id / 100, currentLev.id % 100 ) );
	 DrawTextCentered( TEX_FONT1, 577, 80, 3.0f, 3.0f, CCommon::va( "%01i-%02i", currentLev.id / 100, currentLev.id % 100 ) );
	 
	 DrawTextCentered( TEX_FONT1, 772, 48, 3.0f, 3.0f, CLanguageMgr::GetText( TXT_Score ) );
	 DrawTextCentered( TEX_FONT1, 772, 75, 4.0f, 4.0f, CCommon::va( "%05i", currentLev.score ) );

	 if( fadeToBlack ) {
		DrawText( TEX_FONT1, 255, 255, 255, 426, 103, 2.0f, 2.0f, "00:00" );
	 }
	 else {
		 if( CMoleEngine::GetRemainingTime() <= 5 ) {
			DrawText( TEX_FONT1, 255, 40, 40, 426, 103, 2.0f, 2.0f, CCommon::va( "%02i:%02i", CMoleEngine::GetRemainingTime() / 60, CMoleEngine::GetRemainingTime() % 60 ) );
		 }
		 else {
			DrawText( TEX_FONT1, 426, 103, 2.0f, 2.0f, CCommon::va( "%02i:%02i", CMoleEngine::GetRemainingTime() / 60, CMoleEngine::GetRemainingTime() % 60 ) );
		 }
	 }

	 if( currentLev.score1UpMsgDisappearTime > CMoleEngine::GetSeconds() ) {
		DrawTextCentered( TEX_FONT1,  512, 600, 5.0f, 5.0f, CCommon::va( "1 UP!", currentLev.life ) );
	 }
 }
 
//////////////////////////////////////////////////////////////////////////
/// Rysuje HUD (Heads Up Display, stan) rozgrywki.
/// @param drawText określa czy rysować tekst
//////////////////////////////////////////////////////////////////////////
char *CGUI::KeyNameForNumber( int keynum )
{
	if( keynum >= ALLEGRO_KEY_A && keynum <= ALLEGRO_KEY_Z )
	{
		return CCommon::va( "%c", 'A' + ( keynum - ALLEGRO_KEY_A ) );
	}
	else if( keynum == ALLEGRO_KEY_ENTER ) {
		return CLanguageMgr::GetText( TXT_ENTER );
	}
	else if( keynum == ALLEGRO_KEY_UP ) {
		return CLanguageMgr::GetText( TXT_UP_ARROW );
	}
	else if( keynum == ALLEGRO_KEY_DOWN ) {
		return CLanguageMgr::GetText( TXT_DOWN_ARROW );
	}
	else if( keynum == ALLEGRO_KEY_LEFT ) {
		return CLanguageMgr::GetText( TXT_LEFT_ARROW );
	}
	else if( keynum == ALLEGRO_KEY_RIGHT ) {
		return CLanguageMgr::GetText( TXT_RIGHT_ARROW );
	}
	else if( keynum == ALLEGRO_KEY_SPACE ) {
		return CLanguageMgr::GetText( TXT_SPACE );
	}

	return CLanguageMgr::GetText( TXT_UNKNOWN);
 }


 //////////////////////////////////////////////////////////////////////////
 /// Rysuje ekran opcji podstawowych.
 //////////////////////////////////////////////////////////////////////////
 void CGUI::DrawOptions()
 {
	 bool remapSelected = CMoleEngine::status.current_menu_option == 0;
	 bool languageSelected = CMoleEngine::status.current_menu_option == 1;
	 bool polskiSelected = CLanguageMgr::lang == LANG_POLSKI;
	 bool englishSelected = CLanguageMgr::lang == LANG_ENG;
	 bool sfxSelected = CMoleEngine::status.current_menu_option == 2;
	 bool musicSelected = CMoleEngine::status.current_menu_option == 3;
	 bool difficultySelected = CMoleEngine::status.current_menu_option == 4;
	 bool easySelected = CMoleEngine::status.difficulty == DIF_EASY;
	 bool normalSelected = CMoleEngine::status.difficulty == DIF_NORMAL;
	 bool hardSelected = CMoleEngine::status.difficulty == DIF_HARD;

	 CRenderer::AddToDrawQueue(TEX_OPTIONSBG, 0, 0);

	 CRenderer::AddToDrawQueue(TEX_OPTIONSLINE, 0, 83);
	 
	 // x = 504 = 0%
	 // x = 812 = 100%
	 float svol = CMoleEngine::status.sfx;
	 int svolx = 504 + (int)( 308.0f * svol );

	 float mvol = CMoleEngine::status.music;
	 int mvolx = 504 + (int)( 308.0f * mvol );

	 CRenderer::AddToDrawQueue(TEX_OPTIONSBARBG, 504, 392);
	 CRenderer::AddToDrawQueue(TEX_OPTIONSBARFG, svolx, 392); 
	 
	 CRenderer::AddToDrawQueue(TEX_OPTIONSBARBG, 504, 512);
	 CRenderer::AddToDrawQueue(TEX_OPTIONSBARFG, mvolx, 512); 

	 DrawText( TEX_FONT1, 255, 255, 255, 40, 44, 4.0f, 4.0f, CLanguageMgr::GetText( TXT_OPTIONS ) );
	 
	 DrawText( TEX_FONT1, ( remapSelected ? 255 : 128 ), ( remapSelected ? 255 : 128 ), ( remapSelected ? 255 : 128 ), 
		 40, 152, 3.0f, 3.0f, CLanguageMgr::GetText( TXT_REMAP_KEYS ) );

	 DrawText( TEX_FONT1, ( languageSelected ? 255 : 128 ), ( languageSelected ? 255 : 128 ), ( languageSelected ? 255 : 128 ), 
		 40, 272, 3.0f, 3.0f, CLanguageMgr::GetText( TXT_LANGUAGE) );
	 DrawText( TEX_FONT1, ( polskiSelected ? 255 : 128 ), ( polskiSelected ? 255 : 128 ), ( polskiSelected ? 255 : 128 ), 
		 492, 272, 3.0f, 3.0f, CLanguageMgr::GetText( TXT_POLSKI ) );
	 DrawText( TEX_FONT1, ( englishSelected ? 255 : 128 ), ( englishSelected ? 255 : 128 ), ( englishSelected ? 255 : 128 ), 
		 758, 272, 3.0f, 3.0f, CLanguageMgr::GetText( TXT_ENGLISH ) );

	 DrawText( TEX_FONT1, ( sfxSelected ? 255 : 128 ), ( sfxSelected ? 255 : 128 ), ( sfxSelected ? 255 : 128 ), 
		 40, 400, 3.0f, 3.0f, CLanguageMgr::GetText( TXT_SFX_VOLUME ) );

	 DrawText( TEX_FONT1, ( musicSelected ? 255 : 128 ), ( musicSelected ? 255 : 128 ), ( musicSelected ? 255 : 128 ), 
		 40, 520, 3.0f, 3.0f, CLanguageMgr::GetText( TXT_MUSIC_VOLUME ) );

	 DrawText( TEX_FONT1, ( difficultySelected ? 255 : 128 ), ( difficultySelected ? 255 : 128 ), ( difficultySelected ? 255 : 128 ), 
		 40, 641, 3.0f, 3.0f, CLanguageMgr::GetText( TXT_DIFFICULTY) );
	 DrawText( TEX_FONT1, ( easySelected ? 255 : 128 ), ( easySelected ? 255 : 128 ), ( easySelected ? 255 : 128 ), 
		 492, 646, 3.0f, 3.0f, CLanguageMgr::GetText( TXT_EASY ) );
	 DrawText( TEX_FONT1, ( normalSelected ? 255 : 128 ), ( normalSelected ? 255 : 128 ), ( normalSelected ? 255 : 128 ), 
		 650, 646, 3.0f, 3.0f, CLanguageMgr::GetText( TXT_NORMAL ) );
	 DrawText( TEX_FONT1, ( hardSelected ? 255 : 128 ), ( hardSelected ? 255 : 128 ), ( hardSelected ? 255 : 128 ), 
		 869, 646, 3.0f, 3.0f, CLanguageMgr::GetText( TXT_HARD ) );

	 DrawText( TEX_FONT1, 128, 128, 128, 695, 730, 3.0f, 3.0f, CLanguageMgr::GetText( TXT_BACK ) );
 }

 //////////////////////////////////////////////////////////////////////////
 /// Rysuje ekran opcji mapowania klawiszy.
 //////////////////////////////////////////////////////////////////////////
    void CGUI::DrawOptions2()

 {
	 bool upSelected = CMoleEngine::status.current_menu_option == 0;
	 bool downSelected = CMoleEngine::status.current_menu_option == 1;
	 bool leftSelected = CMoleEngine::status.current_menu_option == 2;
	 bool rightSelected = CMoleEngine::status.current_menu_option == 3;
	 bool suicideSelected = CMoleEngine::status.current_menu_option == 4;

	 bool isBindingUp = CMoleEngine::status.bindingKeyUp;
	 bool isBindingDown = CMoleEngine::status.bindingKeyDown;
	 bool isBindingLeft = CMoleEngine::status.bindingKeyLeft;
	 bool isBindingRight = CMoleEngine::status.bindingKeyRight;
	 bool isBindingSuicide = CMoleEngine::status.bindingKeySuicide;

	 CRenderer::AddToDrawQueue(TEX_OPTIONSBG, 0, 0);
	 CRenderer::AddToDrawQueue(TEX_OPTIONSLINE, 0, 83);	 

	 DrawText( TEX_FONT1, 255, 255, 255, 40, 44, 4.0f, 4.0f, CLanguageMgr::GetText( TXT_REMAP_KEYS ) );

	 CRenderer::AddToDrawQueue(TEX_OPTIONSKEYBOX, 440, 175);
	 CRenderer::AddToDrawQueue(TEX_OPTIONSKEYBOX, 540, 175);
	 CRenderer::AddToDrawQueue(TEX_OPTIONSKEYBOX, 640, 175);
	 	 
	 CRenderer::AddToDrawQueue(TEX_OPTIONSKEYBOX, 440, 295);
	 CRenderer::AddToDrawQueue(TEX_OPTIONSKEYBOX, 540, 295);
	 CRenderer::AddToDrawQueue(TEX_OPTIONSKEYBOX, 640, 295);
	 
	 CRenderer::AddToDrawQueue(TEX_OPTIONSKEYBOX, 440, 415);
	 CRenderer::AddToDrawQueue(TEX_OPTIONSKEYBOX, 540, 415);
	 CRenderer::AddToDrawQueue(TEX_OPTIONSKEYBOX, 640, 415);
	 
	 CRenderer::AddToDrawQueue(TEX_OPTIONSKEYBOX, 440, 535);
	 CRenderer::AddToDrawQueue(TEX_OPTIONSKEYBOX, 540, 535);
	 CRenderer::AddToDrawQueue(TEX_OPTIONSKEYBOX, 640, 535);
	 
	 CRenderer::AddToDrawQueue(TEX_OPTIONSKEYBOX, 440, 655);
	 CRenderer::AddToDrawQueue(TEX_OPTIONSKEYBOX, 540, 655);
	 CRenderer::AddToDrawQueue(TEX_OPTIONSKEYBOX, 640, 655);
	 

	 DrawText( TEX_FONT1, ( upSelected ? 255 : 128 ), ( upSelected ? 255 : 128 ), ( upSelected ? 255 : 128 ), 
		 140, 185, 3.0f, 3.0f, CLanguageMgr::GetText( TXT_UP ) );
	 
	 DrawText( TEX_FONT1, ( downSelected ? 255 : 128 ), ( downSelected ? 255 : 128 ), ( downSelected ? 255 : 128 ),  
		 140, 305, 3.0f, 3.0f, CLanguageMgr::GetText( TXT_DOWN ) );

	 DrawText( TEX_FONT1, ( leftSelected ? 255 : 128 ), ( leftSelected ? 255 : 128 ), ( leftSelected ? 255 : 128 ), 
		 140, 425, 3.0f, 3.0f, CLanguageMgr::GetText( TXT_LEFT ) );

	 DrawText( TEX_FONT1, ( rightSelected ? 255 : 128 ), ( rightSelected ? 255 : 128 ), ( rightSelected ? 255 : 128 ), 
		 140, 545, 3.0f, 3.0f, CLanguageMgr::GetText( TXT_RIGHT ) );

	 DrawText( TEX_FONT1, ( suicideSelected ? 255 : 128 ), ( suicideSelected ? 255 : 128 ), ( suicideSelected ? 255 : 128 ), 
		 140, 665, 3.0f, 3.0f, CLanguageMgr::GetText( TXT_SUICIDE ) );


	 DrawTextCentered( TEX_FONT1, ( isBindingUp ? 255 : 128 ), ( isBindingUp ? 0 : 128 ), ( isBindingUp ? 0 : 128 ),
		 670, 185, 3.0f, 3.0f, KeyNameForNumber( CMoleEngine::status.keyUp ) );
	 
	 DrawTextCentered( TEX_FONT1, ( isBindingDown ? 255 : 128 ), ( isBindingDown ? 0 : 128 ), ( isBindingDown ? 0 : 128 ),
		 670, 305, 3.0f, 3.0f, KeyNameForNumber( CMoleEngine::status.keyDown ) );
	 
	 DrawTextCentered( TEX_FONT1, ( isBindingLeft ? 255 : 128 ), ( isBindingLeft ? 0 : 128 ), ( isBindingLeft ? 0 : 128 ),
		 670, 425, 3.0f, 3.0f, KeyNameForNumber( CMoleEngine::status.keyLeft ) );
	 
	 DrawTextCentered( TEX_FONT1, ( isBindingRight ? 255 : 128 ), ( isBindingRight ? 0 : 128 ), ( isBindingRight ? 0 : 128 ),
		 670, 545, 3.0f, 3.0f, KeyNameForNumber( CMoleEngine::status.keyRight ) );
	 
	 DrawTextCentered( TEX_FONT1, ( isBindingSuicide ? 255 : 128 ), ( isBindingSuicide ? 0 : 128 ), ( isBindingSuicide ? 0 : 128 ),
		 670, 665, 3.0f, 3.0f, KeyNameForNumber( CMoleEngine::status.keySuicide ) );

	 DrawText( TEX_FONT1, 128, 128, 128, 695, 730, 3.0f, 3.0f, CLanguageMgr::GetText( TXT_BACK ) );

 }

//////////////////////////////////////////////////////////////////////////
/// Rysuje ekran "O autorach".
/// @param drawText określa czy rysować tekst
//////////////////////////////////////////////////////////////////////////
void CGUI::DrawCredits( bool drawText )
{
	int y_offset = 0;
	int y_incr = 40;

	if( !( CMoleEngine::time % 2 ) ) {
		credits_y_offset--;
		if( credits_y_offset < ( 147 - 1480 ) ) {
			credits_y_offset = 622;
		}
	}
	
	DrawTextCentered( TEX_FONT1, 255, 0,   0,   512, credits_y_offset + y_offset, 6.0f, 6.0f, "CODENAME: MOLE" ); y_offset += y_incr;
	y_offset += y_incr;
	DrawTextCentered( TEX_FONT1, 255, 0,   0,   512, credits_y_offset + y_offset, 4.0f, 4.0f, "by Fire Team (c) 2015" ); y_offset += y_incr;
	y_offset += y_incr;
	y_offset += y_incr;
	DrawTextCentered( TEX_FONT1, 255, 255, 255, 512, credits_y_offset + y_offset, 4.0f, 4.0f, CLanguageMgr::GetText( TXT_CREDITS ) ); y_offset += y_incr;
	y_offset += y_incr;
	y_offset += y_incr;
	DrawTextCentered( TEX_FONT1, 255, 92,  92,  512, credits_y_offset + y_offset, 4.0f, 4.0f, CLanguageMgr::GetText( TXT_Project_Manager ) ); y_offset += y_incr;
	DrawTextCentered( TEX_FONT1, 255, 92,  92,  512, credits_y_offset + y_offset, 4.0f, 4.0f, CLanguageMgr::GetText( TXT_Quality_Assurance ) ); y_offset += y_incr;
	DrawTextCentered( TEX_FONT1, 255, 255, 255, 512, credits_y_offset + y_offset, 4.0f, 4.0f, "Patryk Widulinski" ); y_offset += y_incr;
	y_offset += y_incr;
	DrawTextCentered( TEX_FONT1, 255, 92,  92,  512, credits_y_offset + y_offset, 4.0f, 4.0f, CLanguageMgr::GetText( TXT_Game_Logic ) ); y_offset += y_incr;
	DrawTextCentered( TEX_FONT1, 255, 92,  92,  512, credits_y_offset + y_offset, 4.0f, 4.0f, CLanguageMgr::GetText( TXT_Core_Programming ) ); y_offset += y_incr;
	DrawTextCentered( TEX_FONT1, 255, 255, 255, 512, credits_y_offset + y_offset, 4.0f, 4.0f, "Piotr Golebiowski" ); y_offset += y_incr;
	y_offset += y_incr;
	DrawTextCentered( TEX_FONT1, 255, 92,  92,  512, credits_y_offset + y_offset, 4.0f, 4.0f, CLanguageMgr::GetText( TXT_Testing ) ); y_offset += y_incr;
	DrawTextCentered( TEX_FONT1, 255, 92,  92,  512, credits_y_offset + y_offset, 4.0f, 4.0f, CLanguageMgr::GetText( TXT_Level_Development ) ); y_offset += y_incr;
	DrawTextCentered( TEX_FONT1, 255, 255, 255, 512, credits_y_offset + y_offset, 4.0f, 4.0f, "Karol Brzeski" ); y_offset += y_incr;
	y_offset += y_incr;
	DrawTextCentered( TEX_FONT1, 255, 92,  92,  512, credits_y_offset + y_offset, 4.0f, 4.0f, CLanguageMgr::GetText( TXT_SFX_and_Music ) ); y_offset += y_incr;
	DrawTextCentered( TEX_FONT1, 255, 92,  92,  512, credits_y_offset + y_offset, 4.0f, 4.0f, CLanguageMgr::GetText( TXT_Input_Programming ) ); y_offset += y_incr;
	DrawTextCentered( TEX_FONT1, 255, 255, 255, 512, credits_y_offset + y_offset, 4.0f, 4.0f, "Damian Zaleski" ); y_offset += y_incr;
	y_offset += y_incr;
	DrawTextCentered( TEX_FONT1, 255, 92,  92,  512, credits_y_offset + y_offset, 4.0f, 4.0f, CLanguageMgr::GetText( TXT_Graphics_and_UI ) ); y_offset += y_incr;
	DrawTextCentered( TEX_FONT1, 255, 255, 255, 512, credits_y_offset + y_offset, 4.0f, 4.0f, "Mateusz Kowalski" ); y_offset += y_incr;
	y_offset += y_incr;
	y_offset += y_incr;
	DrawTextCentered( TEX_FONT1, 255, 255, 255, 512, credits_y_offset + y_offset, 4.0f, 4.0f, "Politechnika" ); y_offset += y_incr;
	DrawTextCentered( TEX_FONT1, 255, 255, 255, 512, credits_y_offset + y_offset, 4.0f, 4.0f, "Koszalinska" ); y_offset += y_incr;
	DrawTextCentered( TEX_FONT1, 255, 255, 255, 512, credits_y_offset + y_offset, 4.0f, 4.0f, "MMXV (c) Fire Team" ); y_offset += y_incr;
	y_offset += y_incr;
	y_offset += y_incr;
	y_offset += y_incr;
	y_offset += y_incr;
	DrawTextCentered( TEX_FONT1, 255, 255, 255, 512, credits_y_offset + y_offset, 4.0f, 4.0f, CLanguageMgr::GetText( TXT_Can_you_find_all ) ); y_offset += y_incr;
	DrawTextCentered( TEX_FONT1, 255, 255, 255, 512, credits_y_offset + y_offset, 4.0f, 4.0f, CLanguageMgr::GetText( TXT_of_the_Cheat_Codes ) ); y_offset += y_incr;

	CRenderer::AddToDrawQueue(TEX_CREDITSBG, 0, 0);

	DrawText( TEX_FONT1, 128, 128, 128, 695, 730, 3.0f, 3.0f, CLanguageMgr::GetText( TXT_BACK ) );
}

#define FONT1_TEX_WIDTH 128
#define FONT1_TEX_HEIGHT 128
#define FONT1_CHAR_WIDTH 8
#define FONT1_CHAR_HEIGHT 8
#define FONT1_CHAR_X_SPACING 0


//////////////////////////////////////////////////////////////////////////
/// Zwraca długość tekstu w pikselach.
/// @param fontTexture tekstura czcionki
/// @param scale_x skala szerokości liter
/// @param text tekst
/// @return długość tekstu w pikselach
//////////////////////////////////////////////////////////////////////////
float CGUI::GetTextWidth( int fontTexture, float scale_x, const char *text ) {
	switch( fontTexture ) {
	case TEX_FONT1:
		return strlen( text ) * (float)( ( (float)FONT1_CHAR_WIDTH + (float)FONT1_CHAR_X_SPACING ) * scale_x );
	}

	return 0.0f;
}

//////////////////////////////////////////////////////////////////////////
/// Rysuje kolorowany tekst.
/// @param fontTexture tekstura czcionki
/// @param color_r składowa R koloru (0-255)
/// @param color_g składowa G koloru (0-255)
/// @param color_b składowa B koloru (0-255)
/// @param x wspolrzedna X tekstu
/// @param y wspolrzedna Y tekstu
/// @param scale_x skala szerokości liter
/// @param scale_y skala długości liter
/// @param text tekst
//////////////////////////////////////////////////////////////////////////
void CGUI::DrawText( int fontTexture, unsigned char color_r, unsigned char color_g, unsigned char color_b, int x, int y, float scale_x, float scale_y, const char *text )
{
	int x_offset = 0;
	int y_offset = 0;
	char *p = (char *)text;

	while( p && *p )
	{
		// rysuj znak
		CRenderer::AddToDrawQueue( fontTexture, x + x_offset, y + y_offset,
			true, (float)( ( *p % 16 ) * FONT1_CHAR_WIDTH ), (float)( ( *p / 16 ) * FONT1_CHAR_HEIGHT ), FONT1_CHAR_WIDTH, FONT1_CHAR_HEIGHT, // wycinek literki z calej tekstury
			true, (float)( (float)FONT1_CHAR_WIDTH / (float)FONT1_TEX_WIDTH ) * scale_x, (float)( (float)FONT1_CHAR_HEIGHT / (float)FONT1_TEX_HEIGHT ) * scale_y,
			true, color_r, color_g, color_b ); // skalowanie

		x_offset += (int)( (float)( FONT1_CHAR_WIDTH + FONT1_CHAR_X_SPACING ) * scale_x );

		p++;
	}
}


//////////////////////////////////////////////////////////////////////////
/// Rysuje biały tekst.
/// @param fontTexture tekstura czcionki
/// @param x wspolrzedna X tekstu
/// @param y wspolrzedna Y tekstu
/// @param scale_x skala szerokości liter
/// @param scale_y skala długości liter
/// @param text tekst
//////////////////////////////////////////////////////////////////////////
void CGUI::DrawText( int fontTexture, int x, int y, float scale_x, float scale_y, const char *text )
{
	DrawText( fontTexture, 255, 255, 255, x, y, scale_x, scale_y, text );
}


//////////////////////////////////////////////////////////////////////////
/// Rysuje biały wyśrodkowany tekst.
/// @param fontTexture tekstura czcionki
/// @param x_center wspolrzedna X ŚRODKA tekstu
/// @param y wspolrzedna Y tekstu
/// @param scale_x skala szerokości liter
/// @param scale_y skala długości liter
/// @param text tekst
//////////////////////////////////////////////////////////////////////////
void CGUI::DrawTextCentered( int fontTexture, int x_center, int y, float scale_x, float scale_y, const char *text )
{
	DrawTextCentered( fontTexture, 255, 255, 255, x_center, y, scale_x, scale_y, text );
}


//////////////////////////////////////////////////////////////////////////
/// Rysuje kolorowany wyśrodkowany tekst.
/// @param fontTexture tekstura czcionki
/// @param color_r składowa R koloru (0-255)
/// @param color_g składowa G koloru (0-255)
/// @param color_b składowa B koloru (0-255)
/// @param x_center wspolrzedna X ŚRODKA tekstu
/// @param y wspolrzedna Y tekstu
/// @param scale_x skala szerokości liter
/// @param scale_y skala długości liter
/// @param text tekst
//////////////////////////////////////////////////////////////////////////
void CGUI::DrawTextCentered( int fontTexture, unsigned char color_r, unsigned char color_g, unsigned char color_b, int x_center, int y, float scale_x, float scale_y, const char *text )
{
	int x_offset = 0;
	int y_offset = 0;
	char *p = (char *)text;

	int x = 0;

	// oblicz od jakiego x rysować tekst by był wyśrodkowany
	float w = GetTextWidth( fontTexture, scale_x, text );
	x = x_center - (int)( w / 2.0f );

	DrawText( fontTexture, color_r, color_g, color_b, x, y, scale_x, scale_y, text );
}
