#pragma once
#include "CSound.h"

//////////////////////////////////////////////////////////////////////////
/// Klasa opisująca pojedynczą ścieżkę muzyczną.
//////////////////////////////////////////////////////////////////////////
class CMusicTrack :
	public CSound
{
public:
	CMusicTrack();
	~CMusicTrack();

	/// 
	/// Nazwa ścieżki
	char* musicName;

	/// 
	/// Flaga zapętlenia ścieżki
	bool loopTrack;
};

