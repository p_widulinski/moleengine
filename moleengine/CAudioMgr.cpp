#include "CAudioMgr.h"

CSfx* CAudioMgr::soundEffects[SFX_MAX];
CMusicTrack* CAudioMgr::musicTracks[MUSIC_MAX];

CAudioMgr::CAudioMgr()
{
}

CAudioMgr::~CAudioMgr()
{
}

//////////////////////////////////////////////////////////////////////////
/// Inicjalizuje klasę.
//////////////////////////////////////////////////////////////////////////
void CAudioMgr::Init()
{
	for (int i = 0; i < SFX_MAX; i++)
	{
		soundEffects[i] = new CSfx();
	}
	for (int i = 0; i < MUSIC_MAX; i++)
	{
		musicTracks[i] = new CMusicTrack();
	}
}

//////////////////////////////////////////////////////////////////////////
/// Pobiera dźwięk o podanym id.
/// @param num id dźwięku
/// @return wskaźnik na dźwięk
//////////////////////////////////////////////////////////////////////////
CSfx* CAudioMgr::GetSfx(int num)
{
	return soundEffects[num];
}

//////////////////////////////////////////////////////////////////////////
/// Pobiera ścieżkę muzyczną o podanym id.
/// @param num id ścieżki
/// @return wskaźnik na ścieżkę
//////////////////////////////////////////////////////////////////////////
CMusicTrack* CAudioMgr::GetMusicTrack(int num)
{
	return musicTracks[num];
}

//////////////////////////////////////////////////////////////////////////
/// Wczytuje wszystkie dźwięki do tablic. Efekty dźwiękowe do tablicy soundEffects, a muzyke do tablicy MusicTracks
//////////////////////////////////////////////////////////////////////////
void CAudioMgr::LoadAudio()
{
	soundEffects[SFX_MENU_ON_CLICK]->Load("SFX_MENU_ON_CLICK");
	soundEffects[SFX_STEP_ON_GROUND]->Load("SFX_STEP_ON_GROUND");
	soundEffects[SFX_ON_LIFE_LOSS]->Load("SFX_ON_LIFE_LOSS");
	soundEffects[SFX_ON_STONE_TUMBLE]->Load("SFX_ON_STONE_TUMBLE");
	soundEffects[SFX_ON_NEXT_LEVEL_UNLOCK]->Load("SFX_ON_NEXT_LEVEL_UNLOCK");
	soundEffects[SFX_DIAMOND_COLLECTED]->Load("SFX_DIAMOND_COLLECTED");
	soundEffects[SFX_LEVEL_COMPLETE]->Load("SFX_LEVEL_COMPLETE");
	soundEffects[SFX_WORM_EAT]->Load("SFX_WORM_EAT");
	soundEffects[SFX_PERFECT_LEVEL_COMPLETION]->Load("SFX_PERFECT_LEVEL_COMPLETION");

	musicTracks[MUSIC_TEST1]->Load("MUSIC_TEST1");
	musicTracks[MUSIC_TEST2]->Load("MUSIC_TEST2");
}


//////////////////////////////////////////////////////////////////////////
/// Dopasowuje głośność dźwięków SFX na podstawie podanej wartości.
//////////////////////////////////////////////////////////////////////////
void CAudioMgr::AdjustSfxVolume( float gain )
{
	for (int i = 0; i < SFX_MAX; i++)
	{
		soundEffects[i]->SetVolume( gain );
	}
}


//////////////////////////////////////////////////////////////////////////
/// Dopasowuje głośność muzyki na podstawie podanej wartości.
//////////////////////////////////////////////////////////////////////////
void CAudioMgr::AdjustMusicVolume( float gain )
{
	for (int i = 0; i < MUSIC_MAX; i++)
	{
		musicTracks[i]->SetVolume( gain );
	}
}


//////////////////////////////////////////////////////////////////////////
/// Usuwa dźwięki z pamięci. 
//////////////////////////////////////////////////////////////////////////
void CAudioMgr::ReleaseAudio()
{
	// TODO
}
